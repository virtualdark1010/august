import styled from 'styled-components';

export const Main = styled.div`
  width: 100%;
`;

export const TopSection = styled.div`
  height: 50%;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: flex-end;
  position: relative;
  background: linear-gradient(180deg, #000 80%, transparent);
`;

export const BottomSection = styled.div`
  position: relative;
  height: 50%;
  overflow: hidden;
  display: flex;
  justify-content: center;
  background: linear-gradient(0deg, #000 80%, transparent);
`;

export const Blur = styled.div<{ direction: 'bottom' | 'top' }>`
  box-shadow: inset 0px 0px 100px 40px #000;
  position: absolute;
  width: 100%;
  height: 200px;
  z-index: 1;
  ${({ direction = 'bottom' }) => (direction === 'bottom' ? 'bottom' : 'top')}: 0;
`;
