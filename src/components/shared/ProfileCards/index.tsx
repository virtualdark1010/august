import {
  CardContainer,
  CardHeader,
  ProfileContainer,
  UserDetailsContainer,
  CardContents,
  CardTimeStampContainer,
} from './styles';

type ProfileCardsType = {
  name: string;
  userName: string;
  content: string;
  timeStamp: string;
  image: JSX.Element;
  infoSource: string;
};

const ProfileCards = (props: ProfileCardsType) => {
  const { name, userName, content, timeStamp, image, infoSource } = props;
  return (
    <CardContainer>
      <CardHeader>
        <ProfileContainer>{image}</ProfileContainer>
        <UserDetailsContainer>
          <p>{name}</p>
          <p>{userName}</p>
        </UserDetailsContainer>
      </CardHeader>
      <CardContents>{content}</CardContents>
      <CardTimeStampContainer>
        <p>{timeStamp}</p>
        <p>{infoSource}</p>
      </CardTimeStampContainer>
    </CardContainer>
  );
};

export default ProfileCards;
