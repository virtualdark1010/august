export type CarouselWithInfoType = {
  addBottomMargin?: boolean;
  reverse?: boolean;
  floatingImage?: boolean;
  carouselImage: string[];
  infoSectionImage: string;
};
