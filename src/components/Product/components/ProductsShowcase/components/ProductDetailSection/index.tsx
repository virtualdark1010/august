import React, { ReactNode, useEffect } from 'react';
import Text from 'shared/Text';
import { useAnimation } from 'framer-motion';

import {
  Main,
  Container,
  Wrapper,
  Description,
  Title,
  ImageWrapper,
  FloatingImageWrapper,
  Image,
  Inline,
  Banner,
} from './styles';
import { useInView } from 'react-intersection-observer';

const ProductDetailSection = ({
  title,
  description,
  imageUrl,
  floatingImageUrl,
  patch,
  highlightedText,
  bannerText,
  fullMargin = false,
}: {
  title?: string;
  description?: string;
  imageUrl: string;
  floatingImageUrl?: string;
  patch?: ReactNode | ReactNode[];
  highlightedText?: string;
  bannerText?: string;
  fullMargin?: boolean;
}) => {
  const animation = useAnimation();
  const { ref, inView } = useInView({ threshold: 0.2 });

  useEffect(() => {
    if (inView) {
      animation.start('visible');
    } else {
      animation.start('hidden');
    }
  }, [animation, inView]);

  return (
    <Main ref={ref} fullMargin={fullMargin}>
      <Container>
        <Wrapper>
          {title && (
            <Title
              animate={animation}
              initial='hidden'
              transition={{ duration: 1 }}
              variants={{
                visible: {
                  opacity: 1,
                  x: 0,
                  transition: { type: 'spring', bounce: 0.1, stiffness: 55, delay: 0.3 },
                },
                hidden: {
                  opacity: 0,
                  x: '-200vw',
                  transition: { type: 'spring', stiffness: 55, delay: 0.3 },
                },
              }}
            >
              <Text text={title} fontSize='3rem' center />
            </Title>
          )}
          {description && (
            <Description
              animate={animation}
              initial='hidden'
              transition={{ duration: 1 }}
              variants={{
                visible: {
                  opacity: 1,
                  x: 0,
                  transition: { type: 'spring', stiffness: 40, delay: 0.3 },
                },
                hidden: {
                  opacity: 0,
                  x: '200vw',
                  transition: { type: 'spring', stiffness: 40, delay: 0.3 },
                },
              }}
            >
              <Inline>
                {highlightedText && (
                  <Text
                    text={highlightedText}
                    fontSize='1.25rem'
                    color='#FD1515'
                    center
                    weight='bold'
                  />
                )}
                <Text
                  text={
                    highlightedText ? description?.replace(highlightedText || '', '') : description
                  }
                  fontSize='1.25rem'
                  color='#EAECF0'
                  center
                />
              </Inline>
            </Description>
          )}
        </Wrapper>
        <ImageWrapper
          animate={animation}
          initial='hidden'
          transition={{ duration: 1.5 }}
          variants={{
            visible: {
              opacity: 1,
              x: 0,
              transition: { type: 'spring', stiffness: 50, delay: 0.6 },
            },
            hidden: {
              opacity: 0,
              x: '200vw',
              transition: { type: 'spring', stiffness: 50, delay: 0.6 },
            },
          }}
        >
          <Image src={imageUrl} />
          {floatingImageUrl && (
            <FloatingImageWrapper
              animate={animation}
              initial='imageHidden'
              transition={{ duration: 1 }}
              variants={{
                visible: {
                  opacity: 1,
                  transform: 'translate(0%,-50%)',
                  transition: { type: 'spring', stiffness: 70, delay: 1 },
                },
                hidden: {
                  opacity: 0,
                  transform: 'translate(-150%,-50%)',
                  transition: { type: 'spring', stiffness: 70, delay: 1 },
                },
              }}
            >
              <Image src={floatingImageUrl} />
            </FloatingImageWrapper>
          )}
          {bannerText && (
            <Banner>
              <Text text={bannerText} fontSize='3.5rem' weight='bold' />
            </Banner>
          )}
        </ImageWrapper>
      </Container>
      {Array.isArray(patch) ? patch.map((p) => p) : patch}
    </Main>
  );
};

export default ProductDetailSection;
