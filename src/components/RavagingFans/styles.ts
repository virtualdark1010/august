import { motion } from 'framer-motion';
import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  margin-bottom: 4rem;
  button {
    color: ${(p) => p.theme.palette.primary.red};
  }
  > span:nth-child(2) {
    width: 25%;
    margin: 2rem 0;
    text-align: center;
  }
`;

export const Wrapper = styled.div`
  width: 80%;
  height: 70vh;
  margin-top: 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
`;

export const CardWrapper = styled(motion.div)`
  display: flex;
  gap: 21px;
  flex-wrap: wrap;
  > :nth-child(1n),
  > :nth-child(2n),
  > :nth-child(3n) {
    flex: 1 0 calc(33.33% - 20px);
    gap: 25px;
    display: flex;
    flex-direction: column;
  }
  overflow: hidden;
`;

export const CardColumns = styled(motion.div)``;
