import styled from 'styled-components';

export const Main = styled.div`
  display: grid;
  grid-template-rows: auto;
  grid-template-columns: repeat(12, 1fr);
  gap: 0px;
  height: 100%;
  padding-bottom: 1rem;
  border-bottom: 1px solid ${(p) => p.theme.palette.common.white};
`;

export const Products = styled.div`
  grid-row-start: 1;
  grid-column-start: 2;
  grid-row-end: 2;
  grid-column-end: 6;
`;

export const Price = styled.div`
  grid-row-start: 1;
  grid-column-start: 9;
  grid-row-end: 2;
  grid-column-end: 10;
`;

export const Quantity = styled.div`
  grid-row-start: 1;
  grid-column-start: 10;
  grid-row-end: 2;
  grid-column-end: 11;
`;

export const Total = styled.div`
  grid-row-start: 1;
  grid-column-start: 11;
  grid-row-end: 2;
  grid-column-end: 12;
`;
