import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  display: flex;
  height: 100%;
  justify-content: space-around;
  align-items: center;
  background-color: ${(p) => p.theme.palette.background.primary};
  ${mobileMediaQuery(`flex-direction: column;`)}
`;

export const VerticalLine = styled.div`
  width: 4px;
  background-color: white;
  height: 100%;
  position: absolute;
  left: -50px;
  ${mobileMediaQuery(`
      width: 100%;
      height: 4px;
      top: 85px;
      left: 0;
  `)}
`;

export const ActiveLine = styled.div<{ index: number }>`
  width: 4px;
  background-color: #bb000e;
  height: 100%;
  transition: 250ms all;
  position: absolute;
  left: -50px;
  ${mobileMediaQuery(`
      width: 100%;
      height: 4px;
      top: 85px;
      left: 0;
  `)}
`;

export const LeftSection = styled.div`
  width: 70%;
  align-items: center;
  justify-content: center;
  display: flex;
`;

export const RightSection = styled.div`
  position: relative;
  width: 30%;
  height: 30%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  ${mobileMediaQuery(`
      flex-direction: row;
      width: 90%;
`)}
`;

export const VideoWrapper = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin-bottom: 1rem;
  iframe,
  video {
    border-radius: 24px;
  }
  > div {
    ${mobileMediaQuery(`
        width: 100vw !important;
        aspect-ratio: 1 !important;
    `)}
  }
`;

export const Row = styled.div<{ active: boolean }>`
  cursor: pointer;
  position: relative;
  display: flex;
  align-items: center;
  transition: 250ms all;
  ${mobileMediaQuery(`flex-direction: column;`)}
  ${(p) =>
    ` .progress-icon {
    g {
      fill: ${p.active ? p.theme.palette.primary.main : 'grey'};
    }
    path {
      stroke:${p.active ? p.theme.palette.primary.main : 'grey'};
    }
  }`}

  span {
    margin-left: 1rem;
  }
`;
