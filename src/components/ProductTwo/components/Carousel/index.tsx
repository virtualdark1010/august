import React from 'react';
import Slider from 'react-slick';
import { Main, Image } from './styles';

const settings = {
  dots: true,
  infinite: true,
  autoplay: false,
  arrows: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  customPaging: () => <div className='ft-slick__dots--custom' />,
};

const Carousel = ({ items, onChange }: { items: string[]; onChange?: (val: number) => void }) => {
  const afterChange = (val: number) => onChange?.(val);

  return (
    <Main className='carousel-wrapper'>
      <Slider {...settings} afterChange={afterChange}>
        {items.map((src) => (
          <Image src={src} className='carousel-image' />
        ))}
      </Slider>
    </Main>
  );
};

export { Carousel };
