import { motion } from 'framer-motion';
import styled from 'styled-components';

export const Container = styled(motion.div)`
  height: 40vh;
  width: 90%;
  margin: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 10rem;
  border-radius: 10px;
  background: linear-gradient(112.62deg, #ff1010 29.4%, #000000 105.15%);
  button {
    margin-top: 2rem;
  }
  @media screen and (max-width: 768px) {
    height: 20vh;
    margin-top: 2rem;
  }
`;
