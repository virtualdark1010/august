import { TextType } from './text';
import { ReactNode } from 'react';

export type CustomPosition = { top?: string; bottom?: string; left?: string; right?: string };

export type PatternPosition = string | 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight';

export type PatternType = {
  size: number;
  pattern?: string;
  icon?: ReactNode;
  customPosition?: CustomPosition;
  position?: PatternPosition;
};

export type LayoutType = {
  withHeader?: boolean;
  fullHeight?: boolean;
  bgColor?: string;
  overflow?: string;
  patterns?: PatternType[];
  heading?: TextType;
};
