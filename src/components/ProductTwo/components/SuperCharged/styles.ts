import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  align-items: center;
  position: relative;
  height: 100vh;
  overflow: hidden;
  ${mobileMediaQuery(`flex-direction: column; margin-top: 4rem;`)}
`;

export const TextWrapper = styled.div`
  width: inherit;
  display: flex;
  flex-direction: column;
`;

export const Flex = styled.div`
  display: flex;
  > div {
    margin-right: 2rem;
  }
  ${mobileMediaQuery(`align-items: center;`)}
`;

export const Image = styled(motion.img)`
  object-fit: cover;
`;

export const Stats = styled(motion.div)`
  background: #f1f5ff;
  padding: 2rem;
  border-radius: 30px;
  width: max-content;
  align-items: center;
  display: flex;
  flex-direction: column;
  margin-top: 4rem;
  > span {
    margin-top: 2rem;
    ${mobileMediaQuery(`margin: 0;`)}
  }
  span {
    ${mobileMediaQuery(`font-size: 0.75rem;`)}
  }
  ${mobileMediaQuery(`
      margin-top: 2rem;
      > span {
        margin-top: 1rem;
      }
      span {
        font-size: 0.75rem;
      }
  `)}
`;

export const LeftSection = styled.div`
  width: 60%;
  display: flex;
  justify-content: center;
  ${mobileMediaQuery(`width: 90%;`)}
`;

export const RightSection = styled.div`
  display: flex;
  justify-content: center;
  ${mobileMediaQuery(`width: 40%; margin-top: 2rem;`)}
`;
