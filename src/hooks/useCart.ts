import { useContext } from 'react';
import { ShopppingCartContext } from 'context/ShoppingCart';

export const useCart = () => {
  const ctx = useContext(ShopppingCartContext);
  if (!ctx) {
    throw new Error('Context must be called inside the provider');
  }

  return ctx;
};
