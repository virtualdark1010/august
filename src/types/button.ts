import { MouseEventHandler, ReactNode } from 'react';

export type ButtonType = {
  children: ReactNode | string;
  width?: string;
  height?: string;
  onClick: MouseEventHandler<HTMLButtonElement>;
  color?: string;
  borderRadius?: string;
  bgColor?: string;
  borderColor?: string;
  disabled?: boolean;
  variant?: 'default' | 'inverted';
  shape?: 'default' | 'square';
};
