import Card from 'components/shared/Card';
import Text from 'components/shared/Text';
import { getFontSize } from 'components/shared/Text/helper';
import { motion } from 'framer-motion';

import { withLayout } from 'hoc/withLayout';
import { isMobile } from 'react-device-detect';
import { EngineeringExcellenceCardContents } from './parts/data';

import {
  Container,
  CardContentContainer,
  CardContainer,
  Wrapper,
  MobileCardContainer,
} from './styles';

const EngineeringExcellence = () => {
  return (
    <Container>
      <Wrapper>
        <CardContainer>
          {EngineeringExcellenceCardContents.map((content) =>
            !isMobile ? (
              <motion.div
                initial={{
                  opacity: 0,
                }}
                whileInView={{
                  opacity: 1,
                  transition: {
                    duration: content?.duration,
                    type: 'spring',
                    delay: content?.delay,
                    stiffness: 10,
                  },
                }}
              >
                <Card bgColor='#0E0E0E' padding={2} key={content.id}>
                  <CardContentContainer>
                    {content.icon}
                    <Text text={content.heading} fontSize={getFontSize[6]} font-weight='bold' />
                    <Text text={content.description} />
                  </CardContentContainer>
                </Card>
              </motion.div>
            ) : (
              <MobileCardContainer className='M'>
                <Card bgColor='#0E0E0E' padding={2} key={content.id}>
                  <CardContentContainer>
                    {content.icon}
                    <Text text={content.heading} fontSize={getFontSize[6]} font-weight='bold' />
                    <Text text={content.description} />
                  </CardContentContainer>
                </Card>
              </MobileCardContainer>
            ),
          )}
        </CardContainer>
      </Wrapper>
    </Container>
  );
};

export default withLayout(EngineeringExcellence, {
  heading: {
    text: 'Engineering Excellence',
  },
});
