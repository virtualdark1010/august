import React, { useState } from 'react';
import Text from 'shared/Text';
import Dropdown from 'shared/Dropdown';
import { Cross } from 'shared/icons';

import { CartRowProps } from 'types/cart';

import { quantityOption } from './quantity';

import {
  Container,
  Products,
  ProductWrapper,
  ImageContainer,
  Price,
  Quantity,
  Total,
  Image,
  Remove,
  Icon,
} from './styles';

const CartRow = ({ image, name, price, quantity, onClick }: CartRowProps) => {
  const [total, setTotal] = useState<string>(quantity);
  const getTotal = Number(price) * Number(total);

  const handleOnRemove = () => onClick(true);

  return (
    <Container>
      <Products>
        <ProductWrapper>
          <ImageContainer>
            <Image src={image} />
          </ImageContainer>
          <Text text={name} color='white' level='4' />
        </ProductWrapper>
      </Products>
      <Price>
        <Text text={price} color='white' level='4' />
      </Price>
      <Quantity>
        <Dropdown options={quantityOption} placeholder={quantity} onChange={setTotal} />
      </Quantity>
      <Total>
        <Text text={getTotal.toFixed(2)} color='white' level='4' />
      </Total>
      <Remove>
        <Icon onClick={handleOnRemove}>
          <Cross />
        </Icon>
      </Remove>
    </Container>
  );
};

export default CartRow;
