import ProfileCards from 'components/shared/ProfileCards';
import { withLayout } from 'hoc/withLayout';
import { createRavingCards, variants } from './config';

import { Container, Wrapper, CardWrapper, CardColumns } from './styles';

const RavagingFans = () => {
  return (
    <Container>
      <Wrapper>
        <CardWrapper>
          <CardColumns variants={variants} animate='animateUp'>
            {createRavingCards(5, ProfileCards)}
          </CardColumns>
          <CardColumns variants={variants} animate='animateDown'>
            {createRavingCards(7, ProfileCards)}
          </CardColumns>
          <CardColumns variants={variants} animate='animateUp'>
            {createRavingCards(5, ProfileCards)}
          </CardColumns>
        </CardWrapper>
      </Wrapper>
    </Container>
  );
};

export default withLayout(RavagingFans, {
  heading: {
    text: 'Our Raving fans',
  },
});
