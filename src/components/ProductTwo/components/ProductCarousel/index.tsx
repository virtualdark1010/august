import React, { useState } from 'react';
import { Carousel } from '../Carousel';

import Text from 'shared/Text';

import productCarousel from 'assets/images/product-carousel.png';
import productCarouselDesign from 'assets/images/product-carousel-design.png';

import { useAnimate } from 'hooks/useAnimate';
import { AnimatePresence } from 'framer-motion';

import { Main, TextWrapper, Image, CarouselWrapper } from './styles';

const arrString = ['The tiny mighty action cam.', 'Slide 2', 'The tiny Slide 3', 'Slide 4'];

const ProductCarousel = () => {
  const [index, setIndex] = useState(0);
  const { ref, config } = useAnimate();

  const handlePrevSlide = (index: number) => setIndex(index);

  return (
    <Main>
      <TextWrapper ref={ref}>
        <AnimatePresence>
          <Text text={arrString[index]} fontSize='5rem' center color='#FFFFFF40' />
        </AnimatePresence>
      </TextWrapper>
      <CarouselWrapper {...config({})}>
        <Image src={productCarouselDesign} />
        <Carousel
          items={[productCarousel, productCarousel, productCarousel, productCarousel]}
          onChange={handlePrevSlide}
        />
      </CarouselWrapper>
    </Main>
  );
};

export { ProductCarousel };
