import { Coinbase, Dropbox, Slack, Spotify, Webflow } from 'components/shared/icons';
import Text from 'components/shared/Text';
import { Container, Wrapper } from './styles';

import { motion } from 'framer-motion';

const Banner = () => {
  return (
    <Container>
      <Text
        text='Used by the best'
        color='#ffffff'
        fontSize='4rem'
        weight='700'
        fontFamily='ClashDisplayBold'
      />
      <Wrapper>
        <motion.div
          initial={{
            opacity: 0,
            scale: 0.5,
          }}
          whileInView={{
            opacity: 1,
            scale: 1,
            transition: {
              duration: 3,
              type: 'spring',
              stiffness: 50,
              delay: 0.3,
            },
          }}
        >
          <Coinbase />
        </motion.div>
        <motion.div
          initial={{
            opacity: 0,
            scale: 0.5,
          }}
          whileInView={{
            opacity: 1,
            scale: 1,
            transition: {
              duration: 3,
              type: 'spring',
              stiffness: 50,
              delay: 0.6,
            },
          }}
        >
          <Spotify />
        </motion.div>
        <motion.div
          initial={{
            opacity: 0,
            scale: 0.5,
          }}
          whileInView={{
            opacity: 1,
            scale: 1,
            transition: {
              duration: 3,
              type: 'spring',
              stiffness: 50,
              delay: 0.9,
            },
          }}
        >
          <Slack />
        </motion.div>
        <motion.div
          initial={{
            opacity: 0,
            scale: 0.5,
          }}
          whileInView={{
            opacity: 1,
            scale: 1,
            transition: {
              duration: 3,
              type: 'spring',
              stiffness: 50,
              delay: 1.2,
            },
          }}
        >
          <Dropbox />
        </motion.div>
        <motion.div
          initial={{
            opacity: 0,
            scale: 0.5,
          }}
          whileInView={{
            opacity: 1,
            scale: 1,
            transition: {
              duration: 3,
              type: 'spring',
              stiffness: 50,
              delay: 1.5,
            },
          }}
        >
          <Webflow />
        </motion.div>
      </Wrapper>
    </Container>
  );
};

export default Banner;
