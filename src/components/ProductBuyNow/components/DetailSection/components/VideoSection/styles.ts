import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  margin-top: 4rem;
`;

export const VideoWrapper = styled.div`
  margin-top: 2rem;
`;

export const Videos = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin-bottom: 2rem;
  iframe,
  video {
    border-radius: 24px;
  }
  > div {
    ${mobileMediaQuery(`width: 100% !important;`)}
  }
`;
