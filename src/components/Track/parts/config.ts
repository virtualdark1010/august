const config = [
  {
    id: 1,
    stateName: 'Ordered',
    orderState: 'completed',
    iconDimensions: {
      height: 40,
      width: 40,
    },
    steps: [
      { id: 1, description: 'step 1', state: true },
      { id: 2, description: 'step 2', state: true },
      { id: 3, description: 'step 3', state: true },
    ],
  },
  {
    id: 2,
    stateName: 'Shipped',
    orderState: 'in progress',
    iconDimensions: {
      height: 50,
      width: 50,
    },
    steps: [
      { id: 1, description: 'step 1', state: true },
      { id: 2, description: 'step 2', state: true },
      { id: 3, description: 'step 3', state: false },
    ],
  },
  {
    id: 3,
    stateName: 'Out for delivery',
    orderState: 'queued',
    iconDimensions: {
      height: 30,
      width: 30,
    },
    steps: [
      { id: 1, description: 'step 1', state: false },
      { id: 2, description: 'step 2', state: false },
      { id: 3, description: 'step 3', state: false },
    ],
  },
];

export default config;
