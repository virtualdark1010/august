export type CartRowProps = {
  image: string;
  name: string;
  price: string;
  quantity: string;
  total: string;
  onClick: (event: any) => void;
};
