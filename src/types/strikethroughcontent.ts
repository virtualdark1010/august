export type StrikethroughTextType = {
  content: string;
  contentColor?: string;
};
