import Button from 'components/shared/Button';
import Card from 'components/shared/Card';
import Text from 'components/shared/Text';
import image from 'assets/images/product-1.png';
import { SubMenuProps, SubMenuContent, MenuType } from 'types/navigation';

import {
  Main,
  Badge,
  Conatainer,
  CardWrapper,
  CardContent,
  CardHeader,
  CardFooter,
  CardBody,
  Image,
  ImageWrapper,
} from './styles';

export const SubMenu = (props: SubMenuProps): any => {
  return (
    (props?.data || [])?.map((d: MenuType) => (
      <Conatainer key={d.title}>
        {d.title && <Text text={d.title} fontSize='2rem' weight='700' color='black' />}
        <CardWrapper>
          {(d?.contents || []).map((c: SubMenuContent) => (
            <Main key={c.title}>
              <Card padding={2} width='350px' height='350px' direction='column'>
                <CardContent>
                  <CardHeader>
                    {c.title && (
                      <Text text={c.title} color='white' fontSize='1.5rem' weight='700' />
                    )}
                    <Badge>{c.badge}</Badge>
                  </CardHeader>
                  {c.body && (
                    <CardBody>
                      <Text text={c.body} color='white' />
                    </CardBody>
                  )}
                </CardContent>
                <CardFooter>
                  {c.button && (
                    <Button onClick={() => {}} shape='square' variant='default'>
                      {c.button}
                    </Button>
                  )}
                  {c.image && (
                    <ImageWrapper>
                      <Image src={image} />
                    </ImageWrapper>
                  )}
                </CardFooter>
              </Card>
            </Main>
          ))}
        </CardWrapper>
      </Conatainer>
    )) || null
  );
};
