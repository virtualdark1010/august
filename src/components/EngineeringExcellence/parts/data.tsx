import { WalletIcon } from 'assets/icons/WalletIcon';

// Not created new type file as this would be removed post api integration
type EngineeringExcellenceCardContentsType = {
  id: number;
  heading: string;
  description: string;
  icon: JSX.Element;
  duration?: number;
  delay?: number;
}[];

export const EngineeringExcellenceCardContents: EngineeringExcellenceCardContentsType = [
  {
    id: 1,
    heading: 'Designed in India',
    description:
      'This growth plan will help you reach your resolutions and achieve the goals you have been striving towards.',
    icon: <WalletIcon />,
    duration: 1,
    delay: 0.75,
  },
  {
    id: 2,
    heading: 'Designed in India',
    description:
      'This growth plan will help you reach your resolutions and achieve the goals you have been striving towards.',
    icon: <WalletIcon />,
    duration: 1,
    delay: 1,
  },
  {
    id: 3,
    heading: 'Designed in India',
    description:
      'This growth plan will help you reach your resolutions and achieve the goals you have been striving towards.',
    icon: <WalletIcon />,
    duration: 1,
    delay: 1.25,
  },
  {
    id: 4,
    heading: 'Designed in India',
    description:
      'This growth plan will help you reach your resolutions and achieve the goals you have been striving towards.',
    icon: <WalletIcon />,
    duration: 1,
    delay: 1.5,
  },
  {
    id: 5,
    heading: 'Designed in India',
    description:
      'This growth plan will help you reach your resolutions and achieve the goals you have been striving towards.',
    icon: <WalletIcon />,
    duration: 1,
    delay: 1.75,
  },
  {
    id: 6,
    heading: 'Designed in India',
    description:
      'This growth plan will help you reach your resolutions and achieve the goals you have been striving towards.',
    icon: <WalletIcon />,
    duration: 1,
    delay: 2,
  },
];
