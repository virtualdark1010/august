import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  body  {
     margin: 0;
     font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu',
        'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;
     -webkit-font-smoothing: antialiased;
     -moz-osx-font-smoothing: grayscale;
    }
   #root {
       margin: 0 auto;
       box-sizing: border-box;
       overflow: hidden;
   }

   /* overwriting carousel dots / indicators */
   .dot {
      width: 20px !important;
      height: 2px !important;
      border-radius: 0 !important;
      box-shadow: none !important;
      background-color: black !important;
   }

   /* Clash Display font */
   @font-face {
      font-family: 'ClashDisplayRegular';
      src: url('src/assets/ClashDisplay/ClashDisplay-Regular.ttf');
      font-weight: 400;
   }
   @font-face {
      font-family: 'ClashDisplayBold';
      src: url('src/assets/fonts/ClashDisplay/ClashDisplay-Bold.ttf');
      font-weight: 800;
   }
   @font-face {
      font-family: 'ClashDisplaySemiBold';
      src: url('src/assets/fonts/ClashDisplay/ClashDisplay-Semibold.ttf');
      font-weight: 700;
   }
   @font-face {
      font-family: 'ClashDisplayLight';
      src: url('src/assets/fonts/ClashDisplay/ClashDisplay-Light.ttf');
      font-weight: 300;
   }

    /* Inter font */
   @font-face {
      font-family: 'InterRegular';
      src: url('src/assets/fonts/Inter/Inter-Regular.ttf');
      font-weight: 400;
   }
   @font-face {
      font-family: 'InterRegularExtraBold';
      src: url('src/assets/fonts/Inter/Inter-ExtraBold.ttf');
      font-weight: 900;
   }
   @font-face {
      font-family: 'InterExtraLight';
      src: url('src/assets/fonts/Inter/Inter-ExtraLight.ttf');
      font-weight: 200;
   }
   @font-face {
      font-family: 'InterRegularLight';
      src: url('src/assets/fonts/Inter/Inter-Light.ttf');
      font-weight: 300;
   }
   @font-face {
      font-family: 'InterRegularMedium';
      src: url('src/assets/fonts/Inter/Inter-Medium.ttf');
      font-weight: 500;
   }
   @font-face {
      font-family: 'InterRegularThin';
      src: url('src/assets/fonts/Inter/Inter-Thin.ttf');
      font-weight: 100;
   }

`;
