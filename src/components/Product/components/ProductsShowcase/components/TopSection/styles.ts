import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled(motion.div)`
  background-color: ${(p) => p.theme.palette.background.primary};
`;

export const Container = styled(motion.div)`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  ${mobileMediaQuery(`height: 40vh;`)}
  span {
    width: 50%;
    ${mobileMediaQuery(`
      width: 80%;
      font-size: 2rem;
    `)}
    &:nth-child(2) {
      margin: 2rem 0 1rem 0;
      ${mobileMediaQuery(`
       font-size: 1.75rem;
      `)}
    }
    &:nth-child(3) {
      ${mobileMediaQuery(`
       font-size: 1rem;
      `)}
    }
  }
`;
