export type ToggleType = {
  onClick?: (checked: boolean) => void;
  offLabel?: string;
  onLabel?: string;
  active?: boolean;
};
