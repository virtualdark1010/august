import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  padding: 4rem 5rem;
  display: flex;
  margin-bottom: 30vh;
  height: 90vh;
  position: relative;
  background-color: #bf0c0c;
  ${mobileMediaQuery(`padding: 2rem; flex-direction: column; margin-bottom: 0;`)}
`;

export const TextWrapper = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  .text-container:nth-child(2) {
    line-height: 5rem;
    margin-bottom: 3rem;
    margin-top: 1rem;
  }
  ${mobileMediaQuery(`
      width: 100%;
      .text-container:nth-child(2) {
        line-height: 3rem; 
        margin-bottom: 1rem;
      }
  `)}
`;

export const ImageOne = styled(motion.img)`
  object-fit: contain;
  margin-top: 8%;
  height: 95%;
  width: 50%;
  ${mobileMediaQuery(`width: 100%;`)}
`;

export const FloatingImage = styled(motion.img)`
  position: absolute;
  left: 50%;
  bottom: -35%;
  width: 48%;
  transform: translateX(-50%);
  ${mobileMediaQuery(`display:none;`)}
`;
