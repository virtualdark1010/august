import { socialLinks } from 'shared/Footer/config';
import { Container, Icon } from './styles';

export const Socials = () => {
  return (
    <Container>
      {socialLinks.map((s) => (
        <Icon> {s.icon}</Icon>
      ))}
    </Container>
  );
};
