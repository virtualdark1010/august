export type ProductModalType = {
  hasPlayButton?: boolean;
  modalHeading?: string;
  modalText?: string;
  modalVideoUrl?: string;
  productUrl?: string;
  videoWidth?: number;
  videoHeight?: number;
};

export type ProductTileType = {
  bgColor?: string;
  title: string;
  buttonText: string;
  image: string;
};
