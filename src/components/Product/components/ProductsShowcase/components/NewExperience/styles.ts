import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.div`
  background-color: ${(p) => p.theme.palette.background.primary};
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: column;
  margin-bottom: 5rem;
`;

export const GridWrapper = styled(motion.div)`
  margin: 40vh 0;
  ${mobileMediaQuery(`margin: 10vh 0 0 0;`)}
`;

export const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  position: relative;
  width: 60vw;
  margin: 10vw 0 5vw 0;
  ${mobileMediaQuery(`width: 90vw;`)}
  span {
    margin-right: 4px;
  }
`;

export const Inline = styled(motion.p)<{ width?: string; offSet?: boolean }>`
  width: ${({ width }) => width || '30%'};
  text-align: center;
  ${(p) => p?.offSet && 'transform: translateX(-150px)'};
  > span:nth-child(1) {
    display: inline-block;
  }
  ${mobileMediaQuery(`width: 90%;`)}
`;

export const ImageWrapper = styled(motion.div)`
  position: absolute;
  right: 6%;
  top: -10%;
  ${mobileMediaQuery(`
      display: flex;
      justify-content: flex-end;
      position: absolute;
      right: -6vw;
      top: -55%;
      `)}
`;

export const AudioOneRSWrapper = styled(motion.div)`
  display: flex;
  align-items: center;
  flex-direction: column;
  position: relative;
  .toggle {
    margin: 2rem 0 1rem 0;
  }
`;

export const AudioOneRS = styled(motion.div)`
  display: flex;
  align-items: center;
  ${mobileMediaQuery(`
  justify-content: center;
  padding-left: 2rem;
  `)}
`;

export const Wave2 = styled(motion.div)`
  transform: translateX(-98%);
  z-index: 2;
`;

export const BatterySection = styled(motion.div)`
  width: 100vw;
  display: flex;
  margin: 5rem 0;
  flex-direction: column;
  position: relative;
  height: 35vh;
  align-items: center;
  span {
    width: 80%;
    &:nth-child(1) {
      ${mobileMediaQuery(`
        font-size: 1.5rem;
        font-weight: bold;
    `)}
    }
    &:nth-child(2) {
      ${mobileMediaQuery(`
        font-size: 1rem;
        margin-top: 2rem;
    `)}
    }
  }
  img {
    position: absolute;
    right: 0;
    bottom: 0;
    width: 50%;
    ${mobileMediaQuery(`width: 75%;`)}
  }
`;

export const AnimationWrapper = styled(motion.div)`
  width: 100%;
`;

export const LeftColumn = styled(motion.div)`
  z-index: 4;
  height: 80vh;
  ${mobileMediaQuery(`height: 100%;`)}
`;

export const LeftColumnWrapper = styled(motion.div)`
  display: flex;
  flex-direction: column;
  background-color: #1e1e1e;
  justify-content: center;
  padding: 0 8rem 0 4rem;
  z-index: 4;
  padding-bottom: 5rem;
  height: 100%;
  ${mobileMediaQuery(`padding: 2rem 2rem;`)}
`;

export const Image = styled(motion.img)<{
  width?: string;
  height?: string;
  borderRadius?: string;
  x?: string;
  z?: number;
}>`
  object-fit: cover;
  width: ${({ width = '30vw' }) => width};
  height: ${({ height }) => height};
  z-index: ${({ z = 2 }) => z};
  transform: ${(p) => `translateX(${p?.x})`};
`;
