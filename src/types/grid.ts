export type GridType = {
  children: any;
  bgColor?: string;
  borderRadius?: number;
  column?: number;
  row?: string | number;
  padding?: string;
  gridGap?: string;
  start?: number;
  end?: number;
  rowStart?: number;
  rowEnd?: number;
  height?: string;
};
