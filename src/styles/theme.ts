import { DefaultTheme } from 'styled-components';

export default {
  dropShadow: 'box-shadow: 0px 0px 10px 0px rgb(0 0 0 / 50%)',
  headerShadow: 'box-shadow: 5px 5px 4px rgba(0, 0, 0, 0.25)',
  cardShadow: '2px 4px 10px 2px rgba(0, 0, 0, 0.25)',
  borderRadius: '20px',
  fullRadius: '100%',
  border: '0.4px solid #1c1c1c',
  palette: {
    common: {
      black: '#1c1c1c',
      white: '#ffffff',
      transparent: 'transparent',
    },
    primary: {
      main: '#FD1515',
      red: '#ff0000',
      contrastText: '#ffffff',
    },
    secondary: {
      main: '#E31C25',
      contrastText: '#ffffff',
    },
    background: {
      primary: '#000000',
      secondary: '#252424',
    },
    button: {
      variant: {
        default: { color: '#ffffff', borderColor: '#FD1515', background: '#FD1515' },
        inverted: { color: '#FD1515', borderColor: '#ffffff', background: '#ffffff' },
      },
      shape: {
        default: { borderRadius: '20px' },
        square: { borderRadius: '0px' },
      },
    },
    font: {
      weight: {
        normal: 'normal',
        bold: 'bold',
      },
    },
  },
} as DefaultTheme;
