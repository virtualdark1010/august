export const GlowingPatch = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width='1543'
      height='1543'
      viewBox='0 0 1543 1543'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <g filter='url(#filter0_f_503_79)'>
        <circle cx='771.5' cy='771.5' r='277.5' fill='#DA2222' fill-opacity='0.59' />
      </g>
      <defs>
        <filter
          id='filter0_f_503_79'
          x='0'
          y='0'
          width='1543'
          height='1543'
          filterUnits='userSpaceOnUse'
          color-interpolation-filters='sRGB'
        >
          <feFlood flood-opacity='0' result='BackgroundImageFix' />
          <feBlend mode='normal' in='SourceGraphic' in2='BackgroundImageFix' result='shape' />
          <feGaussianBlur stdDeviation='247' result='effect1_foregroundBlur_503_79' />
        </filter>
      </defs>
    </svg>
  );
};
