import { Coinbase, Dropbox, Slack, Spotify, Webflow } from 'components/shared/icons';

export const marqueeList = [
  [Coinbase, Slack, Spotify, Dropbox, Webflow, Slack, Coinbase, Spotify, Dropbox, Webflow, Slack],
  [
    Slack,
    Webflow,
    Spotify,
    Slack,
    Coinbase,
    Dropbox,
    Webflow,
    Slack,
    Spotify,
    Coinbase,
    Dropbox,
    Webflow,
  ],
  [Dropbox, Slack, Spotify, Webflow, Dropbox, Coinbase, Slack, Spotify, Dropbox],
];
