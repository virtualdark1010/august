import { withLayout } from 'hoc/withLayout';

import ImageSection from 'components/ProductBuyNow/components/ImageSection';
import DetailSection from 'components/ProductBuyNow/components/DetailSection';

import { Container } from './styles';

const ProductBuyNow = () => {
  return (
    <Container>
      <ImageSection />
      <DetailSection />
    </Container>
  );
};

export default withLayout(ProductBuyNow, { fullHeight: true });
