import React from 'react';
import ProductItem from '../ProductItem';
import products from './config';

import { Container } from './styles';

const ActionTab = () => {
  return (
    <Container>
      <ProductItem data={products} />
    </Container>
  );
};

export default ActionTab;
