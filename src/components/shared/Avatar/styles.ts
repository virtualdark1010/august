import styled from 'styled-components';
import { AvatarType } from 'types/avatar';

export const Container = styled.div<AvatarType>`
  padding: 0.5rem;
  margin: 1rem;
  ${(p) => {
    const { theme, width, height, bgColor, initials } = p;
    return `
         width: ${width || '50px'};
         height: ${height || '50px'};
         color: ${initials ? theme.palette.common.white : ''};
         background: ${bgColor || theme.palette.background.secondary};
         border-radius: ${theme.fullRadius};
         ${theme.dropShadow}
    `;
  }};
`;
