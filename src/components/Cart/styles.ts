import styled from 'styled-components';

export const Main = styled.div`
  padding: 0 5rem;
  margin-top: 5rem;
`;

export const Container = styled.div`
  max-height: 55vh;
  overflow: scroll;
  box-shadow: 0px 4px 10px 0px rgb(255 255 255 / 20%);
  .portal-modal {
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: max-content;
  }
`;

export const ModalWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center; ;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-top: 1rem;
  button:first-child {
    margin-right: 1rem;
  }
`;
