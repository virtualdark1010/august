import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  display: flex;
  width: 95vw;
  flex-wrap: wrap;
  ${mobileMediaQuery(`max-height: 80vh; overflow: auto;`)}
`;
