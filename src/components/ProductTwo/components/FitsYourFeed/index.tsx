import React from 'react';

import Text from 'shared/Text';
import feeds from 'assets/images/fit-your-feed.png';
import feedImage from 'assets/images/fit-your-feed-image.png';

import { Main, TextWrapper, Image, FloatingImage } from './styles';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const FitsYourFeed = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <TextWrapper>
        <Text
          text='Fits in your feed.'
          fontSize={isMobile ? '2rem' : '5rem'}
          weight='bold'
          color='#1E1E1E'
          center={isMobile}
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  rotate: 360,
                  y: 0,
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.5 },
                },
                hidden: {
                  opacity: 0,
                  rotate: 0,
                  y: '-100vw',
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.5 },
                },
              },
            },
          })}
        />
        <Text
          text='Choose from 16:9, 9:16 or 1:1 aspect ratios to make sure your
          shot looks good anywhere you post it.'
          fontSize={isMobile ? '1rem' : '1.75rem'}
          color='#1E1E1E60'
          center={isMobile}
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  rotate: 360,
                  y: 0,
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.7 },
                },
                hidden: {
                  opacity: 0,
                  rotate: 0,
                  y: '100vw',
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.7 },
                },
              },
            },
          })}
        />
      </TextWrapper>
      <Image
        src={feeds}
        // width={}
        {...config({
          customConfig: {
            variants: {
              visible: {
                opacity: 1,
                rotate: 360,
                y: 0,
                transition: { duration: 2.5, type: 'spring', stiffness: 50, delay: 1 },
              },
              hidden: {
                opacity: 0,
                rotate: 0,
                y: '100vw',
                transition: { duration: 2.5, type: 'spring', stiffness: 50, delay: 1 },
              },
            },
          },
        })}
      />
      <FloatingImage>
        <Image
          src={feedImage}
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  y: 0,
                  transition: { duration: 2.5, delay: 1.2 },
                },
                hidden: {
                  opacity: 0,
                  transition: { duration: 2.5, delay: 1.2 },
                },
              },
            },
          })}
        />
      </FloatingImage>
    </Main>
  );
};

export { FitsYourFeed };
