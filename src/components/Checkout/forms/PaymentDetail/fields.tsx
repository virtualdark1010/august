import Dropdown from 'shared/Dropdown';
import Input from 'shared/Input';

export const fields = [
  {
    width: 'full',
    props: { label: 'Cardholder Name', name: 'name', type: 'text' },
    Render: Input,
  },
  {
    width: 'full',
    props: { label: 'Card Number', name: 'cardNumber', type: 'text' },
    Render: Input,
  },
  {
    width: 'quarter',
    props: {
      options: [{ value: 'january', label: 'January' }],
      label: 'Month',
    },
    Render: Dropdown,
  },
  {
    width: 'quarter',
    props: {
      options: [{ value: '2022', label: '2022' }],
      label: 'Year',
    },
    Render: Dropdown,
  },
  {
    width: 'quarter',
    props: { label: 'CVV', name: 'cvv', type: 'text' },
    Render: Input,
  },
];
