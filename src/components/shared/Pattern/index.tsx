import { PatternType } from 'types/layout';
import { Container, Row } from './styles';

export const Pattern = ({
  size = 6,
  pattern,
  icon,
  position,
  customPosition,
}: Partial<PatternType>) => {
  const renderRow = Array.from({ length: size }).map((_) => icon || <span>{pattern || '+'}</span>);
  const renderColumn = Array.from({ length: size }).map((_, i) => <Row key={i}>{renderRow}</Row>);

  return size ? (
    <Container position={position} customPosition={customPosition}>
      {renderColumn}
    </Container>
  ) : null;
};
