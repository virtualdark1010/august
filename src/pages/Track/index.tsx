import { withLayout } from 'hoc/withLayout';
import { withHF } from 'hoc/withHF';
import Track from 'components/Track';

const TrackPage = () => <Track />;

export default withHF(withLayout(TrackPage, { fullHeight: true }));
