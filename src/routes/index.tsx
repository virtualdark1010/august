import { BrowserRouter, Routes as ReactRoutes, Route, RouteProps } from 'react-router-dom';
import routes from './configs';

const Routes = () => (
  <BrowserRouter>
    <ReactRoutes>
      {routes.map((route: RouteProps) => (
        <Route key={route.path} {...route} />
      ))}
    </ReactRoutes>
  </BrowserRouter>
);

export default Routes;
