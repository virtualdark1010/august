import styled from 'styled-components';

export const Main = styled.section`
  background-color: ${(p) => p.theme.palette.background.primary};
  overflow: hidden;
  position: relative;
  .grid-container {
    margin: 5rem 0;
  }
`;

export const Slide = styled.div``;
