import { useOnScreen } from 'hooks/useOnScreen';
import { useEffect, useRef, useState } from 'react';

export const useSetActiveSlide = () => {
  const lensesElementRef = useRef<any>(null);
  const topFeatureElementRef = useRef<any>(null);
  const newExperienceElementRef = useRef<any>(null);
  const editingElementRef = useRef<any>(null);

  const isLensesVisible = useOnScreen(lensesElementRef);
  const isTopFeatureVisible = useOnScreen(topFeatureElementRef);
  const isNewExperienceVisible = useOnScreen(newExperienceElementRef);
  const isEditingVisible = useOnScreen(editingElementRef);

  const [activeSlide, setActiveSlide] = useState<string | undefined>('');

  useEffect(() => {
    if (isLensesVisible) {
      setActiveSlide(lensesElementRef?.current?.className?.split(' ')[1]);
    }
    if (isTopFeatureVisible) {
      setActiveSlide(topFeatureElementRef?.current?.className?.split(' ')[1]);
    }
    if (isNewExperienceVisible) {
      setActiveSlide(newExperienceElementRef?.current?.className?.split(' ')[1]);
    }
    if (isEditingVisible) {
      setActiveSlide(editingElementRef?.current?.className?.split(' ')[1]);
    }
  }, [isLensesVisible, isTopFeatureVisible, isNewExperienceVisible, isEditingVisible]);

  const slides = [
    {
      value: 'lenses',
      label: 'Lenses',
      ref: lensesElementRef,
    },
    {
      value: 'topFeatures',
      label: 'Top Features',
      ref: topFeatureElementRef,
    },
    {
      value: 'newExperience',
      label: 'New Experience',
      ref: newExperienceElementRef,
    },
    {
      value: 'editing',
      label: 'Editing',
      ref: editingElementRef,
    },
  ];

  const hideSlider = !(
    !isLensesVisible &&
    !isTopFeatureVisible &&
    !isNewExperienceVisible &&
    !isEditingVisible
  );

  return {
    slides,
    hideSlider,
    activeSlide,
    lensesElementRef,
    topFeatureElementRef,
    newExperienceElementRef,
    editingElementRef,
  };
};
