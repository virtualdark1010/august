import styled from 'styled-components';

export const Container = styled.div`
  display: grid;
  grid-template-rows: auto;
  grid-template-columns: repeat(12, 1fr);
  gap: 0px;
  height: 100%;
  align-items: center;
  padding: 2rem 0;
  border-bottom: 0.5px solid ${(p) => p.theme.palette.common.white};
`;

export const Products = styled.div`
  grid-row-start: 1;
  grid-column-start: 2;
  grid-row-end: auto;
  grid-column-end: 6;
`;

export const ProductWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Price = styled.div`
  grid-row-start: 1;
  grid-column-start: 9;
  grid-row-end: auto;
  grid-column-end: 10;
`;
export const Quantity = styled.div`
  grid-row-start: 1;
  grid-column-start: 10;
  grid-row-end: auto;
  grid-column-end: 11;
  div[class*='-container'] {
    width: 65%;
  }
`;

export const Total = styled.div`
  grid-row-start: 1;
  grid-column-start: 11;
  grid-row-end: auto;
  grid-column-end: 12;
`;

export const Remove = styled.div`
  grid-row-start: 1;
  grid-column-start: 12;
  grid-row-end: auto;
  grid-column-end: 13;
  transition: 250ms all;
  path {
    stroke: white;
  }
  &:hover {
    transition: 250ms all;
    transform: scale(1.05);
    g,
    path {
      stroke: ${(p) => p.theme.palette.primary.main};
    }
  }
`;

export const Icon = styled.div`
  cursor: pointer;
  
`;

export const ImageContainer = styled.div`
  width: 75px;
  height: 75px;
  position: relative;
`;

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  margin-right: 1rem;
`;
