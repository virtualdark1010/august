import ReactPlayer from 'react-player';
import Text from 'components/shared/Text';
import { ShowMore } from '../ShowMore';

import { Container, VideoWrapper, Videos } from './styles';

export const VideoSection = ({ videoList, title }: { videoList: string[]; title: string }) => {
  return (
    <Container>
      <Text className='pl-6' text={title} color='#FD1515' fontSize='1.5rem' weight='700' />
      <VideoWrapper>
        {videoList?.map((videoUrl) => (
          <Videos key={videoUrl}>
            <ReactPlayer url={videoUrl} muted />
          </Videos>
        ))}
      </VideoWrapper>
      <ShowMore />
    </Container>
  );
};
