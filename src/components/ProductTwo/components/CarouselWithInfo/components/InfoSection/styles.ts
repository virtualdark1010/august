import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled(motion.div)<{ bgColor?: string }>`
  background: ${({ bgColor }) => bgColor || '#0061ffb2'};
  width: 55%;
  padding: 3rem 4rem;
  display: flex;
  flex-direction: column;
  position: relative;
  ${mobileMediaQuery(`
    width: 100%;
    padding: 2rem;
  `)}
`;

export const Image = styled(motion.img)<{ floatingImage?: boolean }>`
  align-self: center;
  object-fit: cover;
  ${({ floatingImage }) =>
    floatingImage
      ? `
  position: absolute;
  left: 50%;
  bottom: -20%;
  transform: translateX(-50%);
  width: 100%;
  `
      : 'max-width: 80%;'}
  margin-top: 1.5rem;
  ${mobileMediaQuery(`
    position: relative;
    left: 0;
    transform: translateX(0);
  `)}
`;
