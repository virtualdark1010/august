import ActionTab from './components/ProductList/components/ActionTab';
import StaticTab from './components/ProductList/components/StaticTab';

export const productList = [
  { label: 'Actions', Render: ActionTab, shape: 'square' },
  {
    label: 'Static',
    Render: StaticTab,
    shape: 'square',
  },
];
