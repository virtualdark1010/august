export type InfoSectionType = {
  floatingImage?: boolean;
  infoSectionImage: string;
  title?: string;
  subTitle?: string;
  description?: string;
  bgColor?: string;
};
