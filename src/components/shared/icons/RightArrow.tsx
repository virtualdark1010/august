export const RightArrow = () => (
  <svg width='25' height='25' viewBox='0 0 25 25' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M5.24377 12.3856H19.2438'
      stroke='#FD1515'
      stroke-width='2'
      stroke-linecap='round'
      stroke-linejoin='round'
    />
    <path
      d='M12.2438 5.38556L19.2438 12.3856L12.2438 19.3856'
      stroke='#FD1515'
      stroke-width='2'
      stroke-linecap='round'
      stroke-linejoin='round'
    />
  </svg>
);
