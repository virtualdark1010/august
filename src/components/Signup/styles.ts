import styled from 'styled-components';

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 2rem;
`;

export const TextContainer = styled.div`
  span {
    font-size: 4rem;
  }
`;

export const LogoContainer = styled.div`
  margin-bottom: 5rem;
`;

export const SocialLinkContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 1rem 0;
  gap: 0rem 3rem;
`;

export const HeadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  text-align: center;
  justify-content: center;
  align-items: center;
  span {
    width: 70%;
    line-height: 4rem;
  }
  /* span:nth-child(1) {
    font-size: 4rem;
  } */
  @media only screen and (max-width: 768px) {
    span:nth-child(1) {
      line-height: 2rem;
      font-size: 2rem;
    }
    span:nth-child(2) {
      line-height: 1rem;
      font-size: 0.7rem;
    }
  }
`;

export const FormWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  gap: 0rem 3rem;
  justify-content: center;
  align-items: center;
  @media only screen and (max-width: 768px) {
    padding: 0;
    div:nth-child(1) {
      width: 100%;
      margin-bottom: 0;
    }
    div:nth-child(2) {
      margin-top: 0;
      width: 100%;
    }
  }
`;

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FormGroup = styled.div`
  position: relative;
  width: 70%;
  margin: 1rem 0;
  gap: 2rem;
  display: flex;
  flex-direction: row;
  margin: 1rem 0;
  div {
    width: 50%;
  }
  @media only screen and (max-width: 768px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 0rem;
  }
`;

export const LeftSide = styled.div`
  flex: 2;
  align-items: center;
  display: flex;
  justify-content: center;
  border-right: 1px solid #ccc;
  height: 100vh;
  @media only screen and (max-width: 768px) {
    display: none;
  }
`;

export const RightSide = styled.div`
  flex: 3;
  align-items: center;
  display: flex;
  justify-content: center;
  height: 100vh;
  padding: 0 5rem;
  background-color: ${(p) => p.theme.palette.background.primary};
  flex-direction: column;
  @media only screen and (max-width: 768px) {
    width: 100%;
    flex: 1;
    padding: 0 3rem;
  }
`;

export const Main = styled.div``;
