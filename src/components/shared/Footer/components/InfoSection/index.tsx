import { Container, Link, Column } from './styles';
import { infoConfig } from 'components/shared/Footer/config';

const InfoSection = () => {
  const renderInfoSection = infoConfig.map(({ label, icon }) => {
    return icon || <Link key={label}>{label}</Link>;
  });

  return (
    <Container>
      <Column>{renderInfoSection}</Column>
    </Container>
  );
};

export default InfoSection;
