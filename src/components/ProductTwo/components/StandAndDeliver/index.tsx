import React from 'react';

import Text from 'shared/Text';
import bgOne from 'assets/images/stand-and-deliver-bg-1.png';
import imageOne from 'assets/images/stand-and-deliver-image-1.png';
import bgTwo from 'assets/images/stand-and-deliver-bg-2.png';
import imageTwo from 'assets/images/stand-and-deliver-image-2.png';

import { Main, TextWrapper, Flex, Image, Content, RightSection } from './styles';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const StandAndDeliver = () => {
  const { ref, config } = useAnimate();
  const { ref: lowerSectionRef, config: lowerSectionConfig } = useAnimate();
  return (
    <Main ref={ref}>
      <Flex>
        <Content>
          <TextWrapper>
            <Text
              text='Stand and deliver.'
              fontSize={isMobile ? '2rem' : '5rem'}
              weight='bold'
              color='#1E1E1E'
              center={isMobile}
              {...config({
                customConfig: {
                  variants: {
                    visible: {
                      opacity: 1,
                      y: 0,
                      transition: { duration: 0.7, type: 'spring', delay: 0.5 },
                    },
                    hidden: {
                      opacity: 0,
                      y: '-100vw',
                      transition: { duration: 0.7, type: 'spring', delay: 0.5 },
                    },
                  },
                },
              })}
            />
            <Text
              text='Fold out the tripod legs and stand GO 2 up for easy tabletop angles.'
              fontSize={isMobile ? '0.875rem' : '1.75rem'}
              center={isMobile}
              color='#1E1E1E60'
              {...config({
                customConfig: {
                  variants: {
                    visible: {
                      opacity: 1,
                      y: 0,
                      transition: { duration: 1, type: 'spring', delay: 0.7 },
                    },
                    hidden: {
                      opacity: 0,
                      y: '-100vw',
                      transition: { duration: 1, type: 'spring', delay: 0.7 },
                    },
                  },
                },
              })}
            />
            <Image
              src={bgOne}
              {...config({
                customConfig: {
                  variants: {
                    visible: {
                      opacity: 1,
                      y: 0,
                      transition: { duration: 1, type: 'spring', delay: 1.2 },
                    },
                    hidden: {
                      opacity: 0,
                      y: '-100vw',
                      transition: { duration: 1, type: 'spring', delay: 1.2 },
                    },
                  },
                },
              })}
            />
          </TextWrapper>
        </Content>
        <RightSection>
          <Image
            src={imageOne}
            {...config({
              customConfig: {
                variants: {
                  visible: {
                    opacity: 1,
                    y: 0,
                    transition: { duration: 1, type: 'spring', delay: 1.5 },
                  },
                  hidden: {
                    opacity: 0,
                    y: '100vw',
                    transition: { duration: 1, type: 'spring', delay: 1.5 },
                  },
                },
              },
            })}
          />
        </RightSection>
      </Flex>
      <Flex reverse ref={lowerSectionRef}>
        <Content>
          <TextWrapper>
            <Text
              text='Snap a selfie.'
              fontSize={isMobile ? '2rem' : '5rem'}
              weight='bold'
              color='#1E1E1E'
              center={isMobile}
              {...lowerSectionConfig({
                customConfig: {
                  variants: {
                    visible: {
                      opacity: 1,
                      y: 0,
                      transition: { duration: 0.7, type: 'spring', delay: 0.5 },
                    },
                    hidden: {
                      opacity: 0,
                      y: '100vw',
                      transition: { duration: 0.7, type: 'spring', delay: 0.5 },
                    },
                  },
                },
              })}
            />
            <Text
              text='Grip the case in your palm or even screw in a selfie stick.'
              fontSize={isMobile ? '0.875rem' : '1.75rem'}
              color='#1E1E1E60'
              center={isMobile}
              {...lowerSectionConfig({
                customConfig: {
                  variants: {
                    visible: {
                      opacity: 1,
                      y: 0,
                      transition: { duration: 1, type: 'spring', delay: 0.7 },
                    },
                    hidden: {
                      opacity: 0,
                      y: '100vw',
                      transition: { duration: 1, type: 'spring', delay: 0.7 },
                    },
                  },
                },
              })}
            />
            <Image
              src={bgTwo}
              {...lowerSectionConfig({
                customConfig: {
                  variants: {
                    visible: {
                      opacity: 1,
                      y: 0,
                      transition: { duration: 1, type: 'spring', delay: 1.2 },
                    },
                    hidden: {
                      opacity: 0,
                      y: '100vw',
                      transition: { duration: 1, type: 'spring', delay: 1.2 },
                    },
                  },
                },
              })}
            />
          </TextWrapper>
        </Content>
        <RightSection>
          <Image
            src={imageTwo}
            {...lowerSectionConfig({
              customConfig: {
                variants: {
                  visible: {
                    opacity: 1,
                    y: 0,
                    transition: { duration: 0.7, type: 'spring', delay: 0.5 },
                  },
                  hidden: {
                    opacity: 0,
                    y: '-100vw',
                    transition: { duration: 0.7, type: 'spring', delay: 0.5 },
                  },
                },
              },
            })}
          />
        </RightSection>
      </Flex>
    </Main>
  );
};

export { StandAndDeliver };
