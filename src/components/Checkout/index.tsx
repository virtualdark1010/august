import { useCart } from 'hooks/useCart';
import React from 'react';
import Stepper from 'shared/Stepper';

import Text from 'shared/Text';
import checkoutConfig from './config';

import { Main } from './styles';

const Checkout = () => {
  const { step, setStep } = useCart();
  const RenderComponent = checkoutConfig[step || 'Address'];

  return (
    <Main>
      <Text text='Checkout' level='6' color='white' weight='bold' />
      <Stepper steps={['Address', 'Shipping', 'Payment']} activeStep={step} onClick={setStep} />
      <RenderComponent />
    </Main>
  );
};

export default Checkout;
