import NoiseVideo from 'components/Product/components/NoiseCancellation/components/NoiseVideo';
import NoiseInfo from 'components/Product/components/NoiseCancellation/components/NoiseInfo';

import { Container, LeftSection, RightSection } from './styles';
import { withLayout } from 'hoc/withLayout';

const NoiseCancellation = () => (
  <Container>
    <LeftSection className='flex items-center justify-center'>
      <NoiseVideo />
    </LeftSection>
    <RightSection className='flex items-center justify-center'>
      <NoiseInfo />
    </RightSection>
  </Container>
);

export default withLayout(NoiseCancellation, { fullHeight: true });
