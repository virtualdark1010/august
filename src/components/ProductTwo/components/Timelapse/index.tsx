import React from 'react';

import { Main, LeftSection, TextWrapper, FloatingImage, Image, ImageWrapper } from './styles';
import Text from 'shared/Text';
import timelapseImageTwo from 'assets/images/timelapse-image-2.png';
import timelapseImageOne from 'assets/images/timelapse-image-1.png';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const Timelapse = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <LeftSection>
        <TextWrapper>
          <Text
            text='Timelapse'
            fontSize='2rem'
            center={isMobile}
            weight='bold'
            color='#BF0C0C'
            {...config({
              customConfig: {
                variants: {
                  visible: {
                    opacity: 1,
                    transition: { duration: 2, delay: 0.3 },
                  },
                  hidden: {
                    opacity: 0,
                    transition: { duration: 2, delay: 0.3 },
                  },
                },
              },
            })}
          />
          <Text
            text='Make Time fly by.'
            fontSize={isMobile ? '2rem' : '5rem'}
            weight='bold'
            color='#1E1E1E'
            center={isMobile}
            {...config({
              customConfig: {
                variants: {
                  visible: {
                    opacity: 1,
                    transition: { duration: 2, delay: 0.5 },
                  },
                  hidden: {
                    opacity: 0,
                    transition: { duration: 2, delay: 0.5 },
                  },
                },
              },
            })}
          />
          <Text
            text='Pick a spot and record a timelapse from any angle. At night, turn on Nightlapse mode and GO 2 will use its brains (and AI) to adjust the exposure and intervals. It’ll blow your socks off.'
            fontSize={isMobile ? '1rem' : '1.75rem'}
            color='#1E1E1E60'
            center={isMobile}
            {...config({
              customConfig: {
                variants: {
                  visible: {
                    opacity: 1,
                    transition: { duration: 2, delay: 1 },
                  },
                  hidden: {
                    opacity: 0,
                    transition: { duration: 2, delay: 1 },
                  },
                },
              },
            })}
          />
        </TextWrapper>
      </LeftSection>
      <ImageWrapper>
        <FloatingImage>
          <Image src={timelapseImageOne} {...config({ leftToRight: true })} />
        </FloatingImage>
        <Image src={timelapseImageTwo} {...config({})} />
      </ImageWrapper>
    </Main>
  );
};

export { Timelapse };
