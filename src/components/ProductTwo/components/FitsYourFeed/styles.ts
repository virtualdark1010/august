import styled from 'styled-components';
import image from 'assets/images/hdr-bg.png';
import { motion } from 'framer-motion';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 5rem 0;
  position: relative;
  background-image: url(${image});
  background-repeat: no-repeat;
  background-size: cover;
  margin-bottom: 30vh;
  height: 100vh;
  ${mobileMediaQuery('padding: 2rem 0; margin-bottom: 0;')}
`;

export const TextWrapper = styled.div`
  width: 60%;
  display: flex;
  flex-direction: column;
  align-items: center;
  overflow: hidden;
  ${mobileMediaQuery(`
    width: 100%; 
    padding:0 1rem;
    span:nth-child(2){
      margin-top: 1rem;
    }
    `)}
`;

export const Image = styled(motion.img)`
  object-fit: cover;
  margin-top: 4rem;
  ${mobileMediaQuery('margin-top: 3rem;')}
`;

export const FloatingImage = styled.div`
  position: absolute;
  bottom: -20%;
  ${mobileMediaQuery('position: relative; bottom: 0%;')}
`;
