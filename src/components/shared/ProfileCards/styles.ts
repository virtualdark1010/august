import styled from 'styled-components';

export const CardContainer = styled.div`
  background: white;
  padding: 2rem;
  border-radius: 10px;
  gap: 15px;
  display: flex;
  flex-direction: column;
`;

export const CardHeader = styled.div`
  display: flex;
  gap: 15px;
  justify-content: flex-start;
  align-items: center;
`;
export const ProfileContainer = styled.div``;
export const UserDetailsContainer = styled.div``;
export const CardContents = styled.div`
  font-size: 18px;
  display: flex;
  flex-wrap: wrap;
`;
export const CardTimeStampContainer = styled.div`
  display: flex;
  font-size: 10px;
`;
