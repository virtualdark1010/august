import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from 'react-accessible-accordion';
import Lottie from 'react-lottie';

import { Checkbox } from 'shared/icons/Checkbox';

import useDefaultOptions from './hooks/useDefaultOptions';
import config from './config';

import {
  OrderHeader,
  OrderHeaderState,
  LottieContainer,
  OrderState,
  CollapseTriggerContainer,
  Steps,
} from '../style';

const QueueStatus = () => {
  return (
    <Accordion>
      <AccordionItem>
        {config.map((conf) => {
          return (
            <>
              <AccordionItemHeading>
                <AccordionItemButton>
                  <OrderHeader>
                    <OrderHeaderState>
                      <LottieContainer>
                        <Lottie
                          height={conf.iconDimensions.height || 40}
                          width={conf.iconDimensions.width || 40}
                          options={useDefaultOptions(conf.orderState)}
                        />
                      </LottieContainer>
                      <OrderState>
                        {conf.stateName} <span>{conf.orderState}</span>
                      </OrderState>
                    </OrderHeaderState>
                    <CollapseTriggerContainer>View</CollapseTriggerContainer>
                  </OrderHeader>
                </AccordionItemButton>
              </AccordionItemHeading>
              <AccordionItemPanel>
                <Steps>
                  <ul key={conf.id}>
                    {conf.steps.map((list) => (
                      <li>
                        <Checkbox checked={list.state} /> {list.description}
                      </li>
                    ))}
                  </ul>
                </Steps>
              </AccordionItemPanel>
            </>
          );
        })}
      </AccordionItem>
    </Accordion>
  );
};

export default QueueStatus;
