import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  background-color: ${(p) => p.theme.palette.background.secondary};
  height: 100vh;
  ${mobileMediaQuery('flex-direction: column;')}
`;
