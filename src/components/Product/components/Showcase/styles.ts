import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  align-items: center;
  padding-top: 5rem;
  .text-container {
    width: 30%;
    text-align: center;
    ${mobileMediaQuery(`width: 80%; font-size: 1.5rem;`)}
  }
`;

export const Container = styled.section`
  position: relative;
  height: 80%;
  display: flex;
  align-items: center;
  svg {
    height: 100%;
    position: absolute;
    margin: 0;
  }
  .slider {
    margin-left: 75px;
    position: relative;
    display: flex;
    align-items: center;
    height: 90%;
    background-color: ${(p) => p.theme.palette.background.primary};
  }
`;

export const Scroller = styled.div``;

export const Slider = styled.div`
  height: 50vh;
  display: flex;
  padding: 0 1rem 0 6rem;
`;

export const Slide = styled.img`
  height: 100%;
  width: auto;
  margin-top: -25px;
  margin-right: 2rem;
`;
