import Text from 'components/shared/Text';
import Button from 'components/shared/Button';

import { Container, InnerContainer, Image, ButtonIconContainer } from './styles';
import { ProductTileType, ProductModalType } from 'types/productTile';
import Modal from 'shared/Modal';
import { useRef, useState } from 'react';
import styled from 'styled-components';
import { PlayIcon } from 'shared/icons';

const ProductTile = (props: ProductTileType & ProductModalType) => {
  const [isOpen, setIsOpen] = useState(false);
  const ref = useRef(null);
  const {
    title,
    buttonText,
    image,
    hasPlayButton,
    modalHeading,
    modalText,
    modalVideoUrl,
    videoHeight,
    videoWidth,
    productUrl,
  } = props;
  return (
    <Container className='product-tile' {...props}>
      <InnerContainer
        ref={ref}
        initial={{
          opacity: 0.5,
        }}
        whileInView={{
          opacity: 1,
          transition: {
            duration: 0.5,
            type: 'spring',
            stiffness: 30,
          },
        }}
      >
        <Text text={title} color='#ffffff' fontSize='40px' weight='700' />
        <ButtonIconContainer>
          <Button onClick={() => {}} bgColor='#3C3C3C' borderColor='#fff' color='#fff'>
            {buttonText}
          </Button>
          <div onClick={() => setIsOpen(true)}>{hasPlayButton && <PlayIcon />}</div>
        </ButtonIconContainer>
        <Image src={image} alt={`${image}`} />
        {hasPlayButton && (
          <Modal isOpen={isOpen} center bgColor='#252424' positionFixed={true}>
            <ModalContainer>
              <Text
                text={modalHeading || ''}
                color='#fff'
                fontSize='2rem'
                fontFamily='ClashDisplayBold'
              />
              <Text text={modalText || ''} color='#fff' />
              <iframe
                width={videoWidth || 350}
                height={videoHeight || 350}
                src={modalVideoUrl}
                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
              ></iframe>
              <ModalButtonContainer>
                <Button onClick={() => setIsOpen(false)}>Close</Button>
                <Button onClick={() => window.location.replace(productUrl || '/')}>
                  Show product
                </Button>
              </ModalButtonContainer>
            </ModalContainer>
          </Modal>
        )}
      </InnerContainer>
    </Container>
  );
};

const ModalContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 500px;
  justify-content: center;
  align-items: center;
  text-align: center;
  gap: 15px;
  span:first-child {
    border-bottom: 1px solid #ccc;
  }
`;

const ModalButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: 15px;
`;

export default ProductTile;
