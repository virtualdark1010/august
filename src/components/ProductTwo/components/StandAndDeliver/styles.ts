import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  align-items: center;
  position: relative;
  flex-direction: column;
  overflow: hidden;
`;

export const Flex = styled(motion.div)<{ reverse?: boolean }>`
  display: flex;
  width: 100%;
  margin-bottom: 3rem;
  flex-direction: ${({ reverse = false }) => (reverse ? 'row-reverse' : 'row')};
  ${mobileMediaQuery('flex-direction: column;')}
`;

export const TextWrapper = styled.div`
  width: inherit;
  display: flex;
  flex-direction: column;
`;

export const Image = styled(motion.img)`
  object-fit: cover;
`;

export const Content = styled.div`
  width: 65%;
  display: flex;
  justify-content: center;
  img {
    margin-top: 2rem;
  }
  ${mobileMediaQuery('width: 100%; padding: 0 1rem;')}
`;

export const RightSection = styled.div`
  display: flex;
  justify-content: center;
  ${mobileMediaQuery(`width: 50%; margin-top: 2rem; align-self: center;`)}
`;
