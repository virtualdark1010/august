import { useRef } from 'react';
import Slider from 'react-slick';
import Text from 'components/shared/Text';

import image1 from 'assets/images/product-1.png';

import {
  SliderWrapper,
  Image,
  Item,
  Main,
  TopSection,
  Divider,
  Flex,
  Arrow,
  ButtonWrapper,
  TextWrapper,
} from './styles';
import { LeftArrow, RightArrow } from 'components/shared/icons';

const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 2,
  slidesToScroll: 2,
};

export const ProductSuggestions = () => {
  const sliderRef = useRef<any>();

  const prev = () => sliderRef?.current?.slickPrev();
  const next = () => sliderRef?.current?.slickNext();

  return (
    <Main>
      <TopSection>
        <Flex>
          <Text text='Related products' color='#FD1515' fontSize='1.5rem' weight='700' />
          <ButtonWrapper>
            <Arrow onClick={prev}>
              <LeftArrow />
            </Arrow>
            <Arrow onClick={next}>
              <RightArrow />
            </Arrow>
          </ButtonWrapper>
        </Flex>
        <Text
          className='mt-4'
          text='This product is usually used with the following ones. You can learn more about them on our Business site'
          fontSize='1rem'
        />
      </TopSection>
      <SliderWrapper>
        <Slider {...settings} ref={sliderRef}>
          {[1, 2, 3, 4, 5].map((_) => (
            <Item>
              <Image src={image1} />
              <TextWrapper>
                <Divider />
                <Text text='360 workspace' fontSize='1rem' weight='700' color='black' />
                <Text text='360 workspace' fontSize='1rem' color='black' />
              </TextWrapper>
            </Item>
          ))}
        </Slider>
      </SliderWrapper>
    </Main>
  );
};
