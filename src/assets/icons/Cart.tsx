export const Cart = () => (
  <svg width='28' height='26' viewBox='0 0 28 26' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <g clipPath='url(#clip0_129_725)'>
      <path
        d='M10.9375 23.8337C11.5657 23.8337 12.075 23.3486 12.075 22.7503C12.075 22.152 11.5657 21.667 10.9375 21.667C10.3093 21.667 9.79999 22.152 9.79999 22.7503C9.79999 23.3486 10.3093 23.8337 10.9375 23.8337Z'
        stroke='black'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M23.45 23.8337C24.0782 23.8337 24.5875 23.3486 24.5875 22.7503C24.5875 22.152 24.0782 21.667 23.45 21.667C22.8218 21.667 22.3125 22.152 22.3125 22.7503C22.3125 23.3486 22.8218 23.8337 23.45 23.8337Z'
        stroke='black'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M1.83746 1.08301H6.38747L9.43598 15.5888C9.54 16.0876 9.8249 16.5356 10.2408 16.8545C10.6567 17.1734 11.1771 17.3428 11.711 17.333H22.7675C23.3013 17.3428 23.8218 17.1734 24.2377 16.8545C24.6536 16.5356 24.9385 16.0876 25.0425 15.5888L26.8625 6.49967H7.52497'
        stroke='black'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </g>
    <defs>
      <clipPath id='clip0_129_725'>
        <rect width='27.3' height='26' fill='white' transform='translate(0.699951)' />
      </clipPath>
    </defs>
  </svg>
);
