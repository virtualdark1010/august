/* eslint-disable react/display-name */
import Footer from 'components/shared/Footer';
import Header from 'components/shared/Header';

export const withHF =
  <T extends object>(WrapperComponent: React.ComponentType<T>): React.ComponentType<T> =>
  (componentProps: T) => {
    return (
      <>
        <Header />
        <WrapperComponent {...(componentProps as T)} />
        <Footer />
      </>
    );
  };
