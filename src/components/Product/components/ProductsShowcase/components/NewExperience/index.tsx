import React from 'react';
import Button from 'shared/Button';
import GlowPatch from 'shared/GlowPatch';
import Grid from 'shared/Grid';
import Text from 'shared/Text';
import Toggle from 'shared/Toggle';
import { AudioWave1, AudioWave2 } from 'shared/icons';
import { useAnimate } from 'hooks/useAnimate';
import TopSection from '../TopSection';
import ImageAnimation from './components/ImageAnimation';

import {
  Main,
  Inline,
  Container,
  AudioOneRS,
  AudioOneRSWrapper,
  Wave2,
  ImageWrapper,
  Image,
  BatterySection,
  AnimationWrapper,
  LeftColumn,
  LeftColumnWrapper,
  GridWrapper,
} from './styles';
import { motion } from 'framer-motion';
import { isMobile } from 'react-device-detect';

const NewExperience = () => {
  const { ref: gridItemRef, config: gridItemConfig } = useAnimate();
  const { ref: cripserTextRef, config: cripserTextConfig } = useAnimate();
  const { ref: fasterWifiImageRef, config: fasterWifiImageConfig } = useAnimate();
  const { ref: topTextRef, config: topTextConfig } = useAnimate();
  const { ref: cameraImageRef, config: cameraImageConfig } = useAnimate();
  const { ref: audioWaveRef, config: audioWaveConfig } = useAnimate();
  return (
    <Main>
      <TopSection text='A whole new Core. All-around improved performance.' />
      <Container ref={topTextRef}>
        <Inline
          offSet={!isMobile}
          {...topTextConfig({
            leftToRight: true,
            x: isMobile ? 0 : -150,
          })}
        >
          <Text text='Faster WiFi.' fontSize='1.25rem' color='#FD1515' center weight='bold' />
          <Text
            text='Transfer and edit your files faster in the Insta360 app, thanks to all-new hardware in the Core.'
            fontSize='1rem'
            color='#EAECF0'
            center
          />
        </Inline>
        <ImageWrapper ref={fasterWifiImageRef}>
          <Image
            src='src/assets/images/faster-wifi.png'
            width={isMobile ? '50%' : '80%'}
            {...fasterWifiImageConfig({})}
          />
        </ImageWrapper>
        <Image
          src='src/assets/images/action-camera.png'
          width={isMobile ? '50vw' : '30vw'}
          {...topTextConfig({
            leftToRight: true,
            x: 0,
            delay: 0.8,
          })}
        />
        <GlowPatch position='center' />
      </Container>
      <Container ref={cripserTextRef}>
        <Inline width={isMobile ? '100%' : '50%'} {...cripserTextConfig({})}>
          <Text text='Crisper audio.' fontSize='1.25rem' color='#FD1515' center weight='bold' />
          <Text
            text='The RS Core now has an additional mic to help capture the audio you do want, and a Wind Noise Reduction algorithm to remove the noise you don’t.'
            fontSize='1rem'
            color='#EAECF0'
            center
          />
        </Inline>
      </Container>
      <AudioOneRSWrapper>
        <AudioOneRS ref={cameraImageRef}>
          <motion.div
            {...cameraImageConfig({
              leftToRight: false,
              x: 0,
              delay: 0.6,
            })}
          >
            <AudioWave1 {...(isMobile && { width: '65px' })} />
          </motion.div>
          <Image
            src='src/assets/images/audio-one-rs.png'
            width={isMobile ? '60%' : '80%'}
            {...cameraImageConfig({
              leftToRight: true,
              x: 0,
              delay: 0.5,
            })}
          />
          <Wave2 ref={audioWaveRef}>
            <motion.div
              {...audioWaveConfig({
                leftToRight: false,
                x: 0,
                delay: 0.8,
              })}
            >
              <AudioWave2 {...(isMobile && { width: '50px' })} />
            </motion.div>
          </Wave2>
        </AudioOneRS>
        <Toggle offLabel='TURN OFF' onLabel='TURN ON' />
        <Text text='NOISE REDUCTION' fontSize='1rem' color='#EAECF0' center />
        <GlowPatch position='center' />
      </AudioOneRSWrapper>
      <ImageAnimation
        imageOne={{ src: 'src/assets/images/new-experience-cam-left-1.png' }}
        imageTwo={{ src: 'src/assets/images/new-experience-cam-right-1.png', x: '-12%' }}
      />
      <ImageAnimation
        imageOne={{
          src: 'src/assets/images/new-experience-cam-left-2.png',
          x: '12%',
          z: 3,
        }}
        imageTwo={{ src: 'src/assets/images/new-experience-cam-right-2.png' }}
      />
      <BatterySection>
        <Text text='Bigger, better battery.' fontSize='3rem' center />
        <Text
          text='Power through the action with the new 1445mAh battery, now with 21% more capacity.'
          fontSize='1.25rem'
          color='#EAECF0'
          center
        />
        <Image src='src/assets/images/battery.png' width='80%' />
        <GlowPatch position='center' />
      </BatterySection>
      <GridWrapper>
        <Grid row={isMobile ? 1 : 12} padding='0' gridGap='0'>
          {(Item: typeof React.Component) => {
            const itemOneProps = {
              start: isMobile ? 1 : 1,
              end: isMobile ? 1 : 5,
              rowStart: 1,
              rowEnd: isMobile ? 6 : 12,
            };
            const itemTwoProps = {
              start: isMobile ? 1 : 5,
              end: isMobile ? 1 : 13,
              rowStart: isMobile ? 6 : 1,
              rowEnd: 12,
            };
            return (
              <>
                <Item {...itemOneProps} height='100%' bgColor='transparent'>
                  <AnimationWrapper ref={gridItemRef}>
                    <Image
                      src='src/assets/images/product-bg.png'
                      borderRadius='0'
                      width='100%'
                      height={isMobile ? '100%' : '80vh'}
                      {...gridItemConfig({
                        leftToRight: false,
                      })}
                    />
                  </AnimationWrapper>
                </Item>
                <Item {...itemTwoProps} bgColor='transparent'>
                  <LeftColumn ref={gridItemRef}>
                    <LeftColumnWrapper {...gridItemConfig({})}>
                      <Text
                        text='Quick Reader'
                        fontSize={isMobile ? '2rem' : '4rem'}
                        weight='bold'
                        className='pb-4'
                        {...gridItemConfig({})}
                      />
                      <Text
                        text='No need for WiFi.'
                        fontSize={isMobile ? '1.5rem' : '2.75rem'}
                        color='#FF0000'
                        weight='bold'
                        {...gridItemConfig({})}
                      />
                      <Text
                        text='Edit faster on your phone and save your camera’s battery from WiFi transfers. Simply plug the Quick Reader into ONE RS to save files to the SD card inside, then plug into your phone to start editing.'
                        color='#EAECF0'
                        fontSize={isMobile ? '0.875rem' : '1.275rem'}
                        className='pb-4'
                        {...gridItemConfig({})}
                      />
                      <Button onClick={() => {}} {...gridItemConfig({})}>
                        BUY NOW
                      </Button>
                    </LeftColumnWrapper>
                  </LeftColumn>
                </Item>
              </>
            );
          }}
        </Grid>
      </GridWrapper>
    </Main>
  );
};

export default NewExperience;
