import React from 'react';

import { Main, Image, ImageWrapper } from './styles';
import InfoSection from '../CarouselWithInfo/components/InfoSection';
import controlBg from 'assets/images/control-bg.png';
import controlFloating from 'assets/images/control-floating.png';
import { useAnimate } from 'hooks/useAnimate';

const ControlFromFar = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <InfoSection
        bgColor='#0061FF70'
        infoSectionImage={controlFloating}
        floatingImage
        subTitle='Control from afar.'
        description='Use the case to dial in your settings and capture from up to 33ft (10m) away.'
      />
      <ImageWrapper
        {...config({
          customConfig: {
            variants: {
              visible: {
                opacity: 1,
                transition: { duration: 3 },
              },
              hidden: {
                opacity: 0,
                transition: { duration: 3 },
              },
            },
          },
        })}
      >
        <Image src={controlBg} />
      </ImageWrapper>
    </Main>
  );
};

export { ControlFromFar };
