import React from 'react';
import Text from 'shared/Text';
import { marqueeList } from './config';
import { Main, Wrapper, Flex, BackDrop } from './styles';

const Marquee = () => {
  const renderMarquee = marqueeList.map((marquee, i) => (
    <Flex paddingLeft={i % 2 === 1 ? '2rem' : '0'}>
      {marquee.map((RenderIcon: any) => (
        <RenderIcon />
      ))}
    </Flex>
  ));

  return (
    <Main>
      <Text text='Used by the best' center fontSize='2.5rem' />
      <Wrapper>
        <BackDrop></BackDrop>
        {renderMarquee}
      </Wrapper>
    </Main>
  );
};

export default Marquee;
