import styled from 'styled-components';

export const Main = styled.div`
  position: relative;
`;

export const StyledInput = styled.input<{ fullBorder?: boolean }>`
  font-size: 18px;
  padding: 10px 10px 10px 5px;
  display: block;
  width: 100%;
  border: none;
  padding-left: ${({ fullBorder }) => (fullBorder ? '1rem' : '5px')};
  ${(p) => {
    const { theme, fullBorder } = p;
    const borderBottomColor = theme.palette.common.white;
    const caretColor = theme.palette.common.white;
    const backgroundColor = theme.palette.common.transparent;
    const color = theme.palette.common.white;
    return `
      color: ${color};
      border${fullBorder ? '' : '-bottom'}: 1px solid ${borderBottomColor};
      caret-color: ${caretColor};
      background-color: ${backgroundColor};
    `;
  }}
  &:focus {
    outline: none;
  }
`;

export const Label = styled.label<{ isFilled?: boolean }>`
  color: #fff;
  font-size: 18px;
  font-weight: normal;
  position: absolute;
  pointer-events: none;
  left: 5px;
  top: 10px;
  transition: 0.2s ease all;
  ${({ isFilled }) =>
    isFilled &&
    ` {
    top: -18px;
    font-size: 14px;
    color: #fff;
  }`}
`;
