import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

export const ProductContainer = styled.div`
  @media screen and (max-width: 768px) {
    div {
      display: flex;
      flex-direction: column;
    }
  }
`;
