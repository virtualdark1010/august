export const AudioWave1 = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width='200'
      height='157'
      viewBox='0 0 200 157'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <rect y='46.875' width='11.7188' height='62.5' rx='5.85938' fill='white' />
      <rect x='140.625' y='46.875' width='11.7188' height='62.5' rx='5.85938' fill='white' />
      <rect x='23.4375' y='31.6406' width='11.7188' height='93.75' rx='5.85938' fill='white' />
      <rect x='117.188' y='31.6406' width='11.7188' height='93.75' rx='5.85938' fill='white' />
      <rect x='164.062' y='31.6406' width='11.7188' height='93.75' rx='5.85938' fill='white' />
      <rect x='46.875' y='16.7969' width='11.7188' height='125' rx='5.85938' fill='white' />
      <rect x='93.75' y='16.7969' width='11.7188' height='125' rx='5.85938' fill='white' />
      <rect x='188.281' y='16.7969' width='11.7188' height='125' rx='5.85938' fill='white' />
      <rect x='70.3125' width='11.7188' height='156.25' rx='5.85938' fill='white' />
    </svg>
  );
};
