import React, { useReducer, useCallback, useMemo, createContext } from 'react';
import { shoppingCartReducer } from './reducer';
import {
  ShoppingCartContextType,
  ShoppingCartInitialState,
  ShoppingCartProviderType,
  ShoppingCartSteps,
} from './types';

export const ShoppingCartActionsMap = {
  add: 'ADD',
  remove: 'REMOVE',
  setStep: 'SET_STEP',
};

const initialState: ShoppingCartInitialState = {
  numberOfItems: 0,
  cartTotal: 0,
  step: 'Address',
  cart: [],
};

export const ShopppingCartContext = createContext<ShoppingCartContextType>({
  numberOfItems: 0,
  cartTotal: 0,
  step: 'Address',
  cart: [],
  setStep: () => undefined,
  addToCart: () => undefined,
  removeFromCart: () => undefined,
});

const ShopppingCartProvider = (props: ShoppingCartProviderType) => {
  const [state, _dispatch] = useReducer(shoppingCartReducer, initialState);
  const { numberOfItems, cartTotal, step, cart } = state;

  const addToCart = useCallback(
    (payload: any) => _dispatch({ type: ShoppingCartActionsMap.add, payload }),
    [],
  );
  const removeFromCart = useCallback(
    (id: number) => _dispatch({ type: ShoppingCartActionsMap.remove, payload: id }),
    [],
  );
  const setStep = useCallback(
    (step: ShoppingCartSteps) => _dispatch({ type: ShoppingCartActionsMap.setStep, payload: step }),
    [],
  );

  const value = useMemo(() => {
    return {
      step,
      cart,
      setStep,
      cartTotal,
      addToCart,
      numberOfItems,
      removeFromCart,
    };
  }, [state, setStep, addToCart, removeFromCart]);

  return <ShopppingCartContext.Provider value={value} {...props} />;
};

export default ShopppingCartProvider;
