import styled from 'styled-components';
import { getPosition } from './helper';
import { PatchPosition } from './types';

export const Main = styled.div<{
  position?: PatchPosition;
}>`
  position: absolute;
  z-index: 1;
  ${({ position = 'center' }) => getPosition(position)}
`;
