import Checkout from 'components/Checkout';

import { withLayout } from 'hoc/withLayout';
import { withHF } from 'hoc/withHF';

const CheckoutPage = () => <Checkout />;

export default withHF(withLayout(CheckoutPage, { fullHeight: true }));
