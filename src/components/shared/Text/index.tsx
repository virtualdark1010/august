import { TextType } from 'types/text';
import { Container } from './styles';

const Text = (props: TextType) => {
  const { text, className, onClick } = props;
  return (
    <Container className={`text-white text-container ${className}`} {...props} onClick={onClick}>
      {text}
    </Container>
  );
};

export default Text;
