import React from 'react';
import Text from 'shared/Text';
import { useAnimate } from 'hooks/useAnimate';
import { Main, Container } from './styles';

const TopSection = ({
  text,
  subHeading,
  description,
}: {
  text: string;
  subHeading?: string;
  description?: string;
}) => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <Container {...config({ leftToRight: true })}>
        <Text text={text} fontSize='5rem' center />
        {subHeading && (
          <Text text={subHeading} color='#FF0000' fontSize='3rem' center weight='bold' />
        )}
        {description && <Text text={description} fontSize='2rem' center />}
      </Container>
    </Main>
  );
};

export default TopSection;
