import styled from 'styled-components';

export const Container = styled.div`
  margin-left: 1.5rem;
  background-color: ${(p) => p.theme.palette.common.white};
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: flex-start;
  margin-top: 1rem;
`;

export const Dot = styled.div`
  width: 6px;
  height: 6px;
  border-radius: 100%;
  margin: 0.5rem 2rem 0 0;
  background-color: ${(p) => p.theme.palette.primary.main};
`;

export const Feature = styled.p`
  word-break: break-word;
  width: 90%;
`;
