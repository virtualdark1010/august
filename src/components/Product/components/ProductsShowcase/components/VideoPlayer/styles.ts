import styled from 'styled-components';

export const Main = styled.div<{ height?: string }>`
  height: ${({ height = '400vh' }) => height};

  > div {
    width: 100vw !important;
    height: 100vh !important;
  }
  @media only screen and (max-width: 768px) {
    display: flex;
    justify-content: center;
  }
`;

export const PlayerWrapper = styled.div<{ show?: boolean }>`
  position: fixed;
  top: 0;
  > div {
    width: 100vw !important;
    height: 100vh !important;
  }
`;
