import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.div`
  position: fixed;
  height: 100vh;
  left: 50px;
  display: flex;
  align-items: center;
  z-index: 10;
  ${mobileMediaQuery(`left: 10px;`)}
`;

export const Wrapper = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

export const Dot = styled.div<{ isActive?: boolean }>`
  width: 12px;
  ${({ isActive }) => {
    const background = isActive ? '#BF0C0C' : 'white';
    const borderRadius = isActive ? '10px' : '100%';
    const height = isActive ? '2rem' : '12px';
    return `
    height: ${height};
    background-color: ${background};
    border-radius: ${borderRadius};
    ${mobileMediaQuery(`
        width: 8px;
        height: ${isActive ? '1.5rem' : '8px'};
    `)}
    `;
  }}
`;

export const Flex = styled.div<{ isActive?: boolean }>`
  display: flex;
  align-items: center;
  cursor: pointer;
  transition: 250ms all;
  flex-direction: column;
  width: 10px;
`;

export const TextWrapper = styled(motion.div)`
  > span {
    transition: height 0.5s;
    white-space: nowrap;
    writing-mode: vertical-rl;
    margin-top: 1rem;
    ${mobileMediaQuery(`
     font-size: 0.875rem;
  `)}
  }
`;
