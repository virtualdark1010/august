import { Container } from './style';

import Input from 'shared/Input';
import Button from 'shared/Button';
import Text from 'shared/Text';
import styled from 'styled-components';
import QueueStatus from './parts/QueueStatus';

const Track = () => {
  return (
    <Container>
      <FormGroup>
        <FormDetails>
          <Text text='Track' fontFamily='ClashDisplayBold' fontSize='4rem' />
          <Text
            text='Please enter your tracking id below to get tracking details of your shipment'
            fontFamily='InterRegular'
            fontSize='1rem'
          />
        </FormDetails>
        <FormContainer>
          <Input type='text' name='email' label='Tracking ID' />
          <Button onClick={() => console.log('submit')}>Track</Button>
        </FormContainer>
      </FormGroup>
      <TrackingContainer>
        <QueueStatus />
      </TrackingContainer>
    </Container>
  );
};

const FormGroup = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const FormDetails = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 50%;
  span:nth-child(2) {
    text-align: center;
    line-height: 2rem;
  }
`;

const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 50%;
  div {
    width: 100%;
    margin-bottom: 25px;
  }
`;

const TrackingContainer = styled.div`
  margin-top: 3rem;
  color: #fff;
  width: 50%;
  border: 1px solid red;
  padding: 2rem;
`;

export default Track;
