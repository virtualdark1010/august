import { ButtonType } from 'types/button';

const getButtonVariant = ({
  variant,
  theme,
  borderColor,
}: Partial<ButtonType> & { theme: any }) => {
  if (!variant) return '';
  return `
      color: ${theme.palette.button.variant[variant].color};
      border: 1px solid ${borderColor || theme.palette.button.variant[variant].borderColor};
      background: ${theme.palette.button.variant[variant].background}
  `;
};

const getButtonStyle = ({ shape = 'default', theme }: Partial<ButtonType> & { theme: any }) => {
  return `
      border-radius: ${theme.palette.button.shape[shape].borderRadius};
  `;
};

export { getButtonStyle, getButtonVariant };
