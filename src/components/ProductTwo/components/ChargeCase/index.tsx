import React from 'react';

import { Main, TextWrapper, Image } from './styles';
import Text from 'shared/Text';
import chargeCase from 'assets/images/charge-case.png';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const ChargeCase = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <TextWrapper>
        <Text
          text='Next up...'
          fontSize={isMobile ? '2rem' : '5rem'}
          weight='bold'
          color='#1E1E1E'
          center
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  rotate: 360,
                  y: 0,
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.5 },
                },
                hidden: {
                  opacity: 0,
                  rotate: 0,
                  y: '-100vw',
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.5 },
                },
              },
            },
          })}
        />
        <Text
          text='The Charge Case'
          fontSize={isMobile ? '2rem' : '5rem'}
          weight='bold'
          color='#1E1E1E'
          center
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  rotate: 360,
                  y: 0,
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.9 },
                },
                hidden: {
                  opacity: 0,
                  rotate: 0,
                  y: '100vw',
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.9 },
                },
              },
            },
          })}
        />
        <Text
          text='GO 2’s Charge Case does more than you’d think. It’s a case, charger, controller and tripod all in one.'
          fontSize={isMobile ? '1rem' : '1.75rem'}
          color='#1E1E1E60'
          center
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  rotate: 360,
                  y: 0,
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.9 },
                },
                hidden: {
                  opacity: 0,
                  rotate: 0,
                  y: '100vw',
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.9 },
                },
              },
            },
          })}
        />
      </TextWrapper>
      <Image
        src={chargeCase}
        {...config({
          customConfig: {
            variants: {
              visible: {
                opacity: 1,
                rotate: 360,
                scale: 1,
                transition: { duration: 3, type: 'spring', stiffness: 50 },
              },
              hidden: {
                opacity: 0,
                rotate: 0,
                scale: 0,
                transition: { duration: 3, type: 'spring', stiffness: 50 },
              },
            },
          },
        })}
      />
    </Main>
  );
};

export { ChargeCase };
