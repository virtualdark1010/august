import { isMobile } from 'react-device-detect';
import { withLayout } from 'hoc/withLayout';
import React from 'react';
import Grid from 'components/shared/Grid';
import imageUrl from 'assets/images/camera.png';
import { Image } from './styles';

const ProductMosaic = () => {
  return (
    <Grid padding='4rem' gridGap='2rem'>
      {(Item: typeof React.Component) => {
        return (
          <>
            <Item
              start={isMobile ? 1 : 2}
              end={isMobile ? 12 : 7}
              rowStart={isMobile ? 1 : 2}
              rowEnd={isMobile ? 1 : 7}
            >
              <Image src={imageUrl} alt='cameraImage' />
            </Item>
            <Item start={isMobile ? 1 : 7} end={12} rowStart={2} rowEnd={isMobile ? 2 : 7}>
              <Image src={imageUrl} alt='cameraImage' />
            </Item>
            <Item
              start={isMobile ? 1 : 2}
              end={isMobile ? 12 : 7}
              rowStart={isMobile ? 3 : 7}
              rowEnd={isMobile ? 3 : 12}
            >
              <Image src={imageUrl} alt='cameraImage' />
            </Item>
            <Item
              start={isMobile ? 1 : 7}
              end={12}
              rowStart={isMobile ? 4 : 7}
              rowEnd={isMobile ? 4 : 12}
            >
              <Image src={imageUrl} alt='cameraImage' />
            </Item>
          </>
        );
      }}
    </Grid>
  );
};

export default withLayout(ProductMosaic, {
  patterns: [
    {
      size: 6,
      position: 'topLeft',
    },
    {
      size: 10,
      position: 'bottomRight',
    },
  ],
});
