import { motion } from 'framer-motion';
import React from 'react';
import GlowPatch from 'shared/GlowPatch';
import Text from 'shared/Text';
import ProductDetailSection from '../ProductDetailSection';
import { useAnimate } from 'hooks/useAnimate';

import TopSection from '../TopSection';
import { Main, Container, Image } from './styles';
import { isMobile } from 'react-device-detect';

const Editing = () => {
  const { ref, config } = useAnimate();
  const { ref: imageRef, config: imageConfig } = useAnimate();
  return (
    <Main>
      <TopSection
        text='Editing made easy.'
        subHeading='The Insta360 app.'
        description='Powerful editing tools at your fingertips. Let AI do the work with auto editing tools and templates, or dial in on your edit with a host of manual controls.'
      />
      <Container ref={ref}>
        <Text
          text='Snap Wizard'
          color='#FF0000'
          fontSize={isMobile ? '2rem' : '3rem'}
          weight='bold'
          {...config({})}
        />
        <Text
          text='The easiest and fastest way to edit 360 videos.'
          fontSize={isMobile ? '1.5rem' : '2rem'}
          weight='700'
          {...config({})}
        />
        <Text
          text='Reframe your videos with the turn of your phone or the swipe of a finger. All your edits are saved instantly. Export and share with just a tap.'
          fontSize={isMobile ? '0.875rem' : '1.25rem'}
          color='EAECF0'
          {...config({})}
        />
        <motion.div ref={imageRef}>
          <Image
            src='src/assets/images/phone.png'
            {...imageConfig({
              leftToRight: true,
            })}
          />
        </motion.div>
      </Container>
      <ProductDetailSection
        bannerText='More ways to reframe.'
        imageUrl='src/assets/images/product.png'
        patch={<GlowPatch position='center' />}
      />
      <ProductDetailSection
        highlightedText='Shot Lab.'
        description='Shot Lab. Shot Lab. Cloning, hyperlapses, barrel rolls, stop motion… Shot Lab has it all. Shoot awesome effects with the help of AI. Just import your 360 footage, choose an effect and that’s it!'
        imageUrl='src/assets/images/product.png'
        patch={<GlowPatch position='center' />}
      />
      <ProductDetailSection
        highlightedText='Shot Lab.'
        description='Shot Lab. Shot Lab. Cloning, hyperlapses, barrel rolls, stop motion… Shot Lab has it all. Shoot awesome effects with the help of AI. Just import your 360 footage, choose an effect and that’s it!'
        imageUrl='src/assets/images/product.png'
        patch={<GlowPatch position='center' />}
      />
      <ProductDetailSection
        highlightedText='Edit like a pro on desktop.'
        description="Edit like a pro on desktop. Insta360 Studio’s new look makes editing on desktop easier. Or, jump right into your edit with the Adobe Premiere Pro plugin's no-stitch workflow."
        imageUrl='src/assets/images/whiteboard.png'
        patch={<GlowPatch position='center' />}
      />
    </Main>
  );
};

export default Editing;
