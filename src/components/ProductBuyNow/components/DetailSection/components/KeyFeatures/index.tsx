import Text from 'components/shared/Text';

import { Container, Feature, Wrapper, Dot } from './styles';
import keyFeatures from './config';

export const KeyFeatures = () => {
  return (
    <Container>
      <Text text='Key features' color='#FD1515' fontSize='1.5rem' weight='700' />
      {keyFeatures.map((feature) => (
        <Wrapper key={feature}>
          <Dot />
          <Feature>{feature}</Feature>
        </Wrapper>
      ))}
    </Container>
  );
};
