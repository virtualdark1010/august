import React, { useState } from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import { Book, Cross, DownArrow, Logo } from 'shared/icons';
import Text from 'shared/Text';
import {
  Links,
  Main,
  Menu,
  Flex,
  Group,
  TextWrapper,
  Row,
  SubMenu,
  Arrow,
  CloseMenu,
} from './styles';
import { itemVariants, links, sideVariants } from './config';

export const SliderMenu = ({ open, onClose }: { open: boolean; onClose?: () => void }) => {
  const [expanded, setExpanded] = useState<false | number>(false);
  return (
    <Main>
      <AnimatePresence>
        {open && (
          <Menu
            initial={{ width: 0 }}
            animate={{
              width: 300,
            }}
            exit={{
              width: 0,
              transition: { delay: 0.7, duration: 0.3 },
            }}
          >
            <Logo />
            <Links initial='closed' animate='open' exit='closed' variants={sideVariants}>
              {links.map(({ name, subMenu }, i) => {
                return (
                  <Row key={name}>
                    <Flex
                      active={expanded === i}
                      onClick={() => setExpanded(expanded === i ? false : i)}
                      variants={itemVariants}
                    >
                      <Group>
                        <Book />
                        <TextWrapper whileHover={{ scale: 1.1 }}>
                          <Text text={name} level='2' color={'#000'} />
                        </TextWrapper>
                      </Group>
                      <Arrow
                        initial='collapsed'
                        animate='open'
                        exit='collapsed'
                        variants={
                          expanded === i
                            ? {
                                open: { opacity: 1, transform: 'rotate(180deg)' },
                                collapsed: { opacity: 0, transform: 'rotate(0deg)' },
                              }
                            : {}
                        }
                        transition={{ duration: 0.8, ease: [0.04, 0.62, 0.23, 0.98] }}
                      >
                        <DownArrow />
                      </Arrow>
                    </Flex>
                    {subMenu.map((subMenu) => (
                      <SubMenu variants={itemVariants}>
                        <AnimatePresence initial={false}>
                          {expanded === i && (
                            <motion.div
                              initial='collapsed'
                              animate='open'
                              exit='collapsed'
                              variants={{
                                open: { opacity: 1, height: '50px' },
                                collapsed: { opacity: 0, height: 0 },
                              }}
                              transition={{ duration: 0.8, ease: [0.04, 0.62, 0.23, 0.98] }}
                            >
                              <Group>
                                <Book />
                                <TextWrapper whileHover={{ scale: 1.1 }} variants={itemVariants}>
                                  <Text text={subMenu?.name} level='2' color={'#000'} />
                                </TextWrapper>
                              </Group>
                            </motion.div>
                          )}
                        </AnimatePresence>
                      </SubMenu>
                    ))}
                  </Row>
                );
              })}
            </Links>
          </Menu>
        )}
      </AnimatePresence>
      {open && (
        <CloseMenu
          onClick={onClose}
          initial='closed'
          animate='open'
          exit='closed'
          variants={sideVariants}
        >
          <Cross />
        </CloseMenu>
      )}
    </Main>
  );
};
