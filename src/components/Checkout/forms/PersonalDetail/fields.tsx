import Dropdown from 'shared/Dropdown';
import Input from 'shared/Input';

export const fields = [
  {
    width: 'half',
    props: { label: 'First Name', name: 'firstName', type: 'text' },
    Render: Input,
  },
  {
    width: 'half',
    props: { label: 'Last Name', name: 'lastName', type: 'text' },
    Render: Input,
  },
  {
    width: 'full',
    props: { label: 'Address', name: 'address', type: 'text' },
    Render: Input,
  },
  {
    width: 'full',
    props: { label: 'Appartment', name: 'appartment', type: 'text' },
    Render: Input,
  },
  {
    width: 'quarter',
    props: {
      options: [{ value: 'India', label: 'India' }],
      label: 'Country',
    },
    Render: Dropdown,
  },
  {
    width: 'quarter',
    props: {
      options: [{ value: 'delhi', label: 'Delhi' }],
      label: 'City',
    },
    Render: Dropdown,
  },
  {
    width: 'quarter',
    props: { label: 'Zip Code', name: 'zipcode', type: 'text' },
    Render: Input,
  },
  {
    width: 'full',
    props: {
      label: 'Optional',
      name: 'firstName',
      type: 'text',
    },
    Render: Input,
  },
];
