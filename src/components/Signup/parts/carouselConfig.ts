import image1 from 'assets/images/product-1.png';
import image2 from 'assets/images/product-2.png';
import image3 from 'assets/images/product-1.png';

export const carouselConfig = [
  {
    heading: 'August Cams',
    subHeading:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum as been the industry's standard dummy text ever since the 1500s",
    image: image1,
    buttonText: 'explore',
  },
  {
    heading: 'Explore 360',
    subHeading:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum as been the industry's standard dummy text ever since the 1500s",
    image: image2,
    buttonText: 'explore',
  },
  {
    heading: 'New Product',
    subHeading:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum as been the industry's standard dummy text ever since the 1500s",
    image: image3,
    buttonText: 'buy',
  },
];
