import styled from 'styled-components';
import { LayoutType } from 'types/layout';

const getHeight = ({ withHeader, fullHeight }: LayoutType) => {
  if (withHeader) return `calc(100vh - 110px)`;
  if (fullHeight) return '100vh';
  return 'max-content';
};

export const Wrapper = styled.section<LayoutType>`
  position: relative;
  ${(p) => {
    const { bgColor, theme, overflow } = p;
    const height = getHeight(p);
    const backgroundColor = bgColor ? bgColor : theme.palette.background.primary;
    return `
    height: ${height};
    background-color: ${backgroundColor};
    overflow: ${overflow};
    `;
  }}
`;

export const HeadingWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
