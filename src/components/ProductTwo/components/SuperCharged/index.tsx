import React from 'react';

import Text from 'shared/Text';
import CircularProgress from 'shared/CircularProgress';
import superCharged from 'assets/images/super-charged.png';
import { Main, TextWrapper, Flex, Image, Stats, LeftSection, RightSection } from './styles';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const SuperCharged = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <LeftSection>
        <TextWrapper>
          <Text
            text='Super charged.'
            fontSize={isMobile ? '2rem' : '5rem'}
            weight='bold'
            color='#1E1E1E'
            center={isMobile}
            {...config({ leftToRight: true, delay: 0.5 })}
          />
          <Text
            text='Charge up in just half an hour and shoot for up to 2.5 hours. That’s double what you’ll get out of most action cams.'
            fontSize={isMobile ? '1rem' : '1.75rem'}
            color='#1E1E1E60'
            center={isMobile}
            {...config({ leftToRight: true, duration: 1.5, delay: 0.7 })}
          />
          <Flex>
            <Stats {...config({ leftToRight: true, duration: 2, delay: 0.9, stiffness: 40 })}>
              <CircularProgress
                size={isMobile ? 80 : 140}
                strokeWidth={8}
                percentage={100}
                color='#BB000E'
                text='150 minutes'
              />
              <Text text='GO 2 :' fontSize='1rem' weight='bold' color='#1E1E1E' />
            </Stats>
            <Stats {...config({ leftToRight: true, duration: 2.4, delay: 1.2, stiffness: 50 })}>
              <CircularProgress
                size={isMobile ? 80 : 140}
                strokeWidth={8}
                percentage={60}
                color='#BB000E'
                text='60 minutes'
              />
              <Text text='GO:' fontSize='1rem' weight='bold' color='#1E1E1E' />
            </Stats>
          </Flex>
        </TextWrapper>
      </LeftSection>
      <RightSection>
        <Image
          src={superCharged}
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  rotate: 360,
                  y: 0,
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.5 },
                },
                hidden: {
                  opacity: 0,
                  rotate: 0,
                  y: '-100vw',
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.5 },
                },
              },
            },
          })}
        />
      </RightSection>
    </Main>
  );
};

export { SuperCharged };
