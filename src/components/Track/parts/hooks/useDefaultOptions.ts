import useCurrentAnimationPlaying from './useCurrentPlaying';

const useDefaultOptions = (state: string) => ({
  loop: true,
  autoplay: true,
  animationData: useCurrentAnimationPlaying(state),
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
});

export default useDefaultOptions;
