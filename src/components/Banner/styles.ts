import { motion } from 'framer-motion';
import styled from 'styled-components';

export const Container = styled(motion.div)`
  height: 50vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 8rem;
`;

export const Wrapper = styled.div`
  width: 80%;
  margin-top: 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
