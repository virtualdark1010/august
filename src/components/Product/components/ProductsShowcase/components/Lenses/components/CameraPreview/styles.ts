import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled(motion.div)`
  display: flex;
  justify-content: center;
  position: relative;
  margin: 10rem 0;
  z-index: 5;
  ${mobileMediaQuery('margin: 6rem 0;')}
`;

export const Image = styled(motion.img)<{ width?: string; height?: string; borderRadius?: string }>`
  object-fit: cover;
  width: ${({ width }) => width || '30vw'};
  height: ${({ height }) => height};
  border-radius: ${({ borderRadius }) => borderRadius || '0'};
`;
