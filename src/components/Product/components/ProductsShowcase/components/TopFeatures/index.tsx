import React from 'react';
import { isMobile } from 'react-device-detect';
import GlowPatch from 'shared/GlowPatch';
import Grid from 'shared/Grid';
import Text from 'shared/Text';
import { useAnimate } from 'hooks/useAnimate';
import ProductDetailSection from '../ProductDetailSection';

import { Main, AnimationWrapper, Column, GridWrapper, Image } from './styles';

const TopFeatures = () => {
  const { ref, config } = useAnimate();
  const { ref: imageRef, config: imageConfig } = useAnimate();
  const { ref: gridImageLayoutRef, config: gridImageLayoutConfig } = useAnimate();
  const { ref: gridTextLayoutRef, config: gridTextLayoutConfig } = useAnimate();
  return (
    <Main>
      <ProductDetailSection
        highlightedText='Invisible Selfie Stick.'
        description="Invisible Selfie Stick. Don't ruin the view with an ugly selfie stick! The 360 Lens makes the Invisible Selfie Stick totally disappear from your shots for impossible drone-like footage. Magic!"
        imageUrl='src/assets/images/product.png'
        patch={<GlowPatch position='right' />}
      />
      <Grid row={12}>
        {(Item: typeof React.Component) => {
          const itemOneProps = {
            start: isMobile ? 1 : 2,
            end: isMobile ? 11 : 6,
            rowStart: 1,
            rowEnd: isMobile ? 6 : 12,
          };
          const itemTwoProps = {
            start: isMobile ? 1 : 6,
            end: isMobile ? 11 : 12,
            rowStart: isMobile ? 6 : 1,
            rowEnd: 12,
          };
          return (
            <>
              <Item {...itemOneProps} height='100%' bgColor='transparent'>
                <Column ref={ref}>
                  <Text
                    text='Third-Person View'
                    fontSize={`${isMobile ? 1.5 : 2}rem`}
                    {...config({})}
                  />
                  <Text
                    text='Doing something epic but nobody around to help you take the shot? Capture insane third-person perspectives with the Invisible Selfie Stick. Guaranteed to get people asking "how did you film that?!"'
                    color='#EAECF0'
                    fontSize={`${isMobile ? 1 : 0.875}rem`}
                    {...config({})}
                  />
                </Column>
              </Item>
              <Item {...itemTwoProps} height='100%' bgColor='transparent'>
                <AnimationWrapper ref={imageRef}>
                  <Image
                    src='src/assets/images/product.png'
                    width='100%'
                    height='100%'
                    {...imageConfig({
                      leftToRight: true,
                    })}
                  />
                </AnimationWrapper>
              </Item>
            </>
          );
        }}
      </Grid>
      <ProductDetailSection
        highlightedText='FlowState Stabilization.'
        description='FlowState Stabilization. Buttery smooth image stabilization, no matter what lens you use. The powerful RS Core now delivers FlowState stabilization in camera when paired with the wide-angle lenses. No editing needed.'
        imageUrl='src/assets/images/product.png'
        patch={<GlowPatch position='bottomLeft' />}
      />
      <ProductDetailSection
        highlightedText='Horizon Lock.'
        description='Horizon Lock. Perfectly level shots, no matter how much you tilt your cam. All three lenses support 360° horizon leveling.'
        imageUrl='src/assets/images/product.png'
        patch={<GlowPatch position='right' />}
      />
      <ProductDetailSection
        highlightedText='Active HDR.'
        description='Active HDR. An innovative new HDR video mode for action sports. Active HDR on the 4K Boost Lens keeps your video stabilized as you move, while keeping details in the highlights and shadows for more vibrant footage.'
        imageUrl='src/assets/images/product.png'
        patch={<GlowPatch position='bottomLeft' />}
      />
      <ProductDetailSection
        highlightedText='Slow Mo.'
        description='Slow Mo. Slow down the action up to 8x with the 4K Boost Lens, or pan around and focus on a killer moment with 4x slow-mo on the 360 Lens.'
        imageUrl='src/assets/images/product.png'
        patch={<GlowPatch position='bottomRight' />}
      />
      <GridWrapper>
        <Grid row={12} padding='0'>
          {(Item: typeof React.Component) => {
            const itemOneProps = {
              start: isMobile ? 1 : 1,
              end: isMobile ? 11 : 8,
              rowStart: 1,
              rowEnd: isMobile ? 4 : 12,
            };
            const itemTwoProps = {
              start: isMobile ? 1 : 8,
              end: isMobile ? 11 : 12,
              rowStart: isMobile ? 7 : 1,
              rowEnd: 12,
            };
            return (
              <>
                <Item {...itemOneProps} height='100%' bgColor='transparent'>
                  <AnimationWrapper ref={gridImageLayoutRef}>
                    <Image
                      src='src/assets/images/product-bg.png'
                      borderRadius='0'
                      width='100%'
                      height='100%'
                      {...gridImageLayoutConfig({})}
                    />
                  </AnimationWrapper>
                </Item>
                <Item {...itemTwoProps} height='100%' bgColor='transparent'>
                  <Column ref={gridTextLayoutRef}>
                    <Text
                      text='Built for action.'
                      fontSize={`${isMobile ? 2 : 4}rem`}
                      weight='bold'
                      className='pb-4'
                      {...gridTextLayoutConfig({ delay: 0.5 })}
                    />
                    <Text
                      text='Waterproof to 16ft'
                      fontSize={`${isMobile ? 1.5 : 2.75}rem`}
                      weight='bold'
                      {...gridTextLayoutConfig({ delay: 0.7 })}
                    />
                    <Text
                      text='Dunk your action cam down to 16ft underwater without a worry.'
                      color='#EAECF0'
                      fontSize={`${isMobile ? 0.875 : 1.275}rem`}
                      className='pb-4'
                      {...gridTextLayoutConfig({ delay: 0.9 })}
                    />
                    <Text
                      text='Rugged Design'
                      fontSize={`${isMobile ? 1.5 : 2.75}rem`}
                      weight='bold'
                      {...gridTextLayoutConfig({ delay: 1 })}
                    />
                    <Text
                      text='High performance connectors and a unique structural design enable stable connection between the mods, even at the height of the action.'
                      color='#EAECF0'
                      fontSize={`${isMobile ? 0.875 : 1.275}rem`}
                      {...gridTextLayoutConfig({ delay: 1.1 })}
                    />
                  </Column>
                </Item>
              </>
            );
          }}
        </Grid>
      </GridWrapper>
    </Main>
  );
};

export default TopFeatures;
