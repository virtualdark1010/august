import FooterLinks from 'shared/Footer/components/FooterLinks';
import InfoSection from 'shared/Footer/components/InfoSection';
import Text from '../Text';

import { Main, Copyright } from './styles';

const Footer = () => {
  return (
    <Main>
      <InfoSection />
      <FooterLinks />
      <Copyright>
        <Text text='© 2023 August. All rights reserved.' color='white' />
      </Copyright>
    </Main>
  );
};

export default Footer;
