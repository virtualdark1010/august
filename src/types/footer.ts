import { ReactNode } from 'react';

export type FooterType = { label: string; links: { name: string; link: null | string }[] };

export type FooterInfoType = {
  label: string;
  link?: null | string;
  icon?: null | ReactNode;
};
