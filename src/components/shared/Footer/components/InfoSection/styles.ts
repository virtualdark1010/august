import styled from 'styled-components';

export const Container = styled.div`
  grid-row-start: 1;
  grid-row-end: 3;
  grid-column-start: 2;
  grid-column-end: 4;
  span {
    width: 300px;
  }
  @media screen and (max-width: 768px) {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    text-align: center;
  }
`;

export const Column = styled.div`
  display: flex;
  max-width: 100px;
  flex-direction: column;
  word-break: break-all;
  @media screen and (max-width: 768px) {
    margin: 3rem 0;
    max-width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

export const Link = styled.span`
  margin: 0.5rem 0;
  width: max-content;
  cursor: pointer;
  color: ${(p) => p.theme.palette.common.white};
`;
