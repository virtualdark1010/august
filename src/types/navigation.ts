export type NavigationConfig = {
  name: string;
  link: string;
  badge: string | null;
};

export type SubMenuContent = {
  title?: string;
  badge?: string;
  body?: string;
  button?: string;
  image?: string;
};

export type MenuType = { title: string; contents?: SubMenuContent[] };

export type SubMenuProps = {
  data: MenuType[];
};
