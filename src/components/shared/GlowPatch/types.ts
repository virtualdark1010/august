export type PatchPosition =
  | 'center'
  | 'left'
  | 'right'
  | 'bottom'
  | 'top'
  | 'bottomLeft'
  | 'topRight'
  | 'topLeft'
  | 'bottomRight';
