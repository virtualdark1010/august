import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  margin-bottom: 30vh;
  position: relative;
  ${mobileMediaQuery(`flex-direction: column; margin-bottom: 5vh;`)}
  img {
    bottom: -15%;
  }
`;

export const ImageWrapper = styled(motion.div)`
  width: 45%;
  height: 100%;
  ${mobileMediaQuery(`width: 100%;`)}
`;

export const Image = styled(motion.img)`
  object-fit: contain;
  width: 100%;
`;
