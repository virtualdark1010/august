import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  border: 1px solid ${(p) => p.theme.palette.common.white};
  width: 50%;
  display: flex;
  padding: 1rem;
  border-radius: 10px;
  margin: 1rem 0;
  ${mobileMediaQuery('width: 100%;')}
`;

export const Stack = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 1rem;
`;
