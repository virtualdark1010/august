export type LinkTextType = {
  text?: string;
  url?: string;
  name?: string;
};
