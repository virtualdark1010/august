import { mobileMediaQuery } from 'utils/mediaQuery';
import { motion } from 'framer-motion';
import styled from 'styled-components';

export const Main = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding: 5rem 0;
  position: relative;
  ${mobileMediaQuery('padding: 2rem 0;')}
`;

export const TextWrapper = styled.div`
  width: 60%;
  display: flex;
  flex-direction: column;
  align-items: center;
  span:nth-child(2) {
    line-height: 3rem;
    margin: 2rem 0 3rem 0;
  }
  ${mobileMediaQuery(`
  width: 90%;
  span:nth-child(2) {
    line-height: 2rem; 
    margin: 0.5rem;
  }
  `)}
`;

export const Image = styled(motion.img)`
  object-fit: cover;
  margin-top: 6rem;
  ${mobileMediaQuery('margin-top: 3rem;')}
`;

export const Flex = styled.div`
  display: flex;
  overflow: hidden;
  ${mobileMediaQuery('margin: 0 0.5rem;')}
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  button {
    padding: 1rem 2rem;
    border-radius: 1000px;
    margin-top: 3rem;
    ${mobileMediaQuery('padding: 0.75rem 2rem; margin-top: 2rem;')}
  }
`;
