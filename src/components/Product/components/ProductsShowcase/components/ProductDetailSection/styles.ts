import { motion } from 'framer-motion';
import styled from 'styled-components';

import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.div<{ fullMargin?: boolean }>`
  display: flex;
  height: 90vh;
  flex-direction: column;
  justify-content: center;
  position: relative;
  margin: 5rem 0;
  ${({ fullMargin }) => fullMargin && 'margin-top: 90vh'};
  ${mobileMediaQuery(`height: 40vh;`)}
`;

export const Container = styled.div<{ column?: boolean }>`
  display: flex;
  flex-direction: ${({ column = false }) => (column ? 'row' : 'column')};
  align-items: center;
  z-index: 2;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Title = styled(motion.div)`
  width: 50%;
  display: flex;
  justify-content: center;
  ${mobileMediaQuery(`
    width: 100%;
    span {
      font-size: 1.5rem;
      width: 80%;
    }
  `)}
`;

export const Description = styled(motion.div)`
  width: 40%;
  display: flex;
  margin: 1rem 0 2rem 0;
  ${mobileMediaQuery(`
    width: 80%;
    span {
      font-size: 0.875rem;
    }
  `)}
`;

export const ImageWrapper = styled(motion.div)`
  position: relative;
  width: 60vw;
  ${mobileMediaQuery(`width: 80vw;`)}
`;

export const FloatingImageWrapper = styled(motion.div)`
  position: absolute;
  left: -20%;
  top: 50%;
  transform: translate(0%, -50%);
  width: 60%;
  ${mobileMediaQuery(` 
    left: -10%;
    width: 45%;
  `)}
`;

export const Image = styled.img`
  border-radius: 20px;
  object-fit: cover;
  width: inherit;
  height: inherit;
`;

export const Inline = styled.p`
  text-align: center;
  > span:nth-child(1) {
    display: inline-block;
  }
`;

export const Banner = styled.div`
  display: flex;
  justify-content: center;
  background: #bf0c0ccc;
  border-radius: 100px;
  width: 80%;
  position: absolute;
  bottom: -8%;
  left: 50%;
  transform: translateX(-50%);
  padding: 1rem;
  ${mobileMediaQuery(`padding: 0.5rem 1rem;`)}
  span {
    ${mobileMediaQuery(`font-size: 1rem;`)}
  }
`;
