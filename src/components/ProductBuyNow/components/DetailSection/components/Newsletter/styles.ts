import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.div`
  display: flex;
  flex-direction: column;
  padding: 3rem 3rem 3rem 7rem;
  background-color: ${(p) => p.theme.palette.common.white};
  span:nth-child(2) {
    margin-top: 1rem;
  }
  ${mobileMediaQuery('padding: 2rem;')}
`;

export const Flex = styled.div`
  display: flex;
  align-items: center;
  margin-top: 2rem;
  cursor: pointer;
  &:hover {
    .signup-icon {
      transition: 300ms all;
      background-color: ${(p) => p.theme.palette.primary.main};
      border: 0;
      path {
        transition: 300ms all;
        stroke: white;
      }
    }
  }
`;

export const Icon = styled.div`
  width: 2.25rem;
  height: 1.75rem;
  border-radius: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 1rem;
  border: 1px solid rgba(0, 0, 0, 0.2);
`;
