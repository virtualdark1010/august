import {
  AquaVision,
  AutoEditing,
  CarouselWithInfo,
  ChargeCase,
  ControlFromFar,
  FitsYourFeed,
  HDRVideo,
  Hyperlapse,
  MountAnywhere,
  Preview,
  ProductCarousel,
  SlowMotion,
  StandAndDeliver,
  SuperCharged,
  Timelapse,
  Waterproof,
} from 'components/ProductTwo/components';

import Header from 'shared/Header';
import Footer from 'shared/Footer';

import carouselImageOne from 'assets/images/carousel-image.png';
import carouselImageTwo from 'assets/images/carousel-2-image.png';
import carouselOneInfoSectionImage from 'assets/images/pov-capture.png';
import carouselTwoInfoSectionImage from 'assets/images/product-2-image.png';

import { Main } from './styles';

const ProductTwo = () => {
  return (
    <Main>
      <Header />
      <ProductCarousel />
      <CarouselWithInfo
        addBottomMargin
        floatingImage
        carouselImage={[carouselImageOne, carouselImageOne, carouselImageOne]}
        infoSectionImage={carouselOneInfoSectionImage}
        title='POV Capture'
        subTitle='Look, no hands!'
        description="Insta360 GO 2 is the world's smallest action camera. It weighs about 6x lighter than your old action cam, with the same sized image sensor, and fits perfectly on your shirt."
      />
      <CarouselWithInfo
        reverse
        carouselImage={[carouselImageTwo, carouselImageTwo, carouselImageTwo]}
        infoSectionImage={carouselTwoInfoSectionImage}
        title='FlowState Stabilization'
        subTitle='Buttery smooth goodness.'
        description='You gotta see it to believe it. Anywhere you mount GO 2, FlowState Stabilization will keep your shot steady. Shake, bump and roll ready.'
      />
      <MountAnywhere />
      <Hyperlapse />
      <Timelapse />
      <SlowMotion />
      <HDRVideo />
      <ChargeCase />
      <SuperCharged />
      <ControlFromFar />
      <StandAndDeliver />
      <Waterproof />
      <AquaVision />
      <FitsYourFeed />
      <AutoEditing />
      <Preview />
      <Footer />
    </Main>
  );
};

export default ProductTwo;
