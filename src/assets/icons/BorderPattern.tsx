export const BorderPattern = () => (
  <svg
    width='275'
    height='466'
    viewBox='0 0 275 466'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
  >
    <g filter='url(#filter0_d_114_966)'>
      <path d='M4 4L271 37V429L4 462V233V4Z' fill='url(#paint0_linear_114_966)' />
    </g>
    <defs>
      <filter
        id='filter0_d_114_966'
        x='0'
        y='0'
        width='275'
        height='466'
        filterUnits='userSpaceOnUse'
        colorInterpolationFilters='sRGB'
      >
        <feFlood floodOpacity='0' result='BackgroundImageFix' />
        <feColorMatrix
          in='SourceAlpha'
          type='matrix'
          values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
          result='hardAlpha'
        />
        <feOffset />
        <feGaussianBlur stdDeviation='2' />
        <feComposite in2='hardAlpha' operator='out' />
        <feColorMatrix type='matrix' values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.5 0' />
        <feBlend mode='normal' in2='BackgroundImageFix' result='effect1_dropShadow_114_966' />
        <feBlend mode='normal' in='SourceGraphic' in2='effect1_dropShadow_114_966' result='shape' />
      </filter>
      <linearGradient
        id='paint0_linear_114_966'
        x1='4'
        y1='4'
        x2='304'
        y2='462'
        gradientUnits='userSpaceOnUse'
      >
        <stop stopColor='#FB1300' />
        <stop offset='1' />
      </linearGradient>
    </defs>
  </svg>
);
