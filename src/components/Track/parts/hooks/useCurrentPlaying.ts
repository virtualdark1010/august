import inProcessAnimation from 'src/animations/lottie/in_process.json';
import completedAnimation from 'src/animations/lottie/completed.json';
import queueAnimation from 'src/animations/lottie/queue.json';

const useCurrentAnimationPlaying = (state: string) => {
  if (state === 'completed') {
    return completedAnimation;
  } else if (state === 'in progress') {
    return inProcessAnimation;
  } else {
    return queueAnimation;
  }
};

export default useCurrentAnimationPlaying;
