import styled from 'styled-components';
import Lottie from 'react-lottie';
import notFoundAnimation from 'src/animations/lottie/404.json';
import Text from 'shared/Text';
import Button from 'shared/Button';

const NotFound = () => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: notFoundAnimation,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  };
  return (
    <Container>
      <LottieContainer>
        <Lottie options={defaultOptions} height={400} width={400} />
      </LottieContainer>
      <TextContainer>
        <Text text='UH OH!! Youre lost.' fontSize='3rem' fontFamily='ClashDisplayBold' />
        <Contents>
          <Text text='The page youre looking for does not exist. How you got here is a mystery.' />
          <Text text='But you can click the button below to go back to home page' />
          <Button onClick={() => console.log('click')}>Home</Button>
        </Contents>
      </TextContainer>
    </Container>
  );
};

const Contents = styled.div`
  display: flex;
  flex-direction: column;
  span {
    margin-bottom: 15px;
  }
`;

const LottieContainer = styled.div`
  flex: 2;
`;
const TextContainer = styled.div`
  flex: 3;
  display: flex;
  flex-direction: column;
  gap: 15px;
`;

const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  height: 100vh;
  justify-content: center;
  align-items: center;
  background-color: #000;
`;

export default NotFound;
