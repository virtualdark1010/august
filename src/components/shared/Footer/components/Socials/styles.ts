import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
`;

export const Icon = styled.div`
  margin: 1rem 1rem 0 0;
  cursor: pointer;
  transition: 250ms all;
  &:hover {
    transform: scale(1.2);
  }
`;
