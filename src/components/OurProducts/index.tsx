import React from 'react';

import ProductTile from 'components/OurProducts/components/ProductTile';
import Grid from 'components/shared/Grid';

import { withLayout } from 'hoc/withLayout';

import config from './config';
import { ProductContainer } from './styles';

const OurProduct = () => {
  return (
    <ProductContainer>
      <Grid>
        {(Item: typeof React.Component) =>
          config.map((product) => (
            <Item start={product?.start} end={product?.end}>
              <ProductTile {...product} />
            </Item>
          ))
        }
      </Grid>
    </ProductContainer>
  );
};

export default withLayout(OurProduct, {
  heading: {
    text: 'Our Products',
  },
});
