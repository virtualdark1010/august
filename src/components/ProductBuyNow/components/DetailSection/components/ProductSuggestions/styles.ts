import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.div`
  background-color: #f6f6f6e6;
`;

export const SliderWrapper = styled.div`
  display: block;
  padding: 0 0 3rem 4rem;
  ${mobileMediaQuery(`padding: 0 0 2rem 2rem;`)}
  &::-webkit-scrollbar {
    background: transparent;
    -webkit-appearance: none;
    width: 0;
    height: 0;
  }
  .slick-track {
    display: flex;
  }
  .slick-slide {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background: white;
    width: 15vw !important;
    height: 300px;
    border-radius: 10px;
    margin-right: 1rem;
    ${mobileMediaQuery(`width: 45vw !important;`)}
    & > div {
      width: 100%;
      height: 100%;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      & > div {
        display: flex !important;
        justify-content: space-between;
      }
    }
  }
`;

export const TopSection = styled.div`
  padding: 4rem;
  margin-left: 5rem;
  padding-left: 1.5rem;
  ${mobileMediaQuery(`
  padding: 2rem 1rem 0 0;
  margin-left: 2rem;
  padding-left: 0;
  `)}
`;

export const Flex = styled.div`
  margin-bottom: 2rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Item = styled.div`
  padding: 1rem;
  background-color: ${(p) => p.theme.palette.background.white};
  border-radius: 4px;
  height: 30vh;
  margin: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  span {
    align-self: center;
  }
`;

export const Image = styled.img`
  width: 25%;
  align-self: center;
  object-fit: cover;
`;

export const Divider = styled.div`
  height: 1px;
  width: 90%;
  align-self: center;
  margin: 1rem 0;
  opacity: 0.2;
  background: ${(p) => p.theme.palette.background.secondary};
`;

export const Arrow = styled.div`
  border: 1px solid rgba(37, 36, 36, 0.5);
  opacity: 0.5;
  border-radius: 100%;
  padding: 0.5rem;
  cursor: pointer;
  margin-right: 0.5rem;
  transition: 250ms all;
  &:hover {
    opacity: 1;
    transition: 250ms all;
    transform: scale(1.025);
    background-color: ${(p) => p.theme.palette.primary.main};
    border: 1px solid transparent;
    path {
      transition: 250ms all;
      stroke: white;
    }
  }
`;

export const TextWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
