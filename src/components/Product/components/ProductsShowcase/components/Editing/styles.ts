import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.div`
  background-color: ${(p) => p.theme.palette.background.primary};
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-bottom: 5rem;
`;

export const Container = styled(motion.div)`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;
  position: relative;
  width: 60vw;
  height: 60vh;
  margin: 10vw 0 5vw 0;
  span {
    ${mobileMediaQuery(`width: 100%; text-align: center;`)}
    margin-right: 4px;
    width: 50%;
    &:nth-child(2) {
      margin: 1rem 0;
    }
    &:nth-child(3) {
      width: 60%;
      ${mobileMediaQuery(`width: 100%;`)}
    }
  }
`;

export const Image = styled(motion.img)<{
  width?: string;
  height?: string;
  borderRadius?: string;
  x?: string;
  z?: number;
}>`
  object-fit: cover;
  position: absolute;
  right: -10%;
  bottom: -30%;
  width: 90%;
  ${mobileMediaQuery(`
    position: relative;
    right: 0%;
    bottom: 0%;
   `)}
`;
