import { ReactNode } from 'react';

export type ModalProps = {
  isOpen: boolean;
  children: ReactNode;
  bgColor?: string;
  top?: string;
  bottom?: string;
  left?: string;
  right?: string;
  width?: string;
  transform?: string;
  center?: boolean;
  positionFixed?: boolean;
};
