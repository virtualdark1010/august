import { useState, useEffect } from 'react';

export const useOnScreen = (ref: any) => {
  const [isVisible, setIsVisible] = useState(false);

  const observer = new IntersectionObserver(([entry]) => setIsVisible(entry.isIntersecting));

  useEffect(() => {
    observer.observe(ref.current);
    return () => {
      observer.disconnect();
    };
  }, []);

  return isVisible;
};
