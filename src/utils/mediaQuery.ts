import { device } from 'src/constants/screen';

export const mobileMediaQuery = (queries: string, additionalMediaQuery?: string) => `
    @media ${device.mobile}${additionalMediaQuery ? ` and ${additionalMediaQuery}` : ''} {
        ${queries}
    }
`;
