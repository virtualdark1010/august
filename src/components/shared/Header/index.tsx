import { isMobile } from 'react-device-detect';
import Navigation from 'components/Navigation';

import Button from 'components/shared/Button';
import { Logo, Menu, Search } from 'shared/icons';
import { Main, LogoWrapper, Nav, Right, MenuWrapper } from './styles';
import { useCycle } from 'framer-motion';
import { SliderMenu } from './components/SlideMenu';

const Header = () => {
  const [open, cycleOpen] = useCycle(false, true);

  const handleOnClick = () => cycleOpen();
  return (
    <Main className='august-header'>
      <LogoWrapper>
        <Logo />
      </LogoWrapper>
      <SliderMenu open={open} onClose={cycleOpen} />
      {isMobile ? (
        <MenuWrapper onClick={handleOnClick}>
          <Menu />
        </MenuWrapper>
      ) : (
        <>
          <Nav>
            <Navigation />
          </Nav>
          <Right>
            <Search />
            <Button
              onClick={() => {
                console.log('🚀 ~ file: index.tsx:15 ~ Header');
              }}
              borderColor='white'
              color='white'
            >
              Buy Now
            </Button>
          </Right>
        </>
      )}
    </Main>
  );
};

export default Header;
