import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  display: grid;
  grid-template-rows: repeat(12, 1fr);
  grid-template-columns: repeat(12, 1fr);
  gap: 0px;
  height: 100%;
  ${mobileMediaQuery(`
       display: flex;
       flex-direction: column;
  `)}
`;

export const LeftSection = styled.div`
  background-color: ${(p) => p.theme.palette.background.primary};
  grid-column-start: 1;
  grid-column-end: 7;
  grid-row-start: 1;
  grid-row-end: 13;
`;

export const RightSection = styled.div`
  background-color: ${(p) => p.theme.palette.background.secondary};
  grid-row-start: 1;
  grid-row-end: 13;
  grid-column-start: 7;
  grid-column-end: 13;
  .avatar {
    padding: 0;
  }
`;

export const Wrapper = styled.div<{ center?: boolean }>`
  width: 70%;
  display: flex;
  align-items: ${({ center = false }) => `${center ? 'center' : 'flex-start'}`};
  justify-content: center;
  flex-direction: column;
  ${mobileMediaQuery(`width: 90%; padding: 1rem 0 2rem 0;`)}
`;

export const VideoWrapper = styled.div`
  width: 90%;
  height: 100%;
  border-radius: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin-bottom: 1rem;
  > div {
    width: 100% !important;
  }
  body {
    border-radius: 100%;
  }
`;
