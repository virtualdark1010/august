export const getFontSize = {
  1: '14px',
  2: '16px',
  3: '18px',
  4: '20px',
  5: '24px',
  6: '28px',
};
