import { Facebook, Github, Hand, LinkedIn, Logo, Twitter } from 'shared/icons';
import { Globe } from '../icons/Globe';
import { Socials } from './components/Socials';

export default [
  {
    label: 'Products',
    links: [
      { name: 'Workspace 360', link: '' },
      { name: 'Smart Screen', link: '' },
      { name: 'ANC Headphones', link: '' },
    ],
  },
  {
    label: 'Resources',
    links: [
      { name: 'About Us', link: '' },
      { name: 'Event', link: '' },
      { name: 'Tutorial', link: '' },
    ],
  },
  {
    label: 'Company',
    links: [
      { name: 'Media', link: '' },
      { name: 'Blog', link: '' },
      { name: 'Pricing', link: '' },
    ],
  },
  {
    label: 'Legal',
    links: [
      { name: 'Terms', link: '' },
      { name: 'Policy', link: '' },
      { name: 'Support', link: '' },
    ],
  },
];

export const infoConfig = [
  {
    label: 'August',
    icon: <Logo />,
    path: null,
  },
  {
    label:
      'This growth plan will help you reach your resolutions and achieve the goals you have been striving towards.',
    icon: null,
    path: null,
  },
  {
    label: null,
    icon: <Socials />,
    path: '/privacy',
  },
];

export const socialLinks = [
  {
    icon: <Twitter />,
    link: '',
  },
  {
    icon: <LinkedIn />,
    link: '',
  },
  {
    icon: <Facebook />,
    link: '',
  },
  {
    icon: <Github />,
    link: '',
  },
  {
    icon: <Hand />,
    link: '',
  },
  {
    icon: <Globe />,
    link: '',
  },
];
