import styled from 'styled-components';

export const Main = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  margin-top: 3rem;
  button {
    z-index: 1;
  }
`;

export const Line = styled.div`
  position: absolute;
  top: 50%;
  height: 1px;
  width: 100%;
  background: ${(p) => p.theme.palette.primary.main};
  opacity: 0.5;
`;
