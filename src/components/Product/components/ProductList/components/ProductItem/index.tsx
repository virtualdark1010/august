import React from 'react';
import { isMobile } from 'react-device-detect';
import Text from 'shared/Text';
import { TabProductItem } from 'types/tabProductItem';
import { Main, Image } from './styles';

const ProductItem = (props: TabProductItem): any => {
  if (!props.data) {
    return null;
  }
  return props?.data?.map((item) => (
    <Main key={item.name}>
      <Image src={item.image} />
      <Text text={item.name} color='#FD1515' weight='bold' level={isMobile ? '2' : '3'} />
      <Text text={item.price} />
    </Main>
  ));
};

export default ProductItem;
