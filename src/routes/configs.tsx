import NotFound from 'components/NotFound';
import ProductBuyNow from 'components/ProductBuyNow';
import Signup from 'components/Signup';
import Login from 'components/Login';
import ForgotPassword from 'components/ForgotPassword';

import LandingPage from 'pages/Landing';
import ProductPage from 'pages/Product';
import CartPage from 'pages/Cart';
import CheckoutPage from 'pages/Checkout';
import Track from 'pages/Track';
import ProductTwoPage from 'pages/ProductTwo';

export default [
  {
    path: '/',
    element: <LandingPage />,
  },
  {
    path: '/product',
    element: <ProductPage />,
  },
  {
    path: '/product-two',
    element: <ProductTwoPage />,
  },
  {
    path: '/buy-now',
    element: <ProductBuyNow />,
  },
  {
    path: '/cart',
    element: <CartPage />,
  },
  {
    path: '/checkout',
    element: <CheckoutPage />,
  },
  {
    path: '/signup',
    element: <Signup />,
  },
  {
    path: '/login',
    element: <Login />,
  },
  {
    path: '/forgot-password',
    element: <ForgotPassword />,
  },
  {
    path: '/track',
    element: <Track />,
  },
  {
    path: '*',
    element: <NotFound />,
  },
];
