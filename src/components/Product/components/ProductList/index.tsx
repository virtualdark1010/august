import Dropdown from 'components/shared/Dropdown';
import { Tabs } from 'components/shared/Tabs';
import { withLayout } from 'hoc/withLayout';

import { Container } from './styles';

const options = [
  { value: 'product', label: 'Product' },
  { value: 'actions', label: 'Action' },
];

const ProductList = (props: any) => {
  return (
    <Container>
      <Tabs {...props} />
      <Dropdown
        options={options}
        onChange={(e) => {
          console.log('🚀 ~ file: index.tsx:22 ~ ProductList ~ e', e);
        }}
      />
    </Container>
  );
};

export default withLayout(ProductList, { fullHeight: true });
