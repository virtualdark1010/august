import { AvatarType } from 'types/avatar';
import { getInitials } from 'utils/name';
import { Container } from './styles';

const Avatar = (props: AvatarType) => {
  const { children, initials } = props;
  const render = initials && typeof children === 'string' ? getInitials(children) : children;

  return (
    <Container className='flex items-center justify-center avatar' {...props}>
      {render}
    </Container>
  );
};

export default Avatar;
