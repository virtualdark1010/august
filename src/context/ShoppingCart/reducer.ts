import { ShoppingCartActionsMap } from './../ShoppingCart';
import { ShoppingCartActionType, ShoppingCartInitialState } from './types';

export const shoppingCartReducer = (
  state: ShoppingCartInitialState,
  action: ShoppingCartActionType,
): ShoppingCartInitialState => {
  switch (action.type) {
    case ShoppingCartActionsMap.add:
      const cartTotal =
        state.cartTotal === 0
          ? action.payload.price
          : state.cart.reduce(
              (sum, cart) => (sum += Number(cart.price)),
              action.payload.price ?? 0,
            );
      return {
        ...state,
        cartTotal,
        numberOfItems: state.numberOfItems + 1,
        cart: [...state?.cart, action.payload],
      };
    case ShoppingCartActionsMap.remove:
      const itemToRemove = state.cart.filter((list) => list.id !== action.payload);
      return {
        ...state,
        ...action.payload,
        cart: itemToRemove,
      };
    case ShoppingCartActionsMap.setStep:
      return {
        ...state,
        step: action.payload,
      };
    default:
      return state;
  }
};
