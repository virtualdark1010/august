import { motion } from 'framer-motion';
import styled from 'styled-components';

export const Main = styled.main`
  display: flex;
`;

export const Menu = styled(motion.aside)`
  background-color: white;
  width: 18.75rem;
  height: 100vh;
  position: absolute;
  top: 0;
  right: 0;
  .august-logo {
    margin: 1rem 0 0 1rem;
    path {
      fill: ${(p) => p.theme.palette.primary.main};
    }
  }
`;

export const Links = styled(motion.div)`
  margin: 2.5rem 1.5rem;
  display: flex;
  flex-direction: column;
`;

export const Flex = styled(motion.div)<{ active?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0.5rem 1rem;
  border-radius: 10px;
  margin-bottom: 1rem;
  position: relative;
  background: ${({ active, theme }) =>
    active ? theme.palette.primary.main : theme.palette.common.white};
  span {
    color: ${({ active, theme }) =>
      active ? theme.palette.common.white : theme.palette.common.black};
  }
  path {
    stroke: ${({ active, theme }) =>
      active ? theme.palette.common.white : theme.palette.common.black};
  }
`;

export const Group = styled(motion.div)`
  display: flex;
  align-items: center;
  svg {
    margin-right: 0.5rem;
  }
`;

export const TextWrapper = styled(motion.div)``;

export const Row = styled(motion.div)``;

export const Arrow = styled(motion.div)``;

export const SubMenu = styled(motion.div)`
  padding-left: 1rem;
`;

export const CloseMenu = styled(motion.div)`
  position: absolute;
  top: 20%;
  right: 4%;
`;
