import styled from 'styled-components';

export const Main = styled.div`
  width: 100%;
  height: 100%;
  .ft-slick__dots--custom {
    height: 10px;
    width: 10px;
    background-color: #ffffff50;
    border-radius: 4px;
    position: relative;
  }
  .slick-dots {
    bottom: 50px;
    align-items: center !important;
    justify-content: center !important;
    display: flex !important;
  }
  .slick-dots li {
    width: 14px;
    margin: 0 1.5rem;
    transition: width 0.3s ease-in-out;
  }

  .slick-dots .slick-active {
    width: 36px;
    transition: width 0.3s ease-in-out;
  }

  .slick-dots .slick-active .ft-slick__dots--custom {
    width: 36px;
    top: 0px;
    overflow: hidden;
    background: #bf0c0c;
  }
`;

export const Image = styled.img`
  width: 100%;
  /* height: 100vh; */
  align-self: center;
  object-fit: cover;
`;
