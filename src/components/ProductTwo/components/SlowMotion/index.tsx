import React from 'react';

import { Main, RightSection, TextWrapper, FloatingImage, Image, ImageWrapper } from './styles';
import Text from 'shared/Text';
import slowMotionImage from 'assets/images/slow-motion.png';
import floatingPhone from 'assets/images/slow-motion-phone.png';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const SlowMotion = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <FloatingImage
        {...config({
          customConfig: {
            variants: {
              visible: {
                opacity: 1,
                rotate: 360,
                x: '-50%',
                y: '-50%',
                scale: 1,
                transition: { duration: 2, type: 'spring', stiffness: 50 },
              },
              hidden: {
                opacity: 0,
                rotate: 0,
                scale: 0,
                transition: { duration: 2, type: 'spring', stiffness: 50 },
              },
            },
          },
        })}
      >
        <Image src={floatingPhone} />
      </FloatingImage>
      <ImageWrapper>
        <Image
          src={slowMotionImage}
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  transition: { duration: 3, delay: 0.5 },
                },
                hidden: {
                  opacity: 0,
                  transition: { duration: 3, delay: 0.5 },
                },
              },
            },
          })}
        />
      </ImageWrapper>
      <RightSection>
        <TextWrapper>
          <Text
            text='SlowMotion'
            fontSize='2rem'
            weight='bold'
            color='#BF0C0C'
            center={isMobile}
            {...config({ leftToRight: true, stiffness: 50, duration: 2, delay: 0.5 })}
          />
          <Text
            text='Oooooh slow-mo.'
            fontSize={isMobile ? '2rem' : '5rem'}
            center={isMobile}
            weight='bold'
            color='#1E1E1E'
            {...config({ leftToRight: true, stiffness: 50, duration: 2, delay: 0.7 })}
          />
          <Text
            text='Couldn’t handle the speed? Slow a key moment down to 4x slow-mo in super-smooth 120fps.'
            fontSize={isMobile ? '1rem' : '1.75rem'}
            center={isMobile}
            color='#1E1E1E60'
            {...config({ leftToRight: true, stiffness: 50, duration: 2, delay: 0.9 })}
          />
        </TextWrapper>
      </RightSection>
    </Main>
  );
};

export { SlowMotion };
