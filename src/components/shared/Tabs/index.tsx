import { useState } from 'react';

import Button from 'components/shared/Button';
import { Tab } from 'types/tabs';
import { Container, Content, TabRow } from './styles';

export const Tabs = ({ tabs }: { tabs: any }) => {
  const [activeIndex, activeTab] = useState<number>(0);
  return (
    <Container>
      <TabRow>
        {tabs?.map((tab: Tab, i: number) => {
          const isActive = activeIndex === i;
          return (
            <Button
              key={i}
              bgColor={isActive ? '#BB000E' : ''}
              onClick={() => {
                activeTab(i);
                console.log('🚀 ~ file: index.tsx:15 ~ Tabs ~ (');
              }}
              color='white'
              shape={tab.shape}
            >
              {tab.label}
            </Button>
          );
        })}
      </TabRow>
      {tabs?.map((tab: Tab, i: number) => {
        const { Render } = tab;
        const isActive = activeIndex === i;
        return (
          isActive && (
            <Content key={i}>
              <Render />
            </Content>
          )
        );
      })}
    </Container>
  );
};
