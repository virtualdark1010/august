import Text from 'shared/Text';
import Button from 'shared/Button';
import { Logo } from 'shared/icons';
import {
  Container,
  LogoContainer,
  TextContainer,
  FormWrapper,
  FormGroup,
  Input,
  Label,
  ButtonContainer,
} from './styles';
import LinkText from 'shared/LinkText';

const ForgotPassword = () => {
  return (
    <Container>
      <LogoContainer>
        <Logo />
      </LogoContainer>
      <TextContainer>
        <Text
          text='Reset Password'
          color='#fff'
          margin='0 0 1rem 0'
          fontFamily='ClashDisplayBold'
        />
        <Text
          text='Enter the email address associated associated with you account and we will send you link to reset password '
          level='2'
          color='#fff'
          fontFamily='InterRegular'
        />
      </TextContainer>
      <FormWrapper>
        <FormGroup>
          <Input type='text' name='email' />
          <Label htmlFor='email'>Email</Label>
        </FormGroup>
        <ButtonContainer>
          <Button
            onClick={() => console.log('submit')}
            bgColor='#fd1515'
            color='#fff'
            width='max-content'
          >
            Send Reset Link
          </Button>
        </ButtonContainer>
        <LinkText text='Not a member yet? ' name='Signup' url='#' />
      </FormWrapper>
    </Container>
  );
};

export default ForgotPassword;
