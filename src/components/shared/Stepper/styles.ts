import Text from 'shared/Text';
import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  margin: 1rem 0;
`;

export const Line = styled.div`
  height: 4px;
  width: 50px;
  background-color: white;
  margin: 0 1rem;
`;

export const Step = styled(Text)`
  cursor: pointer;
`;
