import { HTMLInputTypeAttribute } from 'react';

export type InputType = {
  type: HTMLInputTypeAttribute;
  name: string;
  placeholder?: string;
  label?: string;
  fullBorder?: boolean;
  onChange?: (value: string) => void;
};
