import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  padding: 5rem;
  display: flex;
  margin-bottom: 30vh;
  position: relative;
  background-color: #bf0c0c;
  ${mobileMediaQuery(`padding: 2rem; margin-bottom: 0; flex-direction: column;`)}
`;

export const TextWrapper = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  .text-container:nth-child(2) {
    line-height: 5rem;
    margin-bottom: 3rem;
    margin-top: 1rem;
    width: 85%;
  }
  ${mobileMediaQuery(`
      width: 100%; 
      .text-container:nth-child(2) {
        line-height: 2rem;
        margin-bottom: 1rem;
        width: 100%;
  }
  `)}
`;

export const ImageOne = styled.img`
  object-fit: contain;
  height: 100%;
  ${mobileMediaQuery(`width: 90%;margin: 2rem auto;`)}
`;

export const FloatingImage = styled.img`
  position: absolute;
  right: -12%;
  bottom: -22%;
  width: 45%;
  transform: translateX(-50%);
  ${mobileMediaQuery(`display: none;`)}
`;
