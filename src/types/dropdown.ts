export type Option = {
  value: string;
  label: string;
};

export type DropdownType = {
  options: any[];
  defaultValue?: string;
  placeholder?: string;
  onChange?: (value: string) => void;
};
