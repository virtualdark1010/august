import { CardType } from 'types/card';
import { Container } from './styles';

const Card = (props: CardType) => {
  const { children } = props;
  return (
    <Container className='flex items-center justify-center card-container' {...props}>
      {children}
    </Container>
  );
};

export default Card;
