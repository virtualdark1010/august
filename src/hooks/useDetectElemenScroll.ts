import { useCallback, useEffect, useRef, useState } from 'react';

const useDetectElemenScroll = () => {
  const ref = useRef<any>();
  const [scrolled, setScrolled] = useState<boolean>(false);

  const handleScroll = useCallback(() => {
    if (ref?.current?.scrollTop === 0) {
      setScrolled(false);
      return;
    }
    setScrolled(true);
  }, []);

  useEffect(() => {
    ref?.current?.addEventListener('scroll', handleScroll);

    return () => {
      ref?.current?.removeEventListener('scroll', handleScroll);
    };
  }, [handleScroll]);

  return { scrolled, ref };
};

export { useDetectElemenScroll };
