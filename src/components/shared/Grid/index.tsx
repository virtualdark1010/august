import { GridType } from 'types/grid';
import { Container, Item } from './styles';

const Grid = (props: GridType) => {
  const { children } = props;
  return (
    <Container className='grid-container' {...props}>
      {children(Item)}
    </Container>
  );
};

export default Grid;
