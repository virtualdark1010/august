import styled from 'styled-components';
import { motion } from 'framer-motion';
import { ModalProps } from 'global/types';

export const Wrapper = styled(motion.div)`
  position: ${(p) => (p.positionFixed ? 'fixed' : 'absolute')};
  top: 110px;
  padding: 4rem 6rem;
  left: 0;
  width: 100vw;
  height: max-content;
  background: ${(p) => p.bgColor || p.theme.palette.common.white};
  border-radius: ${(p) => p.theme.borderRadius};
  z-index: 10;
  ${(p) => p.theme.headerShadow};
  ${(p: Partial<ModalProps>) => {
    const { center } = p;
    return center
      ? `
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: max-content;
    padding: 2rem;
    `
      : '';
  }};
`;
