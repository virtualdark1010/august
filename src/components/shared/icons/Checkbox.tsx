export const Checkbox = (
  props: React.SVGProps<SVGSVGElement> & {
    checked?: boolean;
  },
) => {
  const { checked = false } = props;
  return (
    <svg
      width='20'
      height='20'
      viewBox='0 0 20 20'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      {...props}
    >
      <rect x='0.5' y='0.5' width='19' height='18.6154' stroke='white' />
      {checked && (
        <path
          fill-rule='evenodd'
          clip-rule='evenodd'
          d='M15.5113 5.07592C15.8747 5.35298 15.9403 5.86647 15.6578 6.22282L9.17861 14.3959C9.03432 14.5779 8.81836 14.6917 8.58395 14.7092C8.34955 14.7267 8.11852 14.6463 7.94781 14.4877L4.42692 11.2185C4.09271 10.9082 4.07827 10.3909 4.39469 10.0631C4.7111 9.73531 5.23854 9.72116 5.57275 10.0315L8.42664 12.6814L14.3419 5.21949C14.6244 4.86314 15.148 4.79886 15.5113 5.07592Z'
          fill='#BB000E'
        />
      )}
    </svg>
  );
};
