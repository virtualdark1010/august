import React from 'react';
import { isMobile } from 'react-device-detect';
import GlowPatch from 'shared/GlowPatch';
import { useAnimate } from 'hooks/useAnimate';
import { Main, Image } from './styles';

const CameraPreview = ({
  imageUrl,
  leftToRight = false,
}: {
  imageUrl: string;
  leftToRight?: boolean;
}) => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <Image src={imageUrl} {...config({ leftToRight })} width={isMobile ? '50vw' : '30vw'} />
      <GlowPatch position='right' />
    </Main>
  );
};

export default CameraPreview;
