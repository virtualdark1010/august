import React from 'react';

import Text from 'shared/Text';
import HRVImage from 'assets/images/hdr-video.png';

import { Main, TextWrapper, Image } from './styles';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const HDRVideo = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <TextWrapper>
        <Text
          text='HDR Video'
          fontSize={isMobile ? '1rem' : '2rem'}
          weight='bold'
          color='#BF0C0C'
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  rotate: 360,
                  y: 0,
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.5 },
                },
                hidden: {
                  opacity: 0,
                  rotate: 0,
                  y: '-100vw',
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.5 },
                },
              },
            },
          })}
        />
        <Text
          text='All the colors.'
          fontSize={isMobile ? '2rem' : '5rem'}
          weight='bold'
          color='#1E1E1E'
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  rotate: 360,
                  y: 0,
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.7 },
                },
                hidden: {
                  opacity: 0,
                  rotate: 0,
                  y: '-100vw',
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.7 },
                },
              },
            },
          })}
        />
        <Text
          text='Pump up the colors with HDR mode. Get shots in a tap that look like they were edited by a pro. Ideal for stationary shots.'
          fontSize={isMobile ? '0.875rem' : '1.75rem'}
          color='#1E1E1E60'
          center
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  rotate: 360,
                  y: 0,
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.9 },
                },
                hidden: {
                  opacity: 0,
                  rotate: 0,
                  y: '100vw',
                  transition: { duration: 2, type: 'spring', stiffness: 50, delay: 0.9 },
                },
              },
            },
          })}
        />
      </TextWrapper>
      <Image
        src={HRVImage}
        {...config({
          customConfig: {
            variants: {
              visible: {
                opacity: 1,
                rotate: 360,
                x: 0,
                y: 0,
                transition: { duration: 2, type: 'spring', stiffness: 50 },
              },
              hidden: {
                opacity: 0,
                rotate: 0,
                x: '-100vw',
                transition: { duration: 2, type: 'spring', stiffness: 50 },
              },
            },
          },
        })}
      />
    </Main>
  );
};

export { HDRVideo };
