import { FooterType } from 'types/footer';
import links from 'components/shared/Footer/config';

import { Container, LinksWrapper, Label, Column, Link } from './styles';

const FooterLinks = () => {
  const renderLinks = links.map(({ label, links }: FooterType) => (
    <Column key={label}>
      <Label>{label}</Label>
      <Column>
        {links.map((link) => (
          <Link key={link.name}>{link.name}</Link>
        ))}
      </Column>
    </Column>
  ));

  return (
    <Container>
      <LinksWrapper>{renderLinks}</LinksWrapper>
    </Container>
  );
};

export default FooterLinks;
