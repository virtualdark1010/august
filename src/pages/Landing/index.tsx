import Feature from 'components/Feature';
import OurProducts from 'components/OurProducts';
import EngineeringExcellence from 'components/EngineeringExcellence';
import JoinOurCommunity from 'components/JoinOurCommunity';
// import VideoPlayer from 'components/Product/components/ProductsShowcase/components/VideoPlayer';
import Marquee from 'components/Marquee';
import RavagingFans from 'components/RavagingFans';

import { withLayout } from 'hoc/withLayout';
import { withHF } from 'hoc/withHF';

const LandingPage = () => (
  <>
    {/* <VideoPlayer path='src/assets/video/talktsy.mp4' /> */}
    <Feature />
    <OurProducts />
    <EngineeringExcellence />
    <Marquee />
    <RavagingFans />
    <JoinOurCommunity />
  </>
);

export default withHF(withLayout(LandingPage));
