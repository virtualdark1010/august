import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding: 5rem 0;
  position: relative;
  overflow: hidden;
  ${mobileMediaQuery(`padding: 2rem 0;`)}
`;

export const TextWrapper = styled.div`
  width: 60%;
  display: flex;
  flex-direction: column;
  align-items: center;
  span:nth-child(2) {
    line-height: 3rem;
    margin-bottom: 4rem;
  }
  ${mobileMediaQuery(`width: 90%;
   span:nth-child(2) {
     margin-bottom: 1rem;
   }
  `)}
`;

export const Image = styled(motion.img)`
  width: 50%;
  object-fit: cover;
  margin-top: 6rem;
`;
