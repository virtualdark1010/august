import styled from 'styled-components';

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 2rem;
`;

export const LogoContainer = styled.div`
  margin-bottom: 5rem;
`;

export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 60%;
  text-align: center;
  @media only screen and (max-width: 768px) {
    width: 100%;
  }
`;

export const HeadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  span:nth-child(1) {
    font-size: 4rem;
    line-height: 4rem;
  }
  span:nth-child(2) {
    font-size: 1rem;
  }
  @media only screen and (max-width: 768px) {
    span:nth-child(1) {
      font-size: 2.5rem;
      line-height: 2.5rem;
    }
    span:nth-child(2) {
      font-size: 0.7rem;
      margin-top: 15px;
    }
  }
`;

export const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  width: 70%;
  @media only screen and (max-width: 768px) {
    width: 100%;
  }
`;

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FormGroup = styled.div`
  position: relative;
  margin: 1rem 1rem 1rem 0;
`;

export const LeftSide = styled.div`
  flex: 2;
  align-items: center;
  display: flex;
  justify-content: center;
  border-right: 1px solid #ccc;
  height: 100vh;
  @media only screen and (max-width: 768px) {
    display: none;
  }
`;

export const RightSide = styled.div`
  flex: 3;
  align-items: center;
  display: flex;
  justify-content: center;
  height: 100vh;
  padding: 0 5rem;
  background-color: ${(p) => p.theme.palette.background.primary};
  flex-direction: column;
`;
