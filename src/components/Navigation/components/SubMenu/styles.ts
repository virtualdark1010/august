import styled from 'styled-components';

export const Conatainer = styled.div`
  z-index: 10;
`;

export const Main = styled.div`
  margin-right: 2rem;
`;

export const Badge = styled.span`
  padding: 2px 0.4rem;
  text-transform: capitalize;
  margin-left: 0.5rem;
  border-radius: 2px;
  color: ${(p) => p.theme.palette.common.white};
  background-color: ${(p) => p.theme.palette.primary.main};
`;

export const CardWrapper = styled.div`
  display: flex;
  align-items: center;
  margin: 2rem 0;
  overflow: auto;
  .card-container {
    overflow: hidden;
    padding: 1rem 1rem 0 2rem;
    justify-content: space-between;
  }
`;

export const CardContent = styled.div`
  display: flex;
  flex-direction: column;
`;

export const CardHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const CardBody = styled.div`
  margin-top: 1rem;
`;

export const CardFooter = styled(CardHeader)`
  width: 100%;
`;

export const ImageWrapper = styled.div``;

export const Image = styled.img`
  object-fit: cover;
  width: 100%;
`;
