import React from 'react';
import TopFeatures from 'components/Product/components/ProductsShowcase/components/TopFeatures';
import Lenses from 'components/Product/components/ProductsShowcase/components/Lenses';
import NewExperience from 'components/Product/components/ProductsShowcase/components/NewExperience';
import Editing from 'components/Product/components/ProductsShowcase/components/Editing';
import SlideIndicator from 'components/Product/components/ProductsShowcase/components/SlideIndicator';

import { useSetActiveSlide } from 'components/Product/components/ProductsShowcase/hooks/useSetActiveSlide';

import { Main, Slide } from './styles';

const ProductsShowcase = () => {
  const {
    slides,
    hideSlider,
    activeSlide,
    lensesElementRef,
    editingElementRef,
    topFeatureElementRef,
    newExperienceElementRef,
  } = useSetActiveSlide();

  return (
    <Main>
      {hideSlider && <SlideIndicator slides={slides} activeSlide={activeSlide} />}
      <Slide ref={lensesElementRef} className='lenses'>
        <Lenses />
      </Slide>
      <Slide ref={topFeatureElementRef} className='topFeatures'>
        <TopFeatures />
      </Slide>
      <Slide ref={newExperienceElementRef} className='newExperience'>
        <NewExperience />
      </Slide>
      <Slide ref={editingElementRef} className='editing'>
        <Editing />
      </Slide>
    </Main>
  );
};

export default ProductsShowcase;
