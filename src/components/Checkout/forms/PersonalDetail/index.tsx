import React, { useState } from 'react';
import Button from 'shared/Button';
import Text from 'shared/Text';
import { Checkbox } from 'shared/icons/Checkbox';

import { useCart } from 'hooks/useCart';

import { fields } from './fields';

import { Container, Form, Field, SaveDetail } from 'components/Checkout/styles';

const PersonalDetail = () => {
  const [checked, setChecked] = useState<boolean>(false);
  const { setStep } = useCart();
  return (
    <Container>
      <Form>
        {fields.map((field: any) => {
          const { Render, props, width } = field;
          return (
            <Field width={width}>
              <Render {...props} />
            </Field>
          );
        })}
        <SaveDetail onClick={() => setChecked(!checked)}>
          <Checkbox checked={checked} />
          <Text text='Save contact information ' color='white' level='3' />
        </SaveDetail>
        <Button onClick={() => setStep('Shipping')} variant='inverted' width='100%'>
          Continue to shipping
        </Button>
      </Form>
    </Container>
  );
};

export default PersonalDetail;
