import React from 'react';
import Button from 'shared/Button';
import ShippingCard from './components/ShippingCard';

import { useCart } from 'hooks/useCart';

import { Container } from './styles';

const ShippingDetail = () => {
  const { setStep } = useCart();
  return (
    <Container>
      <ShippingCard checked company='DHL' time='In 7-8 Business days' />
      <ShippingCard company='UPS' time='In 4-5 Business days' />
      <Button onClick={() => setStep('Payment')} variant='inverted' width='100%'>
        Continue to payment
      </Button>
    </Container>
  );
};

export default ShippingDetail;
