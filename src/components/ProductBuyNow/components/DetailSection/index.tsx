import { isMobile } from 'react-device-detect';
import Text from 'components/shared/Text';
import {
  KeyFeatures,
  VideoSection,
  Newsletter,
  ProductSuggestions,
} from 'components/ProductBuyNow/components/DetailSection/components';
import { useDetectElemenScroll } from 'hooks/useDetectElemenScroll';

import { Container, ProductDetail, SectionDivider, Line } from './styles';

const videoList = [
  'https://www.youtube.com/watch?v=ao4RCon11eY&ab_channel=DVRST',
  'https://www.youtube.com/watch?v=ao4RCon11eY&ab_channel=DVRST',
  'https://www.youtube.com/watch?v=ao4RCon11eY&ab_channel=DVRST',
];

const DetailSection = () => {
  const { ref, scrolled } = useDetectElemenScroll();

  return (
    <Container ref={ref} scrolled={scrolled}>
      <Line />
      <ProductDetail>
        <Text
          className='pl-6'
          text='360 Workspace'
          color='#FD1515'
          fontSize={isMobile ? '2rem' : '4rem'}
          weight='700'
        />
        <Text
          className='pl-6'
          text='Sub title goes here'
          fontSize='1.25rem'
          weight='700'
          color='black'
        />
        <SectionDivider />
        <KeyFeatures />
        <VideoSection videoList={videoList} title='Tuitorials' />
        <VideoSection videoList={videoList} title='In action' />
      </ProductDetail>
      <ProductSuggestions />
      <Newsletter />
    </Container>
  );
};

export default DetailSection;
