import { ButtonType } from 'types/button';
import { Container } from './styles';

const Button = (props: ButtonType) => {
  const { children, onClick } = props;
  return (
    <Container className='button' {...props} onClick={onClick}>
      {children}
    </Container>
  );
};

export default Button;
