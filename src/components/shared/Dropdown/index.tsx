import Select from 'react-select';
import { DropdownType } from 'types/dropdown';
import { Wrapper } from './styles';

const Dropdown = ({ options, defaultValue, onChange, placeholder }: DropdownType) => {
  const handleOnChange = (option: any) => {
    onChange?.(option?.value);
  };
  return (
    <Wrapper>
      <Select
        defaultValue={defaultValue || ''}
        placeholder={placeholder || `Sort by:`}
        styles={{
          placeholder: (base) => ({
            ...base,
            fontSize: '1em',
            fontWeight: 400,
          }),
        }}
        options={options}
        onChange={handleOnChange}
      />
    </Wrapper>
  );
};

export default Dropdown;
