import styled from 'styled-components';

export const LinkContainer = styled.div`
  width: 100%;
  text-align: center;
  margin-top: 2rem;
  color: #fff;
  & a {
    color: red;
    border-bottom: 1px solid #ccc;
    padding-bottom: 5px;
    cursor: pointer;
  }
  @media only screen and (max-width: 768px) {
    font-size: 0.7rem;
  }
`;
