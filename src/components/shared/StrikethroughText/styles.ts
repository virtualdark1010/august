import styled from 'styled-components';

export const StrikedThroughText = styled.div`
  position: relative;
  z-index: 1;
  width: 100%;
  &:before {
    border-top: 1px solid red;
    content: '';
    margin: 0 auto;
    position: absolute;
    top: 50%;
    left: 0;
    right: 0;
    bottom: 0;
    width: 50%;
    z-index: -1;
  }
  & span {
    background: #000;
    padding: 0 15px;
  }
  @media only screen and (max-width: 768px) {
    & span {
      font-size: 0.6rem !important;
    }
  }
`;
