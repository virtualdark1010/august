import styled from 'styled-components';

export const Nav = styled.nav`
  display: flex;
  height: 100%;
  align-items: center;
  .portal-modal {
    padding: 2rem;
  }
`;

export const Link = styled.a`
  height: 100%;
  display: flex;
  align-items: center;
  margin-right: 3rem;
  cursor: pointer;
`;

export const Label = styled.p<{ show?: boolean }>`
  color: white;
  display: inline-block;
  position: relative;
`;

export const Badge = styled.span`
  padding: 2px 0.4rem;
  text-transform: capitalize;
  margin-left: 0.5rem;
  border-radius: 2px;
  color: ${(p) => p.theme.palette.common.white};
  background-color: ${(p) => p.theme.palette.primary.main};
`;
