import { ReactNode } from 'react';

export type CardType = {
  children: ReactNode;
  bgColor?: string;
  borderRadiusTop?: string;
  borderRadiusBottom?: string;
  borderRadiusRight?: string;
  borderRadiusLeft?: string;
  padding?: number;
  width?: string;
  height?: string;
  direction?: 'row' | 'column';
};
