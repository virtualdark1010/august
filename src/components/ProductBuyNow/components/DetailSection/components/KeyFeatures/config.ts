export default [
  'Equipped with a high-performance CPU/GPU and significantly improved video processing capacity. The output has been expanded to 20xHD, 5x4K outputs',
  'Equipped with 4K support, I/O expansion and CANVAS functions as a standard',
  'The recording capacity of still images and videos (RAM players, clip player) has been significantly increased',
];
