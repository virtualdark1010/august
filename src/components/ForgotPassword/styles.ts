import styled from 'styled-components';

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  width: 25%;
  margin-bottom: 2rem;
  span:nth-child(1) {
    font-size: 4rem;
  }
  span:nth-child(2) {
    font-size: 1rem;
  }
  @media only screen and (max-width: 768px) {
    width: 100%;
    justify-content: center;
    align-items: center;
    span:nth-child(1) {
      font-size: 2rem;
    }
    span:nth-child(2) {
      font-size: 0.7rem;
      width: 70%;
    }
  }
`;

export const LogoContainer = styled.div`
  margin-bottom: 5rem;
`;

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 2rem;
`;

export const FormWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  background-color: ${(p) => p.theme.palette.common.black};
  flex-direction: column;
`;

export const FormGroup = styled.div`
  position: relative;
  margin: 1rem 0;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Input = styled.input`
  font-size: 18px;
  padding: 10px 10px 10px 5px;
  display: block;
  width: 60%;
  border: none;
  border-bottom: 1px solid ${(p) => p.theme.palette.common.white};
  caret-color: ${(p) => p.theme.palette.common.white};
  background-color: ${(p) => p.theme.palette.common.transparent};
  &:focus {
    outline: none;
  }
`;

export const Label = styled.label`
  color: #fff;
  font-size: 18px;
  font-weight: normal;
  position: absolute;
  pointer-events: none;
  left: 45%;
  top: 10px;
  transition: 0.2s ease all;
  ${Input}:focus ~ & {
    top: -18px;
    font-size: 14px;
    color: #fff;
  }
`;
