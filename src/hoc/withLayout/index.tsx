/* eslint-disable react/display-name */
import Text from 'components/shared/Text';
import { isMobile } from 'react-device-detect';

import { LayoutType } from 'types/layout';

import { Wrapper, HeadingWrapper } from './styles';

export const withLayout =
  <T extends object>(
    WrapperComponent: React.ComponentType<T>,
    layoutProps?: LayoutType,
  ): React.ComponentType<T> =>
  (componentProps: T) => {
    const { heading } = layoutProps || {};
    return (
      <Wrapper {...layoutProps} className='layout'>
        <HeadingWrapper>
          {heading?.text && (
            <Text
              color='#ffffff'
              fontSize={isMobile ? '2rem' : '4rem'}
              weight='700'
              {...heading}
              fontFamily='ClashDisplayBold'
            />
          )}
        </HeadingWrapper>
        <WrapperComponent {...(componentProps as T)} />
      </Wrapper>
    );
  };
