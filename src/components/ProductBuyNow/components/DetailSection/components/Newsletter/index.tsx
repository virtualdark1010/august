import { ChevronRight } from 'components/shared/icons';
import Text from 'components/shared/Text';
import React from 'react';
import { Main, Flex, Icon } from './styles';

export const Newsletter = () => {
  return (
    <Main>
      <Text text='News that matter to you' color='#FD1515' fontSize='2rem' weight='700' />
      <Text
        text='Get up to speed on our most important updates, find out where you can meet us at the next in-person events, and learn more about our solutions in action in broadcast, corporate, sports and entertainment productions around the globe.'
        fontSize='1rem'
        color='black'
      />
      <Flex>
        <Text text='Sign up' fontSize='1rem' weight='700' color='#FD1515' />
        <Icon className='signup-icon'>
          <ChevronRight />
        </Icon>
      </Flex>
    </Main>
  );
};
