import Text from '../Text';
import { StrikedThroughText } from './styles';
import { StrikethroughTextType } from 'types/strikethroughcontent';

const StrikedThroughContent = ({ content, contentColor }: StrikethroughTextType) => {
  return (
    <StrikedThroughText>
      <Text text={content} color={contentColor} />
    </StrikedThroughText>
  );
};

export default StrikedThroughContent;
