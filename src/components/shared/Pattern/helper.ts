import { CustomPosition, PatternPosition } from 'types/layout';

export const getPatternPosition = (position?: PatternPosition, customPosition?: CustomPosition) => {
  const { top, bottom, left, right } = customPosition || {};
  if (!position && !customPosition) {
    return '';
  }

  switch (position) {
    case 'topLeft':
      return `
          top: 1rem;
          left: 1rem;
      `;
    case 'topRight':
      return `
          top: 1rem;
         right: 1rem
      `;
    case 'bottomLeft':
      return `
          bottom: 1rem;
          left: 1rem
      `;
    case 'bottomRight':
      return `
          bottom: 1rem;
         right: 1rem
      `;
  }
  if (customPosition)
    return `
        top: ${top};
        left: ${left};
        right: ${right};
        bottom: ${bottom};
  `;
};
