import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  width: 50%;
  margin-top: 3rem;
  ${mobileMediaQuery('width: 100%;')}
  svg {
    margin-top: 6px;
  }
  button {
    margin-top: 5rem;
  }
`;
