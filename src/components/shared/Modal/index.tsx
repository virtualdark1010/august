import React from 'react';
import { createPortal } from 'react-dom';

import { ModalProps } from 'global/types';

import { Wrapper } from './styles';

const Modal: React.FC<ModalProps> = ({ isOpen, children, ...rest }) => {
  if (!isOpen) return null;

  const element = (
    <Wrapper
      className='portal-modal'
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.3, type: 'spring', damping: 25, stiffness: 500 }}
      {...rest}
    >
      {children}
    </Wrapper>
  );

  return createPortal(element, document.body);
};

export default Modal;
