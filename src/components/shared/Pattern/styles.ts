import styled from 'styled-components';
import { PatternType } from 'types/layout';
import { getPatternPosition } from 'components/shared/Pattern/helper';

export const PatternWrapper = styled.div<{ pattern?: string }>``;

export const Container = styled.div<Partial<PatternType>>`
  position: absolute;
  padding: 1rem;
  display: flex;
  flex-direction: column;
  span {
    margin-right: 1rem;
    color: ${(p) => p.theme.palette.common.white};
  }
  ${({ position, customPosition }) => getPatternPosition(position, customPosition)};
`;

export const Row = styled.div`
  display: flex;
`;
