import { PatchPosition } from './types';

export const getPosition = (position: PatchPosition) => {
  switch (position) {
    case 'top':
      return `
            top: -50%;
        `;
    case 'bottom':
      return `
          left: 50%;
          transform: translate(-50%);
           bottom: -100%;
        `;
    case 'right':
      return `
            right: -50%;
        `;
    case 'left':
      return `
            left: -50%;
        `;
    case 'topLeft':
      return `
            top: -50%;
            left: -50%;
        `;
    case 'topRight':
      return `
            top: -50%;
           right: -50%
        `;
    case 'bottomLeft':
      return `
            bottom: -50%;
            left: -50%
        `;
    case 'bottomRight':
      return `
            bottom: -50%;
           right: -50%
        `;
    case 'center':
    default:
      return `
          top:50%;
           left:50%;
           transform: translate(-50%,-50%);
        `;
  }
};
