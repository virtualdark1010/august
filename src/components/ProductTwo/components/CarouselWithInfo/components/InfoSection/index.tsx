import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';
import Text from 'shared/Text';
import { InfoSectionType } from 'types/infoSection';
import { Container, Image } from './styles';

const InfoSection = ({
  title,
  subTitle,
  description,
  floatingImage,
  infoSectionImage,
  bgColor,
}: InfoSectionType) => {
  const { ref, config } = useAnimate();
  return (
    <Container bgColor={bgColor} ref={ref}>
      {title && (
        <Text
          text={title}
          color='#BF0C0C'
          fontSize={isMobile ? '1rem' : '2rem'}
          weight='700'
          center={isMobile}
          {...config({ delay: 1 })}
        />
      )}
      {subTitle && (
        <Text
          text={subTitle}
          center={isMobile}
          fontSize={isMobile ? '2rem' : '5rem'}
          weight='bold'
          {...config({})}
        />
      )}
      {description && (
        <Text
          text={description}
          center={isMobile}
          fontSize={isMobile ? '0.875rem' : '1.75rem'}
          {...config({})}
        />
      )}
      <Image
        src={infoSectionImage}
        floatingImage={floatingImage}
        {...config({
          customConfig: {
            variants: {
              visible: {
                opacity: 1,
                transition: { duration: 2, delay: 0.5 },
              },
              hidden: {
                opacity: 0,
                transition: { duration: 2, delay: 0.5 },
              },
            },
          },
        })}
      />
    </Container>
  );
};

export default InfoSection;
