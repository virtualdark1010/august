import { Noise } from 'assets/icons';
import Avatar from 'components/shared/Avatar';
import Text from 'components/shared/Text';
import { isMobile } from 'react-device-detect';

import { Wrapper } from '../../styles';

const NoiseInfo = () => (
  <Wrapper>
    <Avatar bgColor='white' {...(isMobile && { width: '35px', height: '35px' })}>
      <Noise />
    </Avatar>
    <Text
      text='Noise Cancellation'
      color='#BB000E'
      fontSize={isMobile ? '1.5rem' : '3rem'}
      weight='bold'
    />
    <Text
      text='Lorem ipsum dolor sit amet consectetur adipiscing elit Ut et massa mi. Aliquam in hendrerit urna. Pellentesque sit amet sapien.'
      level={isMobile ? '1' : '3'}
    />
  </Wrapper>
);

export default NoiseInfo;
