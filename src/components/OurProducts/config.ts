import image1 from 'assets/images/product-1.png';
import image2 from 'assets/images/product-2.png';
import image3 from 'assets/images/product-1.png';
import image4 from 'assets/images/product-2.png';
import image5 from 'assets/images/product-1.png';

export default [
  {
    title: '360 Workspace',
    buttonText: 'Explore 360 →',
    image: image1,
    start: 1,
    end: 9,
    hasPlayButton: true,
    modalHeading: '360 workspace',
    modalText:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
    modalVideoUrl: 'https://www.youtube.com/embed/xcJtL7QggTI',
  },
  {
    title: '360 Workspace',
    buttonText: 'Explore 360 →',
    start: 9,
    end: 13,
    image: image2,
    hasPlayButton: true,
    modalHeading: 'Yet another heading',
    modalText:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
    modalVideoUrl: 'https://www.youtube.com/embed/UV0mhY2Dxr0',
  },
  {
    title: '360 Workspace',
    buttonText: 'Explore 360 →',
    start: 1,
    end: 5,
    image: image3,
  },
  {
    title: '360 Workspace',
    buttonText: 'Explore 360 →',
    start: 5,
    end: 13,
    image: image4,
  },
  {
    title: '360 Workspace',
    buttonText: 'Explore 360 →',
    image: image5,
  },
];
