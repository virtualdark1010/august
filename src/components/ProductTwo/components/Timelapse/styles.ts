import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  position: relative;
  .text-container:nth-child(2) {
    line-height: 6rem;
    margin-bottom: 1rem;
    width: 50%;
  }
  img {
    bottom: -30%;
  }
  ${mobileMediaQuery(`
      flex-direction: column;
      .text-container:nth-child(2) {
        margin-bottom: 0.5rem;
        line-height: 2rem;
        width: 100%;
  }
  `)}
`;

export const LeftSection = styled.div`
  width: 55%;
  padding: 3rem;
  display: flex;
  align-items: center;
  ${mobileMediaQuery(`width: 100%; padding: 6rem 1rem;`)}
`;

export const TextWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  span:nth-child(3) {
    width: 50%;
    ${mobileMediaQuery(`width: 100%;`)}
  }
`;

export const FloatingImage = styled(motion.div)`
  position: absolute;
  top: 30%;
  left: -50%;
  z-index: 3;
  ${mobileMediaQuery(`display:none;`)}
`;

export const ImageWrapper = styled.div`
  width: 45%;
  height: 100%;
  position: relative;
  ${mobileMediaQuery(`width: 100%;`)}
`;

export const Image = styled(motion.img)`
  object-fit: contain;
  width: 100%;
`;
