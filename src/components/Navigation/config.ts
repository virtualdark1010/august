import image from 'assets/images/product-1.png';
import image2 from 'assets/images/smart-1.png';

export default [
  {
    name: 'Product',
    link: '/product',
    badge: 'new',
  },
  {
    name: 'Explore',
    link: '/explore',
    badge: null,
  },
  {
    name: 'Downloads',
    link: '/downloads',
    badge: null,
  },
  {
    name: 'Support',
    link: '/suppot',
    badge: null,
  },
];

const product = [
  {
    title: 'Conference Cameras',
    contents: [
      {
        title: 'Workspace-360',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image,
      },
      {
        title: 'Workspace-pi desk',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image: image2,
      },
    ],
  },
  {
    title: 'Actions Cameras',
    contents: [
      {
        title: 'Workspace-360',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image,
      },
      {
        title: 'Workspace-pi desk',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image: image2,
      },
      {
        title: 'Workspace-pi desk',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image: image2,
      },
      {
        title: 'Workspace-pi desk',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image: image2,
      },
      {
        title: 'Workspace-pi desk',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image: image2,
      },
    ],
  },
];
const explore = [
  {
    title: 'Conference Cameras',
    contents: [
      {
        title: 'Workspace-360',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image,
      },
    ],
  },
];
const downloads = [
  {
    title: 'Conference Cameras',
    contents: [
      {
        title: 'Workspace-360',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image,
      },
      {
        title: 'Workspace-pi desk',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image: image2,
      },
      {
        title: 'Workspace-pi desk',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image: image2,
      },
    ],
  },
];
const support = [
  {
    title: 'Conference Cameras',
    contents: [
      {
        title: 'Workspace-360',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image,
      },
      {
        title: 'Workspace-pi desk',
        badge: 'New',
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to",
        button: 'EXPLORE',
        image: image2,
      },
    ],
  },
];

export const subMenuData = { product, explore, downloads, support };
