import styled from 'styled-components';
import image from 'assets/images/hdr-bg.png';
import { motion } from 'framer-motion';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding: 5rem 0;
  position: relative;
  overflow: hidden;
  background-image: url(${image});
  background-repeat: no-repeat;
  background-size: cover;
  ${mobileMediaQuery(`padding: 2rem 0;`)}
`;

export const TextWrapper = styled.div`
  width: 60%;
  display: flex;
  flex-direction: column;
  align-items: center;
  ${mobileMediaQuery(`width: 90%;`)}
`;

export const Image = styled(motion.img)`
  width: 50%;
  object-fit: cover;
  margin-top: 6rem;
  ${mobileMediaQuery(`width: 90%; margin-top: 2rem;`)}
`;
