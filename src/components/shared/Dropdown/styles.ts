import styled from 'styled-components';

export const Wrapper = styled.div`
  div[class*='-control'] {
    background-color: ${(p) => p.theme.palette.background.secondary};
    border: ${(p) => p.theme.palette.background.secondary};
  }
  div[class*='-placeholder'],
  div[class*='-singleValue'] {
    color: ${(p) => p.theme.palette.common.white};
  }
  div[class*='-indicatorContainer'] {
    background-color: ${(p) => p.theme.palette.background.secondary};
  }
`;
