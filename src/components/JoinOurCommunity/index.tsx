import Button from 'components/shared/Button';
import Text from 'components/shared/Text';
import { withLayout } from 'hoc/withLayout';
import { isMobile } from 'react-device-detect';

import { Container } from './styles';

const JoinOurCommunity = () => {
  return (
    <Container>
      <Text
        text='Join Our Community'
        color='#ffffff'
        fontSize={isMobile ? '2rem' : '3rem'}
        weight='700'
        fontFamily='ClashDisplayBold'
      />
      <Button onClick={() => {}} color='black' bgColor='#ffffff'>
        Get Started
      </Button>
    </Container>
  );
};

export default withLayout(JoinOurCommunity);
