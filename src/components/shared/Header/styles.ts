import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.header`
  display: grid;
  background-color: #000000;
  grid-template-rows: 1fr;
  grid-template-columns: repeat(12, 1fr);
  gap: 0px;
  height: 110px;
  padding: 0 4rem;
  align-items: center;
  z-index: 10;
  position: fixed;
  top: 0;
  img {
    height: 100%;
  }
  ${mobileMediaQuery(`
    padding: 0 2rem; 
    display: flex;
    justify-content: space-between;
    width: 100%;
  `)}
`;

export const LogoWrapper = styled.div`
  grid-row-start: 1;
  grid-column-start: 1;
  grid-row-end: 2;
  grid-column-end: 5;
  margin-left: 2rem;
  ${mobileMediaQuery(`
    margin-left: 0;
    svg {
      width: 100px;
    }
  `)}
`;

export const Nav = styled.div`
  grid-row-start: 1;
  grid-column-start: 5;
  grid-row-end: 2;
  grid-column-end: 10;
  height: 100%;
  display: flex;
  align-items: center;
`;

export const Right = styled.div`
  grid-row-start: 1;
  grid-row-end: 2;
  grid-column-start: 11;
  grid-column-end: 12;
  display: flex;
  align-items: center;
  svg {
    margin-right: 1rem;
  }
  button {
    text-transform: uppercase;
  }
`;

export const MenuWrapper = styled.div`
  svg {
    width: 30px;
  }
`;
