import styled from 'styled-components';
import { GridType } from 'types/grid';

export const Container = styled.div<GridType>`
  display: grid;
  align-items: center;
  justify-items: center;
  position: relative;
  ${(p) => {
    const { bgColor, padding, gridGap, row = 'auto', column = 12 } = p;
    return `
         grid-template-columns: repeat(${column}, 1fr);
         grid-template-rows: ${!row ? 'auto auto' : `repeat(${row}, 2vh)`};
         background: ${bgColor || ''};
         padding: ${padding || '4rem'};
         grid-gap: ${gridGap || '2rem'};

    `;
  }};
  @media screen and (max-width: 768px) {
    grid-template-columns: repeat(1, 1fr);
  }
`;

const getColumn = ({ start = 1, end = 13 }: Partial<GridType>) => {
  return `
    grid-column-start: ${start};
    grid-column-end: ${end};
  `;
};

const getRow = ({ rowStart, rowEnd }: Partial<GridType>) => {
  return `
    grid-row-start: ${rowStart || 'auto'};
    grid-row-end: ${rowEnd || 'auto'};
  `;
};

export const Item = styled.div<Partial<GridType>>`
  width: 100%;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  grid-template-rows: auto auto;
  ${(p) => {
    const { borderRadius, theme, bgColor, height } = p;
    return `
    height: ${height};
    border-radius: ${borderRadius || theme.borderRadius}};
    background: ${bgColor || theme.palette.background.secondary};
    ${getColumn(p)}
    ${getRow(p)}
    `;
  }}
`;
