import React from 'react';
import Text from 'shared/Text';

import { isMobile } from 'react-device-detect';

import mountAnywhere1 from 'assets/images/mount-anywhere-1.png';
import mountAnywhere2 from 'assets/images/mount-anywhere-2.png';
import { useAnimate } from 'hooks/useAnimate';
import { Main, TextWrapper, ImageOne, FloatingImage } from './styles';

const MountAnywhere = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <TextWrapper>
        <Text
          text='Mount Anywhere'
          fontSize={isMobile ? '1rem' : '2rem'}
          center={isMobile}
          {...config({ leftToRight: true })}
        />
        <Text
          text='The world is your playground'
          fontSize={isMobile ? '2rem' : '5rem'}
          center={isMobile}
          weight='bold'
          {...config({ delay: 0.5 })}
        />
        <Text
          text='GO 2 is so small, it fits anywhere. Like literally anywhere. Guitar? Easy. Cat? Well... as long as your cat actually listens to you. Just use the in-the-box accessories to mount up anywhere.'
          fontSize={isMobile ? '1rem' : '1.75rem'}
          center={isMobile}
          {...config({ leftToRight: true, delay: 0.7 })}
        />
      </TextWrapper>
      <ImageOne
        src={mountAnywhere1}
        {...config({ leftToRight: true, delay: 0.9, stiffness: 40 })}
      />
      <FloatingImage src={mountAnywhere2} {...config({ delay: 1.1, stiffness: 50, x: '-50%' })} />
    </Main>
  );
};

export { MountAnywhere };
