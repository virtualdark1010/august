import React from 'react';

import { Main, Image, ImageWrapper } from './styles';
import InfoSection from '../CarouselWithInfo/components/InfoSection';
import hyperlapseImageOne from 'assets/images/hyperlapse-image-1.png';
import hyperlapseImageTwo from 'assets/images/hyperlapse-image-2.png';
import { useAnimate } from 'hooks/useAnimate';

const Hyperlapse = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <InfoSection
        bgColor='#F89F11'
        infoSectionImage={hyperlapseImageOne}
        floatingImage
        title='Hyperlapse'
        subTitle='Wooaahhh that’s fast.'
        description='Jam pack all the action into a speedy hyperlapse with TimeShift mode. Up to 6x faster than real life.'
      />
      <ImageWrapper
        {...config({
          duration: 2,
          customConfig: {
            variants: {
              visible: {
                opacity: 1,
              },
              hidden: {
                opacity: 0,
              },
            },
          },
        })}
      >
        <Image src={hyperlapseImageTwo} />
      </ImageWrapper>
    </Main>
  );
};

export { Hyperlapse };
