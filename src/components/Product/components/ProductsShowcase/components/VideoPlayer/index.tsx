import React, { useRef } from 'react';
import ReactPlayer from 'react-player';
import { useVideoPlayOnScroll } from 'hooks/useVideoPlayOnScroll';

import { PlayerWrapper, Main } from './styles';
import { useOnScreen } from 'hooks/useOnScreen';

const VideoPlayer = ({ path, height }: { path?: string; height?: string }) => {
  const videoRef = useRef<any>();
  const videoPlayerRef = useRef<any>();

  const showPlayer = useOnScreen(videoRef);

  useVideoPlayOnScroll(videoRef, videoPlayerRef);

  return (
    <Main ref={videoRef} height={height}>
      {showPlayer && (
        <PlayerWrapper>
          <ReactPlayer ref={videoPlayerRef} url={path} muted />
        </PlayerWrapper>
      )}
    </Main>
  );
};

export default VideoPlayer;
