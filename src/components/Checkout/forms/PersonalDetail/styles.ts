import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 2rem;
  justify-content: center;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Form = styled.div`
  width: 50%;
  display: flex;
  flex-wrap: wrap;
  margin-top: 1rem;
`;

export const SaveDetail = styled.div`
  display: flex;
  align-items: center;
  margin: 1rem 0;
  cursor: pointer;
  svg {
    margin-right: 1rem;
  }
`;
