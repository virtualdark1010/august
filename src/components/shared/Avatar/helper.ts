import { ButtonType } from 'types/button';

const getButtonVariant = ({ variant = 'default', theme }: Partial<ButtonType> & { theme: any }) => {
  return `
      color: ${theme.palette.button.variant[variant].color};
      border: 1px solid ${theme.palette.button.variant[variant].borderColor};
  `;
};

const getButtonStyle = ({ shape = 'default', theme }: Partial<ButtonType> & { theme: any }) => {
  return `
      border-radius: ${theme.palette.button.shape[shape].borderRadius};
  `;
};

export { getButtonStyle, getButtonVariant };
