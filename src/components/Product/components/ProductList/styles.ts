import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 5rem;
  ${mobileMediaQuery(`padding: 2rem;`)}
`;
