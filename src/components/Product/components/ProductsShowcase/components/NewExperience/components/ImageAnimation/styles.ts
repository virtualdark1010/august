import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled(motion.div)`
  display: flex;
  margin: 10rem 0;
  position: relative;
  ${mobileMediaQuery(`justify-content: center;`)}
`;

export const Image = styled(motion.img)<{
  width?: string;
  height?: string;
  borderRadius?: string;
  x?: string;
  z?: number;
}>`
  object-fit: cover;
  width: ${({ width = '30vw' }) => width};
  height: ${({ height }) => height};
  z-index: ${({ z = 2 }) => z};
  transform: ${(p) => `translateX(${p?.x})`};
`;
