import { motion } from 'framer-motion';
import styled from 'styled-components';

export const Container = styled.div`
  margin: 3rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  button {
    color: ${(p) => p.theme.palette.primary.red};
  }
  > span:nth-child(2) {
    width: 25%;
    margin: 2rem 0;
    text-align: center;
  }
  @media screen and (max-width: 768px) {
    margin: 0;
  }
`;

export const MobileCardContainer = styled.div`
  div {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  span {
    text-align: center;
  }
`;

export const Wrapper = styled.div`
  width: 80%;
  margin-top: 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const CardContainer = styled(motion.div)`
  display: flex;
  gap: 21px;
  flex-wrap: wrap;
  > :nth-child(1n),
  > :nth-child(2n),
  > :nth-child(3n) {
    flex: 1 0 calc(33.33% - 20px);
  }
`;

export const CardContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 13px;
`;
