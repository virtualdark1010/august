import { ProfileAvatar } from 'shared/icons';

export const createRavingCards = (length: number = 5, Comp: any) =>
  Array.from({ length }).map((_) => {
    const props = {
      name: 'john doe',
      userName: '@johncappa',
      content:
        'Heres an idea  do something good for someone and dont post about it on social ,media.',
      timeStamp: '12:30 PM · Apr 21, 2021',
      infoSource: 'Twitter web',
      image: <ProfileAvatar />,
    };
    return <Comp {...props} />;
  });

export const variants = {
  animateUp: {
    y: [0, -400],
    transition: {
      y: {
        repeat: Infinity,
        repeatType: 'loop',
        duration: 15,
        ease: 'linear',
      },
    },
  },
  animateDown: {
    y: [-400, 0],
    transition: {
      y: {
        repeat: Infinity,
        repeatType: 'loop',
        duration: 15,
        ease: 'linear',
      },
    },
  },
};
