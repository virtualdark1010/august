import InfoSection from 'components/ProductTwo/components/CarouselWithInfo/components/InfoSection';
import { Carousel } from 'components/ProductTwo/components';
import { CarouselWithInfoType } from 'types/carouselWithInfo';
import { InfoSectionType } from 'types/infoSection';

import { Container } from './styles';

const CarouselWithInfo = ({
  addBottomMargin,
  reverse,
  carouselImage,
  ...rest
}: CarouselWithInfoType & InfoSectionType) => (
  <Container addBottomMargin={addBottomMargin} reverse={reverse}>
    <InfoSection {...rest} />
    <Carousel items={carouselImage} />
  </Container>
);

export { CarouselWithInfo };
