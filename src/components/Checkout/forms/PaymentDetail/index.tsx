import React, { useState } from 'react';
import Button from 'shared/Button';
import Text from 'shared/Text';

import { fields } from './fields';

import { Container, Form, Field, SaveDetail } from 'components/Checkout/styles';

import Toggle from 'shared/Toggle';

const PaymentDetail = () => {
  const [, setChecked] = useState<boolean>(false);

  return (
    <Container>
      <Form>
        {fields.map((field: any) => {
          const { Render, props, width } = field;
          return (
            <Field width={width}>
              <Render {...props} />
            </Field>
          );
        })}
        <SaveDetail>
          <Text text='Save card data for future payments' color='white' level='3' />
          <Toggle onClick={(v) => setChecked(v)} />
        </SaveDetail>
        <Button onClick={() => {}} variant='inverted' width='100%'>
          Pay Now
        </Button>
      </Form>
    </Container>
  );
};

export default PaymentDetail;
