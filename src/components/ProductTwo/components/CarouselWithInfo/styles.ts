import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.section<{ addBottomMargin?: boolean; reverse?: boolean }>`
  display: flex;
  max-height: 100vh;
  flex-direction: ${({ reverse = false }) => (reverse ? 'row-reverse' : 'row')};
  ${({ addBottomMargin = false }) => addBottomMargin && 'margin-bottom: 30vh'};
  .carousel-wrapper {
    width: 45%;
  }
  .text-container:nth-child(2) {
    line-height: 5rem;
    margin-bottom: 3rem;
    ${mobileMediaQuery(`line-height: 3rem; margin-bottom: 1rem;`)}
  }
  .carousel-image {
    height: 100vh;
  }
  ${mobileMediaQuery(`
  flex-direction:column;  
  max-height: 100%;
  margin-bottom: 0 ;
  .carousel-wrapper {
    width: 100%;
  }
  .slick-list{
   height: 54vh;
  }
  .carousel-image{
    height: 100%;
  }
  .slick-dots{
    bottom: 20px;
  }
  .slick-dots .slick-active .ft-slick__dots--custom{
   width: 26px;
  }
  .slick-dots .slick-active{
    width: 26px;
  }
  .slick-dots li{
   margin: 0 0.5rem;
  }
  `)}
`;
