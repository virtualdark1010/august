export type TextType = {
  text: string;
  color?: string;
  fontSize?: string;
  weight?: string;
  className?: string;
  active?: boolean;
  center?: boolean;
  onClick?: (v: any) => void;
  level?: '1' | '2' | '3' | '4' | '5' | '6';
  fontFamily?:
    | 'ClashDisplayRegular'
    | 'ClashDisplayBold'
    | 'ClashDisplaySemiBold'
    | 'ClashDisplayLight'
    | 'InterRegular'
    | 'InterRegularExtraBold'
    | 'InterExtraLight'
    | 'InterRegularLight'
    | 'InterRegularMedium'
    | 'InterRegularThin';
  margin?: string;
};
