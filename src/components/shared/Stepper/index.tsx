import React from 'react';
import { isMobile } from 'react-device-detect';
import { Container, Step, Line } from './styles';

const Stepper = ({
  steps,
  activeStep,
  onClick,
}: {
  steps: string[];
  activeStep: string;
  onClick: (v: any) => void;
}) => {
  const renderSteps = steps.map((step, index) => {
    const isLastStep = index !== steps.length - 1;
    const isActive = step === activeStep;
    return (
      <>
        <Step
          active={isActive}
          color={!isActive ? 'white' : ''}
          text={step}
          level={isMobile ? '2' : '5'}
          onClick={() => onClick(step)}
        />
        {isLastStep && <Line />}
      </>
    );
  });

  return <Container>{renderSteps}</Container>;
};

export default Stepper;
