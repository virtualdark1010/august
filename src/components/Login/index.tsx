import Text from 'shared/Text';
import Button from 'shared/Button';
import Input from 'shared/Input';
import LinkText from 'shared/LinkText';
import CarouselProduct from 'shared/CarouselProduct';

import { carouselConfig } from 'components/Signup/parts/carouselConfig';
import { Logo } from 'shared/icons';

import {
  Container,
  LeftSide,
  RightSide,
  HeadingContainer,
  FormWrapper,
  FormGroup,
  ButtonContainer,
  FormContainer,
  LogoContainer,
} from './style';

const Login = () => {
  return (
    <Container>
      <LeftSide>{CarouselProduct(carouselConfig)}</LeftSide>
      <RightSide>
        <LogoContainer>
          <Logo />
        </LogoContainer>
        <FormContainer>
          <HeadingContainer>
            <Text text='Welcome to august cam' fontFamily='ClashDisplayBold' color='#fff' />
            <Text text='Please enter details below' fontFamily='InterRegular' color='#fff' />
          </HeadingContainer>
          <FormWrapper>
            <FormGroup>
              <Input type='text' name='email' label='Email' />
            </FormGroup>
            <FormGroup>
              <Input type='text' name='password' label='Password' />
            </FormGroup>
            <ButtonContainer>
              <Button
                onClick={() => console.log('submit')}
                bgColor='#fd1515'
                color='#fff'
                width='max-content'
              >
                Login
              </Button>
            </ButtonContainer>
          </FormWrapper>
          <LinkText text='Not a member yet? ' name='Signup' url='#' />
        </FormContainer>
      </RightSide>
    </Container>
  );
};

export default Login;
