import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  display: flex;
  position: relative;
`;

export const TabRow = styled.div`
  display: flex;
  button {
    ${mobileMediaQuery('padding: 0.5rem 0.5rem; font-size: 0.875rem;')}
  }
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Content = styled.div`
  position: absolute;
  margin-top: 8rem;
  ${mobileMediaQuery('margin-top: 5rem;')}
  h1 {
    color: white;
  }
`;
