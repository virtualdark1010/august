import { useAnimation } from 'framer-motion';
import React, { useEffect } from 'react';
import GlowPatch from 'shared/GlowPatch';
import { Main, Image } from './styles';
import { useInView } from 'react-intersection-observer';
import { isMobile } from 'react-device-detect';

const ImageAnimation = ({ imageOne, imageTwo }: { imageOne: any; imageTwo: any }) => {
  const animation = useAnimation();
  const { ref, inView } = useInView({ threshold: 0.2 });

  useEffect(() => {
    if (inView) {
      animation.start('visible');
    } else {
      animation.start('hidden');
    }
  }, [animation, inView]);

  return (
    <Main ref={ref}>
      <Image
        animate={animation}
        initial='hidden'
        transition={{ duration: 1 }}
        variants={{
          visible: {
            opacity: 1,
            x: imageOne?.x || 0,
            transition: { type: 'spring', stiffness: 55, delay: 0.3 },
          },
          hidden: {
            opacity: 0,
            x: '-200vw',
            transition: { type: 'spring', stiffness: 55, delay: 0.3 },
          },
        }}
        width={isMobile ? '40%' : '80%'}
        {...imageOne}
      />
      <Image
        animate={animation}
        initial='hidden'
        transition={{ duration: 1 }}
        variants={{
          visible: {
            opacity: 1,
            x: imageTwo?.x || 0,
            transition: { type: 'spring', stiffness: 55, delay: 0.3 },
          },
          hidden: {
            opacity: 0,
            x: '200vw',
            transition: { type: 'spring', stiffness: 55, delay: 0.3 },
          },
        }}
        width={isMobile ? '40%' : '80%'}
        {...imageTwo}
      />
      <GlowPatch position='center' />
    </Main>
  );
};

export default ImageAnimation;
