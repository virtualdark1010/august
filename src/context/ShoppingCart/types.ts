import { ReactNode } from 'react';
import { ShoppingCartActionsMap } from 'context/ShoppingCart';

export type ShoppingCartActionType = {
  type: typeof ShoppingCartActionsMap[keyof typeof ShoppingCartActionsMap];
  payload?: any;
};

export type ShoppingCartProviderType = { children: ReactNode };

export type ShoppingCartSteps = 'Address' | 'Shipping' | 'Payment';

export type ShoppingCart = {
  price: string;
  id: number;
  name: string;
  quantity?: number;
};

export type ShoppingCartInitialState = {
  numberOfItems: number;
  cartTotal: number;
  step: ShoppingCartSteps;
  cart: ShoppingCart[];
};

export type ShoppingCartContextType = ShoppingCartInitialState & {
  setStep: (step: ShoppingCartSteps) => void;
  addToCart: (payload: Partial<ShoppingCart>) => void;
  removeFromCart: (id: number) => void;
};
