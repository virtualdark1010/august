import styled from 'styled-components';
import { getFieldWidth } from 'components/Checkout/utils';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.div`
  padding: 4rem 10rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  ${mobileMediaQuery('padding: 4rem 1rem;')}
`;

export const Container = styled.div`
  margin-top: 2rem;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  button {
    margin-top: 4rem;
    ${mobileMediaQuery('margin: 0;')}
  }
`;

export const Form = styled.div`
  width: 50%;
  display: flex;
  flex-wrap: wrap;
  margin-top: 1rem;
  ${mobileMediaQuery('width: 100%;')}
`;

export const SaveDetail = styled.div`
  display: flex;
  align-items: center;
  margin: 1rem 0;
  justify-content: space-between;
  width: 100%;
  cursor: pointer;
  svg {
    margin-right: 1rem;
  }
`;

export const Field = styled.div<{ width: 'half' | 'full' | 'quarter' }>`
  width: ${({ width }) => getFieldWidth(width)};
  ${mobileMediaQuery('width:90%;')}
  margin: 0.5rem 0.5rem 1rem 0;
  space[class*='-indicatorSeparator'] {
    display: none;
  }
  div[class*='-control'] {
    height: 50px;
    background-color: ${(p) => p.theme.palette.common.transparent};
    border: 1px solid ${(p) => p.theme.palette.common.white};
  }
  div[class*='-placeholder'],
  div[class*='-singleValue'] {
    color: ${(p) => p.theme.palette.common.whtie};
  }
  div[class*='-indicatorContainer'] {
    background-color: ${(p) => p.theme.palette.common.transparent};
  }
`;
