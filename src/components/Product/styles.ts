import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const BestSellerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  .grid-container {
    ${mobileMediaQuery('padding:2rem; grid-template-rows: repeat(4,1fr);')}
  }
  span:nth-child(1) {
    align-self: center;
  }
`;
