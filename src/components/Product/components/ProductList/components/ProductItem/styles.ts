import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 2rem 2rem 0;
  ${mobileMediaQuery('margin: 0 1rem 1rem 0;')}
  span:nth-child(1) {
    margin: 0.5rem 0;
  }
`;

export const Image = styled.img`
  object-fit: cover;
  width: 30vh;
  ${mobileMediaQuery(' width: 18vh;')}
`;
