import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  background: #020202;
  margin-top: 110px;
  padding: 3rem 0 6rem 0;
  ${mobileMediaQuery(`
    flex-direction: column;
  `)}
`;

export const TextWrapper = styled(motion.div)`
  transform: rotate(270deg);
  width: 100%;
  height: 100%;
  display: flex;
  align-self: center;
  justify-content: center;
  span {
    font-weight: 900;
  }
  ${mobileMediaQuery(`
  transform: rotate(0);
  span {
   font-size: 1.5rem;
   margin-bottom: 2rem;
  }
`)}
`;

export const Image = styled.img`
  position: absolute;
  left: -75px;
  bottom: -65px;
  ${mobileMediaQuery(`
    display:none;
  `)}
`;

export const CarouselWrapper = styled(motion.div)`
  position: relative;
  width: 60%;
  ${mobileMediaQuery(`
    width: 90%;
    margin: auto;
    .slick-dots .slick-active .ft-slick__dots--custom{
      width: 26px;
    }
    .slick-dots .slick-active{
      width: 26px;
    }
    .slick-dots li{
      margin: 0 0.5rem;
    }
  `)}
  .slick-list,
  .slick-slider {
    height: 100%;
  }
  .slick-list {
    border-radius: 30px;
  }
  .slick-dots {
    bottom: -50px;
  }
`;
