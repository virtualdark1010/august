import React from 'react';
import GlowPatch from 'shared/GlowPatch';
import ProductDetailSection from '../ProductDetailSection';
import VideoPlayer from '../VideoPlayer';
import CameraPreview from './components/CameraPreview';

const Lenses = () => {
  return (
    <>
      <VideoPlayer path='src/assets/video/talktsy.mp4' />
      <ProductDetailSection
        title='4K Boost Lens'
        description='Zero in on the action with the 4K Boost Lens. Rugged and durable, with a new 1/2" 48MP image sensor for more detailed 4K 60fps videos and 48MP photos.'
        imageUrl='src/assets/images/product.png'
        floatingImageUrl='src/assets/images/action-camera.png'
        patch={<GlowPatch position='right' />}
        fullMargin
      />
      <ProductDetailSection
        title='48MP Photos'
        description='4x more megapixels. Capture the extra detail those
        picture-perfect moments deserve, with better colors and lighting.'
        imageUrl='src/assets/images/product.png'
      />
      <ProductDetailSection
        title='6K Widescreen Mode'
        description='Shoot ultra-detailed 6K footage that would look right at home in a Hollywood movie, with a classic 2.35:1 ratio for widescreen viewing.'
        imageUrl='src/assets/images/product.png'
      />
      <CameraPreview imageUrl='src/assets/images/action-camera-2.png' />
      <ProductDetailSection
        title='5.7K 360 Lens'
        description='Shoot first, point later. Capture the action from all angles, then choose what to focus on after with 360 reframing. Use it with the Invisible Selfie Stick for impossible third-person view shots.'
        imageUrl='src/assets/images/product.png'
        floatingImageUrl='src/assets/images/action-camera-2.png'
        patch={[<GlowPatch position='left' />, <GlowPatch position='right' />]}
      />
      <CameraPreview imageUrl='src/assets/images/action-camera-3.png' leftToRight />
      <ProductDetailSection
        title='5.3K 1-Inch Lens'
        description='Co-engineered with Leica, the 1-Inch Wide Angle Lens has the largest image sensor of any action camera. Shoot 5.3K wide-angle footage with precise details and lifelike colors—even in low light.'
        imageUrl='src/assets/images/product.png'
        floatingImageUrl='src/assets/images/action-camera-3.png'
        patch={[<GlowPatch position='left' />, <GlowPatch position='right' />]}
      />
    </>
  );
};

export default Lenses;
