import styled from 'styled-components';

export const Container = styled.div`
  grid-row-start: 1;
  grid-row-end: 3;
  grid-column-start: 6;
  grid-column-end: 11;
  padding: 0.5rem 1rem;
  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;

export const LinksWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  word-break: break-all;
`;

export const Label = styled.div`
  font-weight: ${(p) => p.theme.palette.font.weight.bold};
  color: ${(p) => p.theme.palette.common.white};
  @media screen and (max-width: 768px) {
    font-size: 0.7rem;
  }
`;

export const Link = styled.span`
  margin: 0.5rem 0;
  width: max-content;
  cursor: pointer;
  color: ${(p) => p.theme.palette.common.white};
  @media screen and (max-width: 768px) {
    font-size: 0.7rem;
  }
`;
