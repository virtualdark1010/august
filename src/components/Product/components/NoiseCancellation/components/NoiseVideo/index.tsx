import Text from 'components/shared/Text';
import Toggle from 'components/shared/Toggle';
import ReactPlayer from 'react-player';

import { Wrapper, VideoWrapper } from '../../styles';

const NoiseVideo = () => {
  const handleProgress = () => {
    // console.log('🚀 ~ file: index.tsx:9 ~ handleProgress ~ state', state);
  };
  const handleDuration = () => {
    // console.log('🚀 ~ file: index.tsx:14 ~ handleDuration ~ duration', duration);
  };
  return (
    <Wrapper center>
      <VideoWrapper>
        <ReactPlayer
          url='https://www.youtube.com/watch?v=ysz5S6PUM-U'
          playing
          loop
          muted
          onProgress={handleProgress}
          onDuration={handleDuration}
          onSeek={(e) => {
            console.log('🚀 ~ file: index.tsx:31 ~ NoiseVideo ~ e', e);
          }}
        />
      </VideoWrapper>
      <Toggle />
      <Text text='Noise' color='#BB000E' level='4' />
    </Wrapper>
  );
};

export default NoiseVideo;
