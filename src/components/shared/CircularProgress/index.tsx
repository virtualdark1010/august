import { useEffect, useState } from 'react';
import Text from 'shared/Text';
import { Wrapper, IconWrapper } from './styles';

const CircularProgress = ({ className, size, strokeWidth, percentage, color, icon, text }: any) => {
  const [progress, setProgress] = useState(0);
  useEffect(() => {
    setProgress(percentage ?? 0);
  }, [percentage]);

  const viewBox = `0 0 ${size} ${size}`;
  const radius = (size - strokeWidth) / 2;
  const circumference = radius * Math.PI * 2;
  const dash = (progress * circumference) / 100;

  return (
    <Wrapper>
      <svg width={size} height={size} viewBox={viewBox} className={className}>
        <circle
          fill='none'
          stroke='#ccc'
          cx={size / 2}
          cy={size / 2}
          r={radius}
          strokeWidth={`${strokeWidth}px`}
        />
        <circle
          fill='none'
          stroke={color}
          cx={size / 2}
          cy={size / 2}
          r={radius}
          strokeWidth={`${strokeWidth}px`}
          transform={`rotate(-90 ${size / 2} ${size / 2})`}
          strokeDasharray={`${dash}, ${circumference - dash}`}
          strokeLinecap='round'
          style={{ transition: 'all 0.5s' }}
        />
      </svg>
      <IconWrapper className='progress-icon'>
        {icon || <Text text={text} color='#1E1E1E' fontSize='1.5rem' center />}
      </IconWrapper>
    </Wrapper>
  );
};

export default CircularProgress;
