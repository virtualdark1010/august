import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled(motion.div)<{ scrolled?: boolean }>`
  background-color: ${(p) => p.theme.palette.common.white};
  width: 45%;
  margin-top: 3.5rem;
  border-top-left-radius: 50px;
  overflow-y: scroll;
  ${({ scrolled }) =>
    mobileMediaQuery(`
     width: 100%;
     border-top-left-radius: 40px; 
     border-top-right-radius: 40px;
     margin-top: 0;
     background: white;
     position: fixed;
     top: 60vh;
     bottom: -40vh;
     position: fixed;
     overflow-y: scroll;
     overflow-x: hidden;
     transform: translateY(${scrolled ? '-50vh' : '-10vh'});
     transition: all 250ms ${scrolled ? 'ease-in' : 'ease-out'};
     ::-webkit-scrollbar {
        display:none;
      }
    `)}
  ::-webkit-scrollbar {
    width: 10px;
    height: 50%;
  }

  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: ${(p) => p.theme.palette.primary.main};
    border-radius: 10px;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: ${(p) => p.theme.palette.primary.main};
  }
`;

export const ProductDetail = styled.div`
  display: flex;
  flex-direction: column;
  padding: 4rem;
  ${mobileMediaQuery(`padding: 1rem;`)}
`;

export const SectionDivider = styled.div`
  height: 2px;
  width: 100%;
  margin: 2rem 0 2rem 1.5rem;
  background: ${(p) => p.theme.palette.background.secondary};
  opacity: 0.2;
`;

export const Video = styled.section`
  padding: 5rem 3rem 3rem 6rem;
  width: 80%;
  background-color: blue;
  height: 400px;
`;

export const Line = styled.div`
  width: 30%;
  height: 8px;
  border-radius: 10px;
  background: #252424;
  margin: 1rem auto;
  opacity: 0.5;
`;
