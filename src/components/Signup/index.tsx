import Text from 'shared/Text';
import Button from 'shared/Button';
import StrikedThroughContent from 'shared/StrikethroughText';
import Input from 'shared/Input';
import { Facebook, Twitter } from 'shared/icons';
import { Logo } from 'shared/icons';
import {
  Container,
  LeftSide,
  RightSide,
  HeadingContainer,
  FormGroup,
  FormWrapper,
  ButtonContainer,
  SocialLinkContainer,
  LogoContainer,
  Main,
  TextContainer,
} from './styles';
import CarouselProduct from 'shared/CarouselProduct';
import { carouselConfig } from './parts/carouselConfig';

const Signup = () => {
  return (
    <Container>
      <LeftSide>{CarouselProduct(carouselConfig)}</LeftSide>
      <RightSide>
        <LogoContainer>
          <Logo />
        </LogoContainer>
        <Main>
          <HeadingContainer>
            <TextContainer>
              <Text text='Sign in to August Cams' fontFamily='ClashDisplayBold' color='#fff' />
            </TextContainer>
            <SocialLinkContainer>
              <Facebook />
              <Twitter />
            </SocialLinkContainer>
            <StrikedThroughContent content='or do it by folling form below' contentColor='#fff' />
          </HeadingContainer>
          <FormWrapper>
            <FormGroup>
              <Input type='text' name='firstName' label='First Name' />
              <Input type='text' name='lastName' label='Last Name' />
            </FormGroup>
            <FormGroup>
              <Input type='email' name='email' label='Email' />
              <Input type='text' name='phone' label='Phone Number' />
            </FormGroup>
            <ButtonContainer>
              <Button
                onClick={() => console.log('submit')}
                bgColor='#fd1515'
                color='#fff'
                width='max-content'
              >
                Signup
              </Button>
            </ButtonContainer>
          </FormWrapper>
        </Main>
      </RightSide>
    </Container>
  );
};

export default Signup;
