import { LinkTextType } from 'types/linktext';
import { LinkContainer } from './style';

const LinkText = ({ text, url, name }: LinkTextType) => {
  return (
    <LinkContainer>
      {text} <a href={url}>{name}</a>
    </LinkContainer>
  );
};

export default LinkText;
