import styled from 'styled-components';

export const Container = styled.div`
  background-color: #000;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  margin-bottom: 4rem;
  button {
    color: ${(p) => p.theme.palette.primary.red};
  }
  > span:nth-child(2) {
    width: 25%;
    margin: 2rem 0;
    text-align: center;
  }
`;

export const Steps = styled.div`
  ul {
    margin: 0;
    padding: 0;
    width: 100%;
  }
  li {
    display: flex;
    flex-direction: row;
    gap: 15px;
    margin: 15px;
    align-items: center;
    font-family: InterRegularLight;
    font-size: 13px;
    justify-content: space-between;
    flex-direction: row-reverse;
  }
`;

export const CollapseTriggerContainer = styled.div`
  font-size: 12px;
  margin-right: 15px;
  border-bottom: 1px solid #fff;
  padding-bottom: 3px;
`;

export const OrderState = styled.div`
  font-family: 'InterRegularExtraBold';
  span {
    font-size: 11px;
    font-family: 'InterRegular';
    margin-left: 15px;
    font-style: italic;
  }
`;

export const LottieContainer = styled.div`
  margin-right: 15px;
`;

export const OrderHeaderState = styled.div`
  display: flex;
  align-items: center;
`;

export const OrderHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
