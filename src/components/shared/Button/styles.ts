import { motion } from 'framer-motion';
import styled from 'styled-components';
import { ButtonType } from 'types/button';
import { getButtonStyle, getButtonVariant } from './helper';

export const Container = styled(motion.button)<ButtonType>`
  padding: 0.5rem 2rem;
  transition: 250ms all;
  font-family: 'InterRegularExtraBold';
  transition: 0.5s;
  ${(p) => {
    const { theme, width, height, color, bgColor, borderColor, borderRadius, variant, shape } = p;

    return `
    width: ${width || 'max-content'};
    height: ${height || 'max-content'};
    color: ${color || theme.palette.primary.main};
    background: ${bgColor || theme.palette.background.transparent};
    border: 1px solid ${borderColor || theme.palette.primary.main};
    border-radius: ${borderRadius || theme.borderRadius};
    ${getButtonVariant({ variant, theme, borderColor })};
    ${getButtonStyle({ shape, theme })};
         `;
  }};
  &:hover {
    box-shadow: inset 0em 0em 0em 10em rgba(0, 0, 0, 0.1);
    transition: 0.5s;
  }
  /* &:hover {
    transition: 250ms all;
    transform: scale(1.05);
  } */
  :disabled {
    cursor: not-allowed;
    filter: grayscale(1);
  }
`;
