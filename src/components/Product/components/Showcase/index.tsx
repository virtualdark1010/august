import { BorderPattern } from 'assets/icons';
import { withLayout } from 'hoc/withLayout';
import { Main, Container, Slider, Scroller, Slide } from './styles';

import imageUrl from 'assets/images/camera.png';
import Text from 'components/shared/Text';

const Showcase = () => {
  return (
    <Main>
      <Text text='Pair your products with our cool accessories' fontSize='2.5rem' weight='bold' />
      <Container>
        <Slider className='relative flex items-center'>
          <BorderPattern />
          <Scroller className='w-full h-full overflow-x-scroll scroll whitespace-nowrap scroll-smooth scrollbar-hide slider'>
            {Array(8)
              .fill(0)
              .map((_, ind) => (
                <Slide
                  src={imageUrl}
                  alt={ind + 'imageUrl'}
                  key={ind}
                  className='w-[220px] inline-block p-2 cursor-pointer hover:scale-105 ease-in-out duration-300'
                />
              ))}
          </Scroller>
        </Slider>
      </Container>
    </Main>
  );
};

export default withLayout(Showcase, { fullHeight: true });
