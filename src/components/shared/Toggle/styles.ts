import styled from 'styled-components';

export const Container = styled.label`
  display: flex;
  align-items: center;
  gap: 10px;
  cursor: pointer;
  justify-content: space-between;
  margin: 1rem 0;
`;

export const Switch = styled.div<{ checked: boolean }>`
  position: relative;
  width: 60px;
  height: 28px;
  background: white;
  border-radius: 32px;
  padding: 4px;
  transition: 250ms all;
  margin: 0 0.5rem;
  &:before {
    transition: 250ms all;
    content: '';
    position: absolute;
    width: 22px;
    height: 22px;
    border-radius: 35px;
    top: 50%;
    left: 4px;
    background: ${({ checked, theme }) =>
      checked ? theme.palette.common.white : theme.palette.primary.main};
    transform: translate(0, -50%);
  }
`;

export const Input = styled.input<{ checked: boolean }>`
  opacity: 0;
  position: absolute;

  &:checked + ${Switch} {
    background: ${(p) => p.theme.palette.primary.main};

    &:before {
      transform: translate(28px, -50%);
    }
  }
`;
