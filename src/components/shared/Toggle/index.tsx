import { useState, ChangeEvent } from 'react';
import { ToggleType } from 'types/toggle';
import Text from 'components/shared/Text';

import { Switch, Input, Container } from './styles';

const Toggle = ({ onClick, onLabel, offLabel }: ToggleType) => {
  const [checked, setChecked] = useState(false);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setChecked(e.target.checked);
    onClick?.(e.target.checked);
  };

  return (
    <Container className='toggle'>
      {offLabel && <Text text={offLabel} level='3' />}
      <Input checked={checked} type='checkbox' onChange={handleChange} />
      <Switch checked={checked} />
      {onLabel && <Text text={onLabel} level='3' weight={checked ? 'bold' : ''} />}
    </Container>
  );
};

export default Toggle;
