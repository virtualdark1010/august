import { motion } from 'framer-motion';
import styled from 'styled-components';
import { TextType } from 'types/text';
import { getFontSize } from './helper';

export const Container = styled(motion.span)<TextType>`
  ${(p) => {
    const {
      color,
      theme,
      fontSize,
      weight,
      level = 1,
      fontFamily,
      margin,
      active,
      center = false,
    } = p;
    return `
         color: ${(active && theme.palette.primary.main) || color || theme.palette.common.white};
         font-size: ${fontSize || getFontSize[level]};
         font-weight: ${weight || 'normal'};
         font-family: ${fontFamily || 'InterRegular'};
         margin: ${margin || '0'};
         ${center ? 'text-align: center' : ''};
    `;
  }};
`;
