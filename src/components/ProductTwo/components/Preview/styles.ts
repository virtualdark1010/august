import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  position: relative;
  .text-container:nth-child(1) {
    line-height: 5rem;
    margin-bottom: 1rem;
  }
  ${mobileMediaQuery(`
      flex-direction: column;
      .text-container:nth-child(1) {
        margin-bottom: 0.5rem;
        line-height: 2rem;
        width: 100%;
  }
  `)}
`;

export const RightSection = styled.div`
  width: 50%;
  padding: 3rem;
  display: flex;
  align-items: center;
  ${mobileMediaQuery(`width: 100%; padding: 6rem 3rem;`)}
`;

export const TextWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding: 0px 10rem;
  ${mobileMediaQuery(`padding: 0;`)}
`;

export const FloatingImage = styled(motion.div)`
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 3;
  transform: translate(-50%, -50%);
  ${mobileMediaQuery(`display:none;`)}
`;

export const ImageWrapper = styled.div`
  width: 50%;
  height: 100%;
  position: relative;
  ${mobileMediaQuery(`width: 100%;`)}
`;

export const Image = styled(motion.img)`
  object-fit: contain;
  width: 100%;
`;
