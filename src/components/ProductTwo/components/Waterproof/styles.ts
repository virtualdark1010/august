import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  align-items: center;
  position: relative;
  height: 100vh;
  ${mobileMediaQuery('flex-direction: column;')}
`;

export const TextWrapper = styled.div`
  width: inherit;
  display: flex;
  flex-direction: column;
  .text-container:nth-child(2) {
    line-height: 6rem;
    margin-bottom: 1rem;
    ${mobileMediaQuery('line-height: 2rem;')}
  }
`;

export const Flex = styled.div`
  display: flex;
  > div {
    margin-right: 2rem;
  }
`;

export const Image = styled(motion.img)`
  object-fit: cover;
`;

export const Content = styled.div`
  height: 100%;
  width: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #ff000060;
  ${mobileMediaQuery('width: 100%;  padding: 0 1.5rem;')}
`;

export const RightSection = styled.div`
  display: flex;
  justify-content: center;
  width: 50%;
  height: 100%;
  ${mobileMediaQuery('width: 100%;')}
`;
