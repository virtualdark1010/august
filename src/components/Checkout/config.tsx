import PaymentDetail from './forms/PaymentDetail';
import PersonalDetail from './forms/PersonalDetail';
import ShippingDetail from './forms/ShippingDetail';

export default {
  Address: PersonalDetail,
  Shipping: ShippingDetail,
  Payment: PaymentDetail,
};
