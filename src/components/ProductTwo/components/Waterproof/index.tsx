import React from 'react';

import Text from 'shared/Text';
import waterproof from 'assets/images/waterproof.png';

import { Main, TextWrapper, Image, Content, RightSection } from './styles';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const Waterproof = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <Content>
        <TextWrapper>
          <Text
            text='IPX8 Waterproof'
            fontSize={isMobile ? '1rem' : '2rem'}
            weight='bold'
            color='#00000040'
            center={isMobile}
            {...config({ leftToRight: true, delay: 0.5 })}
          />
          <Text
            text='Dunk it to 13ft.'
            fontSize={isMobile ? '2rem' : '5rem'}
            weight='bold'
            center={isMobile}
            {...config({ leftToRight: true, delay: 0.7 })}
          />
          <Text
            text='Surfing, pool party and skinny dipping ready. A bit like your spring break… only it’s an action cam.3'
            color='#00000060'
            fontSize={isMobile ? '0.875rem' : '1.75rem'}
            center={isMobile}
            {...config({ leftToRight: true, delay: 0.9 })}
          />
        </TextWrapper>
      </Content>
      <RightSection>
        <Image src={waterproof} {...config({ delay: 1.2 })} />
      </RightSection>
    </Main>
  );
};

export { Waterproof };
