export const quantityOption = Array.from({ length: 12 }).map((_, i) => ({
  value: i + 1,
  label: i + 1,
}));
