import { ReactNode } from 'react';

export type AvatarType = {
  children: ReactNode;
  width?: string;
  height?: string;
  bgColor?: string;
  initials?: boolean;
};
