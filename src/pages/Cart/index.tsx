import Cart from 'components/Cart';

import { withLayout } from 'hoc/withLayout';
import { withHF } from 'hoc/withHF';

const CartPage = () => <Cart />;

export default withHF(withLayout(CartPage, { fullHeight: true }));
