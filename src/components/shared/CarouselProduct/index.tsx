import Button from 'shared/Button';
import { Carousel } from 'react-responsive-carousel';
import { carouselProductType } from 'types/carouselProduct';

import {
  CarouseContainer,
  CarouseHeading,
  CarouselContents,
  CarouselDescriptionText,
  CarouselImageContainer,
} from './style';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

const CarouselProduct = (props: carouselProductType) => {
  return (
    <Carousel showThumbs={false} showStatus={false} showArrows={false} autoPlay>
      {props.map((info) => (
        <CarouseContainer>
          <CarouselContents>
            <CarouseHeading>{info.heading}</CarouseHeading>
            <CarouselDescriptionText>{info.subHeading}</CarouselDescriptionText>
          </CarouselContents>
          <CarouselImageContainer>
            <div>
              <img alt='' src={info.image} />
              <div className='glow-wrap'>
                <i className='glow'></i>
              </div>
            </div>
          </CarouselImageContainer>
          <div>
            <Button
              onClick={() => console.log('submit')}
              bgColor='#3C3C3C'
              borderColor='#fff'
              color='#fff'
              width='max-content'
            >
              {info.buttonText}
            </Button>
          </div>
        </CarouseContainer>
      ))}
    </Carousel>
  );
};

export default CarouselProduct;
