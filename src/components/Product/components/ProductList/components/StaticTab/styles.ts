import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  width: 95vw;
  flex-wrap: wrap;
`;
