import Text from 'components/shared/Text';
import ProductsShowcase from 'components/Product/components/ProductsShowcase';
import NoiseCancellation from 'components/Product/components/NoiseCancellation';
import ProductList from 'components/Product/components/ProductList';
import Showcase from 'components/Product/components/Showcase';
import OurSpeciality from 'components/Product/components/OurSpeciality';
import ProductMosaic from 'components/Product/components/ProductMosaic';

import { productList } from 'components/Product/config';
import { BestSellerWrapper } from 'components/Product/styles';

import { withLayout } from 'hoc/withLayout';
import Header from 'shared/Header';

const BestSeller = withLayout(() => (
  <BestSellerWrapper>
    <Text text='Our Best Sellers' color='#BB000E' fontSize='2rem' weight='bold' />
    <ProductMosaic />
  </BestSellerWrapper>
));

const Product = () => {
  return (
    <>
      <Header />
      <ProductsShowcase />
      <OurSpeciality />
      <NoiseCancellation />
      <ProductList tabs={productList} />
      <BestSeller />
      <Showcase />
    </>
  );
};

export default Product;
