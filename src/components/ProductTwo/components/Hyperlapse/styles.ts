import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  display: flex;
  margin-bottom: 30vh;
  position: relative;
  .text-container:nth-child(2) {
    line-height: 6rem;
    margin-bottom: 1rem;
    width: 50%;
  }
  img {
    bottom: -30%;
  }
  ${mobileMediaQuery(`
      flex-direction: column;
      margin-bottom: 0;
      .text-container:nth-child(2) {
        line-height: 3rem; 
        margin-bottom: 0;
        width: 100%;
      }
  `)}
`;

export const ImageWrapper = styled(motion.div)`
  width: 45%;
  height: 100%;
  ${mobileMediaQuery(`width: 100%;`)}
`;

export const Image = styled(motion.img)`
  object-fit: contain;
  width: 100%;
`;
