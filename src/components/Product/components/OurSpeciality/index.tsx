import { useState } from 'react';
import ReactPlayer from 'react-player';
import { isMobile } from 'react-device-detect';
import CircularProgress from 'components/shared/CircularProgress';
import { withLayout } from 'hoc/withLayout';

import {
  Row,
  Container,
  LeftSection,
  ActiveLine,
  VerticalLine,
  RightSection,
  VideoWrapper,
} from './styles';
import Text from 'components/shared/Text';
import { Cart } from 'assets/icons';

const VIDEO_LIST = [
  'https://www.youtube.com/watch?v=ysz5S6PUM-U',
  'https://www.youtube.com/watch?v=ysz5S6PUM-U',
  'https://www.youtube.com/watch?v=ysz5S6PUM-U',
];

const OurSpeciality = () => {
  const [playing, setPlaying] = useState<any>({ 0: 0, 1: 0, 2: 0 });
  const [index, setIndex] = useState(0);

  const handleProgress = (state: any) => {
    setPlaying(() => ({ [index]: Number(state.played.toFixed(2)) * 100 }));
  };

  const renderTab = ({ activeIndex }: { activeIndex: number }) => {
    return (
      <>
        <Row active={index === activeIndex} onClick={() => setIndex(activeIndex)}>
          {index === activeIndex && <ActiveLine index={index} />}
          <CircularProgress
            size={isMobile ? 45 : 75}
            strokeWidth={4}
            percentage={index === activeIndex && playing[index]}
            color='#BB000E'
            icon={<Cart />}
          />
          <Text text='Video One' color='white' fontSize='2' weight='bold' />
        </Row>
      </>
    );
  };

  return (
    <Container>
      <LeftSection className='flex items-center justify-center'>
        <VideoWrapper>
          <ReactPlayer
            url={VIDEO_LIST[index]}
            playing
            muted
            onProgress={handleProgress}
            onEnded={() => setIndex((i) => (i === VIDEO_LIST.length - 1 ? 0 : i + 1))}
          />
        </VideoWrapper>
      </LeftSection>
      <RightSection className='flex items-center justify-center'>
        <VerticalLine />
        {VIDEO_LIST.map((_, activeIndex) => renderTab({ activeIndex }))}
      </RightSection>
    </Container>
  );
};

export default withLayout(OurSpeciality, { fullHeight: true });
