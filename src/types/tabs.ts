import { ReactNode } from 'react';

export type Tab = {
  Render: any;
  shape: 'default' | 'square';
  label: ReactNode | string;
};

export type Tabs = {
  tabs: Tab[];
};
