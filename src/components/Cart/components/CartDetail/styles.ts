import styled from 'styled-components';

export const Main = styled.div`
  display: flex;
  margin-top: 3rem;
  justify-content: flex-end;
  button {
    margin-top: 1rem;
  }
`;

export const Container = styled.div`
  width: 35%;
`;

export const Detail = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 10px;
  margin-top: 0.5rem;
  padding: 0.5rem 1.5rem;
  border: 0.5px solid ${(p) => p.theme.palette.common.white};
`;

export const DetailRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 4px 0;
  padding-bottom: 0.5rem;
  border-bottom: 0.5px solid ${(p) => p.theme.palette.common.white};
  &:last-child {
    border-bottom: none;
    padding-bottom: 0;
  }
`;
