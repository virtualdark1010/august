import React, { useState } from 'react';
import Text from 'shared/Text';
import { Main, Wrapper, Dot, Flex, TextWrapper } from './styles';

const animation = {
  initial: { opacity: 0 },
  animate: { opacity: 1 },
  transition: { duration: 0.3 },
};

const SlideIndicator = ({
  slides,
  activeSlide,
}: {
  slides: { value: string; label: string; ref: any }[];
  activeSlide: string | undefined;
}) => {
  const [hoverSlide, setHoverSlide] = useState('');

  const scrollToElement = (ref: any) =>
    ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' });

  return (
    <Main>
      <Wrapper>
        {slides.map((slide) => {
          const isActive = slide.value === activeSlide;

          const textProps = {
            text: hoverSlide === slide.label || isActive ? slide.label : '',
            color: isActive ? '#BF0C0C' : 'white',
            weight: isActive ? 'bold' : 'normal',
          };

          const flexProps = {
            onMouseEnter: () => setHoverSlide(slide.label),
            onMouseLeave: () => setHoverSlide(''),
            key: slide.value,
            onClick: () => scrollToElement(slide.ref),
            isActive: isActive,
          };

          return (
            <Flex {...flexProps}>
              <Dot isActive={isActive} />
              {(hoverSlide === slide.label || isActive) && (
                <TextWrapper {...animation}>
                  <Text {...textProps} level='3' />
                </TextWrapper>
              )}
            </Flex>
          );
        })}
      </Wrapper>
    </Main>
  );
};

export default SlideIndicator;
