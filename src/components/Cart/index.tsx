import { useState } from 'react';
import CartRow from 'components/Cart/components/CartRow';
import CartHeader from 'components/Cart/components/CartHeader';
import CartDetail from 'components/Cart//components/CartDetail';
import Modal from 'shared/Modal';
import Text from 'components/shared/Text';
import Button from 'components/shared/Button';

import config from './config';

import { Main, Container, ModalWrapper, ButtonWrapper } from './styles';

const Cart = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <Main>
      <CartHeader />
      <Container>
        {config.map((c) => (
          <CartRow {...c} onClick={setIsOpen} />
        ))}
        <Modal isOpen={isOpen} center bgColor='#252424'>
          <ModalWrapper>
            <Text text='Are you sure you want to remove this item?' color='white' level='4' />
            <ButtonWrapper>
              <Button color='white' onClick={() => setIsOpen(false)}>
                No
              </Button>
              <Button color='white' onClick={() => setIsOpen(false)}>
                Yes
              </Button>
            </ButtonWrapper>
          </ModalWrapper>
        </Modal>
      </Container>
      <CartDetail />
    </Main>
  );
};

export default Cart;
