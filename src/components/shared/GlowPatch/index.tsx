import React from 'react';
import { GlowingPatch } from 'shared/icons';
import { Main } from './styles';
import { PatchPosition } from './types';

const GlowPatch = ({ position }: { position: PatchPosition }) => {
  return (
    <Main position={position}>
      <GlowingPatch />
    </Main>
  );
};

export default GlowPatch;
