import { useState, useEffect } from 'react';

export const useVideoPlayOnScroll = (videoElementWrapperRef?: any, videoPlayerRef?: any) => {
  const [scrollTop, setScrollTop] = useState(0);

  useEffect(() => {
    const onScroll = () => {
      const winScroll = document.documentElement.scrollTop;
      const height = videoElementWrapperRef?.current?.getBoundingClientRect()?.height;

      const scrolled = ((winScroll / height) * 100) / 100;
      if (scrolled <= 1) {
        videoPlayerRef?.current?.seekTo(scrolled);
        setScrollTop(scrolled);
      }
    };
    window.addEventListener('scroll', onScroll);
    return () => window.removeEventListener('scroll', onScroll);
  }, []);

  return scrollTop;
};
