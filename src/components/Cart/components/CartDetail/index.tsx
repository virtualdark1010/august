import Button from 'components/shared/Button';
import React from 'react';
import Text from 'shared/Text';
import { Main, Container, Detail, DetailRow } from './styles';

const CartDetail = () => {
  return (
    <Main>
      <Container>
        <Text text='Cart Detail:' color='white' level='4' />
        <Detail>
          <DetailRow>
            <Text text='Cart Total:' color='white' level='4' />
            <Text text='199.00' color='white' level='4' />
          </DetailRow>
          <DetailRow>
            <Text text='Shipping:' color='white' level='4' />
            <Text text='Free' color='white' level='4' />
          </DetailRow>
          <DetailRow>
            <Text text='Discount:' color='white' level='4' />
            <Text text='10%' color='white' level='4' />
          </DetailRow>
        </Detail>
        <Button onClick={() => {}} variant='inverted'>
          Checkout
        </Button>
      </Container>
    </Main>
  );
};

export default CartDetail;
