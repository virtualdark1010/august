import Text from 'shared/Text';
import { Main, Products, Price, Quantity, Total } from './styles';

const CartHeader = () => {
  return (
    <Main>
      <Products>
        <Text text='Products' color='white' level='4' />
      </Products>
      <Price>
        <Text text='Price' color='white' level='4' />
      </Price>
      <Quantity>
        <Text text='Quantity' color='white' level='4' />
      </Quantity>
      <Total>
        <Text text='Total' color='white' level='4' />
      </Total>
    </Main>
  );
};

export default CartHeader;
