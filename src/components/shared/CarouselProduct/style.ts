import styled from 'styled-components';

export const CarouselImageContainer = styled.div`
  width: 50%;
  padding: 5px;
  border-radius: 15px;
  filter: drop-shadow(11px 11px 5px #ccc);
  div {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
  }
  img {
    object-fit: cover;
    display: block;
    transition: all 0.5s cubic-bezier(0.645, 0.045, 0.355, 1);
    margin-top: -10px;
    max-width: 70%;
  }
  .glow-wrap {
    overflow: hidden;
    position: absolute;
    width: 100%;
    height: 100%;
    margin-top: -10px;
  }
  .glow {
    display: block;
    position: absolute;
    width: 40%;
    height: 200%;
    background: rgba(255, 255, 255, 0.2);
    top: 0;
    filter: blur(5px);
    transform: rotate(45deg) translate(-450%, 0);
    transition: all 0.5s cubic-bezier(0.645, 0.045, 0.355, 1);
  }
  :hover .glow {
    transform: rotate(45deg) translate(450%, 0);
    transition: all 1s cubic-bezier(0.645, 0.045, 0.355, 1);
  }
  :hover img,
  :hover .glow-wrap {
    margin-top: 0;
  }
`;

export const CarouseContainer = styled.div`
  height: 95vh;
  background-color: #fff;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;
`;

export const CarouselContents = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 15px;
`;

export const CarouseHeading = styled.div`
  font-size: 2rem;
  font-family: 'InterRegularExtraBold';
`;

export const CarouselDescriptionText = styled.div`
  font-size: 11px;
  line-height: 15px;
  width: 70%;
  font-family: InterExtraLight;
`;
