import { motion } from 'framer-motion';
import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Main = styled.section`
  .grid-container {
    ${mobileMediaQuery(`padding:2rem;`)}
  }
`;

export const ImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
  margin: 10rem 0;
`;

export const Column = styled(motion.div)`
  display: flex;
  flex-direction: column;
  span {
    ${mobileMediaQuery(`text-align: center;`)}
  }
`;

export const AnimationWrapper = styled(motion.div)`
  width: 100%;
  img {
    border-radius: 20px;
  }
`;

export const Image = styled(motion.img)<{ width?: string; height?: string; borderRadius?: string }>`
  object-fit: cover;
  width: ${({ width }) => width || '30vw'};
  height: ${({ height }) => height};
  border-radius: ${({ borderRadius }) => borderRadius || '0'};
`;

export const GridWrapper = styled(motion.div)`
  margin-top: 15rem;
`;
