import React from 'react';
import Text from 'shared/Text';

import { Main, TextWrapper, ImageOne, FloatingImage } from './styles';
import mountAnywhere1 from 'assets/images/auto-editing-bg.png';
import mountAnywhere2 from 'assets/images/auto-editing-image.png';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const AutoEditing = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <TextWrapper>
        <Text
          text='Auto Editing'
          fontSize='2rem'
          color='#ffffff70'
          center={isMobile}
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  y: 0,
                  transition: { duration: 0.7, type: 'spring' },
                },
                hidden: {
                  opacity: 0,
                  y: '100vw',
                  transition: { duration: 0.7, type: 'spring' },
                },
              },
            },
          })}
        />
        <Text
          text='The AI is taking over...'
          fontSize={isMobile ? '2rem' : '5rem'}
          weight='bold'
          center={isMobile}
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  y: 0,
                  transition: { duration: 1, type: 'spring', delay: 0.7 },
                },
                hidden: {
                  opacity: 0,
                  y: '-100vw',
                  transition: { duration: 1, type: 'spring', delay: 0.7 },
                },
              },
            },
          })}
        />
        <Text
          text="Your edit with FlashCut 2.0. AI takes your favorite clips, puts 'em in a story and edits them together on beat to music. Choose from themed templates or let AI freestyle. Don’t worry, it’s not coming for your job… yet."
          fontSize={isMobile ? '1rem' : '1.75rem'}
          center={isMobile}
          {...config({
            customConfig: {
              variants: {
                visible: {
                  opacity: 1,
                  y: 0,
                  transition: { duration: 1, type: 'spring', delay: 1 },
                },
                hidden: {
                  opacity: 0,
                  y: '-100vw',
                  transition: { duration: 1, type: 'spring', delay: 1 },
                },
              },
            },
          })}
        />
      </TextWrapper>
      <ImageOne src={mountAnywhere1} />
      <FloatingImage src={mountAnywhere2} />
    </Main>
  );
};

export { AutoEditing };
