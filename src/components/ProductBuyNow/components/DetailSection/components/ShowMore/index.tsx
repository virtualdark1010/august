import Button from 'components/shared/Button';
import { Main, Line } from './styles';

export const ShowMore = () => {
  return (
    <Main>
      <Line />
      <Button onClick={() => {}} bgColor='white'>
        Show More
      </Button>
    </Main>
  );
};
