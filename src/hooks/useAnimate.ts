import { useAnimation } from 'framer-motion';
import { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';

type AnimationConfigType = {
  leftToRight?: boolean;
  customConfig?: any;
  x?: number | string;
  delay?: number;
  duration?: number;
  stiffness?: number;
};

export const useAnimate = () => {
  const animate = useAnimation();
  const { ref, inView } = useInView({ threshold: 0.2 });

  const config = ({
    leftToRight,
    duration = 1,
    customConfig,
    x = 0,
    delay = 0.5,
    stiffness = 30,
  }: AnimationConfigType) => ({
    animate,
    initial: 'hidden',
    transition: { duration },
    variants: {
      visible: {
        opacity: 1,
        x,
        transition: { type: 'spring', stiffness, delay },
      },
      hidden: {
        opacity: 0,
        x: leftToRight ? '-200vw' : '200vw',
        transition: { type: 'spring', stiffness, delay: 0.3 },
      },
    },
    ...customConfig,
  });

  useEffect(() => {
    if (inView) {
      animate.start('visible');
    } else {
      animate.start('hidden');
    }
  }, [animate, inView]);

  return { ref, config, inView };
};
