import { motion } from 'framer-motion';
import styled from 'styled-components';

export const Main = styled.footer`
  position: relative;
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  gap: 0px;
  height: 450px;
  align-items: flex-end;
  padding: 2rem 0 8rem 0;
  background-color: ${(p) => p.theme.palette.background.primary};
  > span:last-of-type {
  }
  @media screen and (max-width: 768px) {
    display: flex;
    flex-direction: column;
    height: auto;
  }
`;

export const Copyright = styled(motion.div)`
  position: absolute;
  left: 50%;
  bottom: 0;
  transform: translate(-50%);
  text-align: center;
  font-size: 1rem;
  padding: 2rem 0;
  border-top: 1px solid rgba(255, 255, 255, 0.2);
  width: 85%;
  align-self: center;
`;
