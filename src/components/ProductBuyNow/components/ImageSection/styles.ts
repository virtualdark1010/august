import styled from 'styled-components';
import { mobileMediaQuery } from 'utils/mediaQuery';

export const Container = styled.div`
  background-color: ${(p) => p.theme.palette.background.secondary};
  width: 55%;
  ${mobileMediaQuery('width: 100%;')}
  >div {
    ${mobileMediaQuery('height: 50vh !important;')}
  }
`;
