import React from 'react';
import { Checkbox } from 'shared/icons/Checkbox';
import Text from 'shared/Text';
import { Container, Stack } from './styles';

const ShippingCard = ({
  checked,
  company,
  time,
}: {
  checked?: boolean;
  company: string;
  time: string;
}) => (
  <Container>
    <Checkbox checked={checked} />
    <Stack>
      <Text text={company} color='white' level='3' weight='bold' />
      <Text text={time} color='white' level='2' />
    </Stack>
  </Container>
);

export default ShippingCard;
