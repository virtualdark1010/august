type Field = 'half' | 'full' | 'quarter';

export const getFieldWidth = (wdith: Field) => {
  switch (wdith) {
    case 'half':
      return '48.5%';
    case 'quarter':
      return '32%';
    default:
    case 'full':
      return '98.5%';
  }
};
