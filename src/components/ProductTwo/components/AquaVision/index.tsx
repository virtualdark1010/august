import React from 'react';

import Text from 'shared/Text';
import Button from 'shared/Button';
import ipad1 from 'assets/images/ipad-1.png';
import ipad2 from 'assets/images/ipad-2.png';

import { Main, TextWrapper, Image, Flex, Column } from './styles';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const AquaVision = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <TextWrapper>
        <Text
          text='AquaVision'
          fontSize={isMobile ? '1rem' : '2rem'}
          weight='bold'
          color='#BF0C0C80'
          center
          {...config({ leftToRight: true, delay: 0.5 })}
        />
        <Text
          text='Make water crystal clear.'
          fontSize={isMobile ? '1.5rem' : '5rem'}
          weight='bold'
          color='#1E1E1E'
          center
          {...config({ delay: 0.7 })}
        />
        <Text
          text='Ugly blue haze in your underwater footage? Turn on AquaVision in the app. Upgraded from the bottom up, the algorithm now produces even more realistic and vivid colors than before.'
          fontSize={isMobile ? '1rem' : '1.75rem'}
          color='#1E1E1E60'
          center
          {...config({ leftToRight: true, delay: 0.9, duration: 2 })}
        />
      </TextWrapper>
      <Flex>
        <Column>
          <Image
            src={ipad1}
            {...config({
              customConfig: {
                variants: {
                  visible: {
                    opacity: 1,
                    x: 0,
                    transition: { duration: 1, type: 'spring', delay: 0.5 },
                  },
                  hidden: {
                    opacity: 0,
                    x: '100vw',
                    transition: { duration: 1, type: 'spring', delay: 0.5 },
                  },
                },
              },
            })}
          />
          <Button
            onClick={() => {}}
            bgColor='#1E1E1E'
            borderColor='#1E1E1E'
            color='white'
            {...config({
              customConfig: {
                variants: {
                  visible: {
                    opacity: 1,
                    x: 0,
                    transition: { duration: 1, type: 'spring', stiffness: 50 },
                  },
                  hidden: {
                    opacity: 0,
                    x: '-100vw',
                    transition: { duration: 1, type: 'spring', stiffness: 50 },
                  },
                },
              },
            })}
          >
            No AquaVision
          </Button>
        </Column>
        <Column>
          <Image
            src={ipad2}
            {...config({
              customConfig: {
                variants: {
                  visible: {
                    opacity: 1,
                    x: 0,
                    transition: { duration: 1, type: 'spring', delay: 1 },
                  },
                  hidden: {
                    opacity: 0,
                    x: '-100vw',
                    transition: { duration: 1, type: 'spring', delay: 1 },
                  },
                },
              },
            })}
          />
          <Button
            onClick={() => {}}
            bgColor='#BF0C0C'
            borderColor='#BF0C0C'
            color='white'
            {...config({
              customConfig: {
                variants: {
                  visible: {
                    opacity: 1,
                    x: 0,
                    transition: { duration: 0.7, type: 'spring', delay: 1.2 },
                  },
                  hidden: {
                    opacity: 0,
                    x: '100vw',
                    transition: { duration: 0.7, type: 'spring', delay: 1.2 },
                  },
                },
              },
            })}
          >
            AquaVision
          </Button>
        </Column>
      </Flex>
    </Main>
  );
};

export { AquaVision };
