export type carouselProductType = {
  heading: string;
  subHeading: string;
  image: string;
  buttonText: string;
  showThumbs?: boolean;
  showStatus?: boolean;
  showArrows?: boolean;
  autoPlay?: boolean;
}[];
