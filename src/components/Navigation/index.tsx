import { useMemo, useState } from 'react';

import Modal from 'components/shared/Modal';
import { NavigationConfig } from 'types/navigation';
import { SubMenu } from './components/SubMenu';

import config, { subMenuData } from './config';
import { Nav, Link, Label, Badge } from './styles';

const Navigation = () => {
  const [activeHoverLink, setActiveHoverLink] = useState('product');
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleMouseEnter = (name: string) => {
    setIsOpen(true);
    setActiveHoverLink(name.toLowerCase());
  };

  const data = useMemo(() => subMenuData[activeHoverLink as 'product'], [activeHoverLink]);

  return (
    <Nav onMouseLeave={() => setIsOpen(false)}>
      {config.map(({ link, name, badge }: NavigationConfig) => (
        <Link key={link} onMouseEnter={() => handleMouseEnter(name)}>
          <Label>
            {name} {badge && <Badge>{badge}</Badge>}
          </Label>
        </Link>
      ))}
      <Modal isOpen={isOpen}>
        <SubMenu data={data} />
      </Modal>
    </Nav>
  );
};

export default Navigation;
