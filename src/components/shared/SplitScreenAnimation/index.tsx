import { motion } from 'framer-motion';
import React from 'react';
import Text from 'shared/Text';
import { Blur, BottomSection, Main, TopSection } from './styles';

const SplitTextAnimation = ({
  topSectionText,
  bottomSectionText,
}: {
  topSectionText: string;
  bottomSectionText: string;
}) => {
  return (
    <Main>
      {topSectionText && (
        <TopSection>
          <Blur direction='bottom' />
          <motion.div
            initial={{ y: 200, opacity: 1 }}
            animate={{ y: 0, opacity: 1 }}
            transition={{ duration: 2 }}
          >
            <Text
              text={topSectionText}
              color='#ffffff'
              fontSize='5vw'
              weight='700'
              fontFamily='ClashDisplayBold'
            />
          </motion.div>
        </TopSection>
      )}
      {bottomSectionText && (
        <BottomSection>
          <Blur direction='top' />
          <motion.div
            initial={{ y: -200, opacity: 0 }}
            animate={{ y: 0, opacity: 1 }}
            transition={{ duration: 2 }}
          >
            <Text
              text={bottomSectionText}
              color='#FF0000'
              fontSize='5vw'
              weight='700'
              fontFamily='ClashDisplayBold'
            />
          </motion.div>
        </BottomSection>
      )}
    </Main>
  );
};

export default SplitTextAnimation;
