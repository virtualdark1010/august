import styled, { keyframes } from 'styled-components';

const commonstyles = `
top: 0;
width: 50%;
bottom: 0;
content: ' ';
z-index: 1;
position: absolute;
pointer-events: none;
`;

const marquee = keyframes`
  0% {
    transform: translate(0%, 0);
  }
  100% {
    transform: translate(-200%, 0);
  }
 `;

const centerAlign = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Main = styled.section`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  border-radius: 2rem;
  margin: 10vw 0;
  span {
    margin-bottom: 2rem;
  }
`;

export const Flex = styled.div<{ paddingLeft?: string }>`
  display: flex;
  align-items: center;
  margin: 2rem 0;
  ${({ paddingLeft }) => paddingLeft && `padding-left: ${paddingLeft}`};
  svg {
    margin: 2rem 4rem 0 0;
    animation: ${marquee} linear 15s infinite;
  }
`;

export const Wrapper = styled.div`
  width: 80%;
  display: flex;
  align-items: center;
  flex-direction: column;
  overflow: hidden;
  position: relative;
  background: #1c1c1c;
  border-radius: 20px;
  box-shadow: 0px 0px 100px 1px rgb(255 255 255 / 7%);
`;

export const BackDrop = styled(centerAlign)`
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: 1;
  position: absolute;
  &::before {
    left: 0;
    background: linear-gradient(
      90deg,
      rgba(28, 28, 28, 1) 0%,
      rgba(28, 28, 28, 1) 5%,
      rgba(28, 28, 28, 0) 100%
    );
    ${commonstyles};
  }
  &::after {
    right: 0;
    background: linear-gradient(
      90deg,
      rgba(28, 28, 28, 0) 0%,
      rgba(28, 28, 28, 1) 95%,
      rgba(28, 28, 28, 1) 100%
    );
    ${commonstyles};
  }
`;

export const Content = styled.div`
  padding: 32px;
  max-width: 640px;
  background: rgba(28, 28, 28, 1);
  box-shadow: 0px 0px 156px 158px rgba(28, 28, 28);
`;
