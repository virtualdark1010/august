import React from 'react';

import { Main, RightSection, TextWrapper, FloatingImage, Image, ImageWrapper } from './styles';
import Text from 'shared/Text';
import slowMotionImage from 'assets/images/slow-motion.png';
import floatingPhone from 'assets/images/slow-motion-phone.png';
import { useAnimate } from 'hooks/useAnimate';
import { isMobile } from 'react-device-detect';

const Preview = () => {
  const { ref, config } = useAnimate();
  return (
    <Main ref={ref}>
      <FloatingImage
        {...config({
          customConfig: {
            variants: {
              visible: {
                opacity: 1,
                rotate: 360,
                x: '-50%',
                y: '-50%',
                scale: 1,
                transition: { duration: 2, type: 'spring', stiffness: 50 },
              },
              hidden: {
                opacity: 0,
                rotate: 0,
                scale: 0,
                transition: { duration: 2, type: 'spring', stiffness: 50 },
              },
            },
          },
        })}
      >
        <Image src={floatingPhone} />
      </FloatingImage>
      <ImageWrapper>
        <Image src={slowMotionImage} {...config({ stiffness: 40, duration: 2, delay: 0.5 })} />
      </ImageWrapper>
      <RightSection>
        <TextWrapper>
          <Text
            text='Preview on the go.'
            fontSize={isMobile ? '2rem' : '5rem'}
            center={isMobile}
            weight='bold'
            color='#1E1E1E'
            {...config({ leftToRight: true, stiffness: 50, duration: 2, delay: 0.5 })}
          />
          <Text
            text='Tap open the Insta360 app to preview your shot in real time over WiFi and adjust settings on the fly.'
            fontSize={isMobile ? '1rem' : '1.75rem'}
            center={isMobile}
            color='#1E1E1E60'
            {...config({ leftToRight: true, stiffness: 50, duration: 2, delay: 0.7 })}
          />
        </TextWrapper>
      </RightSection>
    </Main>
  );
};

export { Preview };
