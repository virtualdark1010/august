import { withLayout } from 'hoc/withLayout';

import { Container } from './styles';
import SplitTextAnimation from 'shared/SplitScreenAnimation';

const Feature = () => {
  return (
    <Container>
      <SplitTextAnimation topSectionText='Conference devices for' bottomSectionText='the doers' />
    </Container>
  );
};

export default withLayout(Feature, { withHeader: true });
