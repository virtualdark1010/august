import { FormEvent, useState } from 'react';
import { InputType } from 'types/input';
import { Main, StyledInput, Label } from './style';

const Input = ({ type, name, label, placeholder, onChange, fullBorder = false }: InputType) => {
  const [value, setValue] = useState<string>('');
  const [isFilled, setIsFilled] = useState<boolean>(false);

  const handleChange = (event: FormEvent<HTMLInputElement>) => {
    const value = event?.currentTarget?.value;
    onChange?.(value);
    setValue(value);
  };

  const handleFocus = () => setIsFilled(true);

  const handleBlur = () => !value && setIsFilled(false);

  return (
    <Main>
      <StyledInput
        type={type}
        name={name}
        placeholder={placeholder}
        fullBorder={fullBorder}
        onChange={handleChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
      />
      {label && (
        <Label htmlFor={name} isFilled={isFilled}>
          {label}
        </Label>
      )}
    </Main>
  );
};

export default Input;
