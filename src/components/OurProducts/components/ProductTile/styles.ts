import { motion } from 'framer-motion';
import styled from 'styled-components';
import { ProductTileType } from 'types/productTile';

export const Container = styled.div<ProductTileType>`
  padding: 1px;
  border-radius: 20px;
  width: 100%;
  position: relative;
  background: linear-gradient(112.62deg, #ff0000 29.4%, #373737 105.15%);
  > div {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    ${(p) => {
      const { theme, bgColor } = p;
      const {
        borderRadiusTop = theme.borderRadius,
        borderRadiusBottom = theme.borderRadius,
        borderRadiusRight = theme.borderRadius,
        borderRadiusLeft = theme.borderRadius,
      } = theme;
      return `
      background: ${bgColor || theme.palette.background.secondary};
      box-shadow: ${theme.cardShadow};
      border-radius: ${borderRadiusTop} ${borderRadiusRight} ${borderRadiusBottom} ${borderRadiusLeft};
      `;
    }};
  }
`;

export const InnerContainer = styled(motion.div)`
  display: flex;
  flex-direction: column;
  padding: 3rem 3rem 0 3rem;
  position: relative;
`;

export const ButtonIconContainer = styled(motion.div)`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 1rem;
  button {
    margin-top: 0;
  }
  div {
    width: 40px;
    height: 40px;
    svg {
      cursor: pointer;
      fill: #fff;
    }
  }
`;

export const Image = styled.img`
  margin-top: 3rem;
  object-fit: cover;
`;
