import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './index.css';

import React from 'react';
import ReactDOM from 'react-dom/client';
import { ThemeProvider } from 'styled-components';

import Routes from 'src/routes';

import ShopppingCartProvider from 'context/ShoppingCart';

import GlobalStyle from 'styles/global';
import theme from 'styles/theme';

import './index.css';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <ThemeProvider theme={theme}>
    <ShopppingCartProvider>
      <Routes />
    </ShopppingCartProvider>
    <GlobalStyle />
  </ThemeProvider>,
);
