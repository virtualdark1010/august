import { motion } from 'framer-motion';
import styled from 'styled-components';
import { CardType } from 'types/card';

export const Container = styled(motion.div)<CardType>`
  ${(p) => {
    const { theme, bgColor, padding, width, height, direction = 'row' } = p;
    const {
      borderRadiusTop = theme.borderRadius,
      borderRadiusBottom = theme.borderRadius,
      borderRadiusRight = theme.borderRadius,
      borderRadiusLeft = theme.borderRadius,
    } = theme;
    return `
         background: ${bgColor || theme.palette.background.secondary};
         box-shadow: ${theme.cardShadow};
         border-radius: ${borderRadiusTop} ${borderRadiusRight} ${borderRadiusBottom} ${borderRadiusLeft};
         padding: ${`${padding}rem` || '4rem'};
         width: ${`${width || ''}`};
         height: ${`${height || ''}`};
         flex-direction: ${direction};
    `;
  }};
`;
